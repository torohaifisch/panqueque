<?php
class Proveedor_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_proveedor($data)
    {
        return $this->db->insert('proveedor',$data);
    }

    public function borrar_proveedor($id){

         $query=$this->db
            ->set('activo',0)
            ->where('id_proveedor',$id)
            ->update('proveedor');

        return $query;
    }

    public function actualizar_proveedor($id,$data){

        $this->db->where('id_proveedor', $id);
        return $this->db->update('proveedor', $data);


    }

    public function get_lista_proveedores(){

        $this->db
            ->select('*')
            ->from('proveedor')
            ->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }


    public function get_proveedor($id){

        $this->db
            ->select('*')
            ->from('proveedor')
            ->where('id_proveedor',$id);

        $query=$this->db->get();

        return $query->row();
    }




}