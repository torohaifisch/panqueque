<?php
class Producto_formato_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function crear($data)
    {
        $this->db->insert('producto_formato',$data);
        return $this->db->insert_id();
    }


    public function get_lista(){
        $query= $this->db
            ->select('*')
            ->from('producto_formato')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->get();

        return $query->result();
    }


    public function get_lista_productos(){
        $query= $this->db

            ->select('producto_formato.id_producto,producto_formato.id_unidad_medida,producto.nombre,unidad_medida.medida')
            ->from('producto_formato')
            ->join('producto','producto_formato.id_producto=producto.id_producto')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->group_by('producto_formato.id_producto','producto_formato.id_unidad_medida')
            ->get();

        return $query->result();

    }


    public function combinacion_existe($id_p,$id_m,$id_c){
        $this->db
            ->select('*')
            ->from('producto_formato')
            ->where('id_producto',$id_p)
            ->where('id_unidad_medida',$id_m)
            ->where('cantidad_contenida',$id_c);
        $query=$this->db->get();
        if ($query->num_rows()>0){
            return $query->row()->id_producto_formato;
        }
        else{
            return null;
        }

    }
}