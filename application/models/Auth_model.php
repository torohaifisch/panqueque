<?php

class Auth_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function validar_usuario($user,$pass)
    {
        $query= $this->db
            ->select('*')
            ->from('usuario')
            ->where('nombre_usuario',$user)
            ->where('pass',$pass)
            ->get();

        return Array($query->num_rows(),$query->row());
    }


}
