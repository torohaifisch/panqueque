<?php
class Marca_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_marca($data)
    {
        return $this->db->insert('marca',$data);
    }

    public function borrar_marca($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_marca',$id)
            ->update('marca');
        return $query;
    }

    public function actualizar_marca($data){

        return $this->db->replace('marca',$data);

    }

    public function get_lista_marcas(){

        $this->db
            ->select('*')
            ->from('marca')
            ->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }



}