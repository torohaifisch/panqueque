<?php
class Solicitud_compra_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('solicitud_compra',$data);
        return $this->db->insert_id();

    }


    public function actualizar_total($id,$data){

        $query=$this->db
            ->set('valor_estimado',$data)
            ->where('id_solicitud_compra',$id)
            ->update('solicitud_compra');

        return $query;
    }


    public function get_lista(){

        $this->db
            ->select('solicitud_compra.*,sucursal.nombre as sucursal,usuario.nombre,usuario.ape_pat,area_trabajo.area')
            ->from('solicitud_compra')
            ->join('usuario','solicitud_compra.id_usuario=usuario.id_usuario')
            ->join('sucursal','solicitud_compra.id_sucursal=sucursal.id_sucursal')
            ->join('area_trabajo','solicitud_compra.id_area_trabajo=area_trabajo.id_area_trabajo')
            ->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }

    public function get_solicitud($id){

        $this->db
            ->select('solicitud_compra.*,sucursal.nombre as sucursal,usuario.nombre,usuario.ape_pat,area_trabajo.area')
            ->from('solicitud_compra')
            ->join('usuario','solicitud_compra.id_usuario=usuario.id_usuario')
            ->join('sucursal','solicitud_compra.id_sucursal=sucursal.id_sucursal')
            ->join('area_trabajo','solicitud_compra.id_area_trabajo=area_trabajo.id_area_trabajo')
            ->where('id_solicitud_compra',$id);


        $query=$this->db->get();

        return $query->row();
    }


    public function solicitudes_pendientes(){
        $this->db
            ->select('*')
            ->from('solicitud_compra')
            ->where('estado',2);
        $query=$this->db->get();

        return $query->num_rows();
    }

    public function aceptar_solicitud($id){
        $query=$this->db
            ->set('estado',1)
            ->where('id_solicitud_compra',$id)
            ->update('solicitud_compra');

        return $query;

    }
    public function rechazar_solicitud($id){
        $query=$this->db
            ->set('estado',0)
            ->where('id_solicitud_compra',$id)
            ->update('solicitud_compra');

        return $query;

    }

}