<?php
class Historial_pago_salario_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function ingresar_salario($data)
    {
        return $this->db->insert('historial_pago_salario',$data);
    }

    public function borrar($id){
        return $this->db
            ->where('id_historial_pago_salario',$id)
            ->delete('historial_pago_salario');


    }

    public function actualizar($id,$data){

        $this->db->where('id_historial_pago_salario', $id);
        return $this->db->update('historial_pago_salario', $data);

    }

    public function get_lista(){

        $this->db
            ->select('historial_pago_salario.*,tipo_salario.tipo,trabajador.nombre, trabajador.rut')
            ->from('historial_pago_salario')
            ->join('tipo_salario','historial_pago_salario.id_tipo_salario=tipo_salario.id_tipo_salario')
            ->join('trabajador','historial_pago_salario.id_trabajador=trabajador.id_trabajador');

        $query=$this->db->get();

        return $query->result();
    }

    public function get_salario($id){

        $this->db
            ->select('*')
            ->from('historial_pago_salario')
            ->where('id_historial_pago_salario',$id);

        $query=$this->db->get();

        return $query->result();
    }
}