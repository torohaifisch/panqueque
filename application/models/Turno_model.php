<?php
class Turno_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function iniciar_turno($data){
        return $this->db->insert('turno',$data);

    }

    public function get_lista(){
        $query=$this->db
            ->select('turno.*,sucursal.nombre as sucursal,area_trabajo.area ')
            ->from('turno')
            ->join('sucursal','turno.id_sucursal=sucursal.id_sucursal')
            ->join('area_trabajo','turno.id_area=area_trabajo.id_area_trabajo')
            ->order_by('turno.fecha','DESC')
            ->get();

        return $query->result();
    }
    public function cerrar_turno($id,$p){
        date_default_timezone_set("America/Santiago");


        $query=$this->db
            ->set('abierto',0)
            ->set('cierra_turno',$p)
            ->set('hora_termino',date("H:i:s"))
            ->where('id_usuario',$id)
            ->where('abierto',1)
            ->update('turno');

        return $query;
    }

    public function esta_activo($id){

        $this->db
            ->select('*')
            ->from('turno')
            ->where('id_usuario',$id)
            ->where('abierto',1);


        $query=$this->db->get();

        return $query->num_rows();

    }


}