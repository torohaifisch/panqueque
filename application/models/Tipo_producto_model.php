<?php
class Tipo_producto_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function get_lista(){
        $query= $this->db
            ->select('*')
            ->from('tipo_producto')
            ->get();

        return $query->result();
    }
}