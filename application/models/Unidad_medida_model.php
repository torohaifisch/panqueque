<?php
class Unidad_medida_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function get_lista(){
        $query= $this->db
            ->select('*')
            ->from('unidad_medida')
            ->get();

        return $query->result();
    }
}