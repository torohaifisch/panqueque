<?php
class Mini_bodega_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('mini_bodega',$data);
        return $this->db->insert_id();
    }

    public function activar_producto($id){
        return   $this->db
            ->set('activo',1)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');
    }

    public function desactivar_producto($id){
        return   $this->db
            ->set('activo',0)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');
    }



    public function actualizar($id,$data){

        $this->db->where('id_mini_bodega', $id);
        return $this->db->update('mini_bodega', $data);
    }


    public function actualizar_stock($id_marca_producto_formato,$id_sucursal,$stock){
        return   $this->db
            ->set('stock',$stock)
            ->where('id_marca_producto_formato',$id_marca_producto_formato)
            ->where('id_sucursal',$id_sucursal)
            ->update('inventario_bodega');



    }


    public function modificar_stock($id,$cantidad){
        return   $this->db
            ->set('sub_stock',$cantidad)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');

    }





    public function sumar_stock($id,$stock){
        return $this->db
            ->set('sub_stock','sub_stock+'.$stock,FALSE)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');

    }


    public function restar_stock($id,$stock){
        return $this->db
            ->set('sub_stock','sub_stock-'.$stock,FALSE)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');

    }


    public function existe_producto($id,$id_unidad,$id_sucursal,$id_area){
        $this->db
            ->select('*')
            ->from('mini_bodega')
            ->where('id_producto',$id)
            ->where('id_sucursal',$id_sucursal)
            ->where('id_unidad_medida',$id_unidad)
            ->where('id_area',$id_area);
        return $query=$this->db->get();
    }




    public function actualizar_cantidad_critica($id,$valor){
        return $this->db
            ->set('cantidad_critica',$valor)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');
    }

    public function actualizar_cantidad_optima($id,$valor){
        return $this->db
            ->set('cantidad_optima',$valor)
            ->where('id_mini_bodega',$id)
            ->update('mini_bodega');
    }


    public function get_productos_sucursal($s){
        $this->db
            ->select('area_trabajo.area,mini_bodega.id_mini_bodega,producto.nombre AS producto,mini_bodega.sub_stock,sucursal.nombre AS sucursal, unidad_medida.medida, mini_bodega.cantidad_critica,mini_bodega.cantidad_optima')
            ->from('mini_bodega')
            ->join('sucursal','mini_bodega.id_sucursal=sucursal.id_sucursal')
            ->join('producto','mini_bodega.id_producto=producto.id_producto')
            ->join('area_trabajo','mini_bodega.id_area=area_trabajo.id_area_trabajo')
            ->join('unidad_medida','mini_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('mini_bodega.id_sucursal',$s);
        $query=$this->db->get();
        return $query->result();
    }




    public function get_productos_sucursal_area($s,$a){
        $this->db
            ->select('area_trabajo.area,mini_bodega.id_mini_bodega,producto.nombre AS producto,mini_bodega.sub_stock,sucursal.nombre AS sucursal, unidad_medida.medida, mini_bodega.cantidad_critica,mini_bodega.cantidad_optima')
            ->from('mini_bodega')
            ->join('sucursal','mini_bodega.id_sucursal=sucursal.id_sucursal')
            ->join('producto','mini_bodega.id_producto=producto.id_producto')
            ->join('area_trabajo','mini_bodega.id_area=area_trabajo.id_area_trabajo')
            ->join('unidad_medida','mini_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('mini_bodega.id_sucursal',$s)
            ->where('mini_bodega.id_area',$a);
        $query=$this->db->get();
        return $query->result();
    }


    public function get_productos_sucursal_area_con_stock($id,$a){
        $sql='SELECT mini_bodega.*,producto.nombre as producto,unidad_medida.medida, inventario_bodega.cantidad as stock_general,inventario_bodega.id_inventario_bodega FROM mini_bodega join inventario_bodega on mini_bodega.id_sucursal=inventario_bodega.id_sucursal
              and mini_bodega.id_producto=inventario_bodega.id_producto and mini_bodega.id_unidad_medida=inventario_bodega.id_unidad_medida
              join unidad_medida on unidad_medida.id_unidad_medida = mini_bodega.id_unidad_medida join producto on mini_bodega.id_producto=producto.id_producto
              WHERE  mini_bodega.id_sucursal=? and mini_bodega.id_area=?';

        $query=$this->db->query($sql,array($id,$a));

        return $query->result();
    }


    public function valor_total_estimado($id=null){
        if ($id==null){
            $id=0;
        }
        if ($id!=0){
            $sql='SELECT FLOOR(sum(media*cantidad)) as valor from (SELECT AVG(valor_medio/cantidad_contenida) as media,producto_formato.id_producto FROM marca_producto_formato join producto_formato on marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato WHERE valor_medio>0 GROUP BY producto_formato.id_producto,producto_formato.id_unidad_medida) a join (select sum(sub_stock) as cantidad,id_producto from mini_bodega where mini_bodega.id_sucursal=? GROUP BY id_producto) b on a.id_producto=b.id_producto
';
            $query=$this->db->query($sql,array($id));


        }
        else{
            $query=$this->db->query('SELECT FLOOR(AVG(valor_medio/cantidad_contenida)*(mini_bodega.sub_stock)) as valor FROM marca_producto_formato join producto_formato on marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato join mini_bodega on producto_formato.id_producto=mini_bodega.id_producto AND producto_formato.id_unidad_medida=mini_bodega.id_unidad_medida WHERE valor_medio>0 GROUP BY producto_formato.id_producto,producto_formato.id_unidad_medida
');}
        if ($query->num_rows()>0){
            return $query->row()->valor;

        }
        return 0;

    }


}