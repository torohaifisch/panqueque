<?php
class Venta_empresa_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {

        return $this->db->insert('venta_empresa',$data);
    }



    public function fetch_ingreso_dia(){

        date_default_timezone_set("America/Santiago");

        $this->db
            ->select('SUM(monto) as monto')
            ->from('venta_empresa')
            ->where('fecha',date("Y-m-d"));

        $query=$this->db->get();
        return $query->result();
    }

    public function fetch_acum_mes(){
        $this->db->query('SET lc_time_names = \'es_ES\'');
        $query=$this->db->query('SELECT MONTHNAME(fecha) as mes, SUM(monto) as acum FROM venta_empresa WHERE fecha > DATE_SUB(CURRENT_DATE, INTERVAL 6 MONTH) GROUP BY YEAR(fecha), MONTH(fecha)');
        return $query->result();

    }

}