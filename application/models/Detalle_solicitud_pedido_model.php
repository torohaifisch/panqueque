<?php
class Detalle_solicitud_pedido_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('detalle_solicitud_pedido',$data);

    }


    public function listar_detalles($id){
        $this->db
            ->select('detalle_solicitud_pedido.id_inventario_bodega,producto.id_producto,unidad_medida.id_unidad_medida,detalle_solicitud_pedido.cantidad_solicitada,unidad_medida.medida,producto.nombre as producto, inventario_bodega.cantidad')
            ->from('detalle_solicitud_pedido')
            ->join('inventario_bodega','detalle_solicitud_pedido.id_inventario_bodega=inventario_bodega.id_inventario_bodega')
            ->join('producto','inventario_bodega.id_producto=producto.id_producto')
            ->join('unidad_medida','inventario_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('id_solicitud_pedido',$id);

        $query=$this->db->get();
        return $query->result();
    }




}