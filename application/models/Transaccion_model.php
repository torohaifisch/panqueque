<?php
class Transaccion_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_transaccion($data)
    {
        $this->db->insert('transaccion',$data);
        return $this->db->insert_id();
    }

    public function borrar_transaccion($id){

        return $this->db
            ->delete('dte')
            ->where('id',$id);


    }

    public function actualizar_transaccion($data){

        return $this->db->replace('transaccion',$data);

    }


    public function get_lista_transaccion(){
        $query= $this->db
            ->select('*')
            ->from('transaccion')
            ->get();

        return $query->result();
    }

    public function fetch_transacciones(){
        $this->db
            ->select('transaccion.id_transaccion,tipo_io.nombre AS tipo_io, proveedor.nombre AS proveedor, dte.monto,dte.fecha_emision,dte.fecha_vencimiento')
            ->from('transaccion')
            ->join('tipo_io','transaccion.id_tipo_io = tipo_io.id_tipo_io')
            ->join('proveedor','transaccion.id_proveedor=proveedor.id_proveedor')
            ->join('dte','transaccion.id_dte=dte.id_dte');
        $query=$this->db->get();
        return $query->result();
    }



    public function fetch_ultimas_transacciones($n){
        $this->db
            ->select('transaccion.id_tipo_io,dte.monto,dte.fecha_emision,proveedor.nombre AS proveedor')
            ->from('transaccion')
            ->join('dte','transaccion.id_dte=dte.id_dte')
            ->join('proveedor','transaccion.id_proveedor=proveedor.id_proveedor')
            ->order_by('dte.fecha_emision','DESC')
            ->limit($n);

        $query=$this->db->get();
        return $query->result();
    }

    public function fetch_egreso(){
        $this->db
            ->select('dte.monto')
            ->from('transaccion')
            ->join('dte','transaccion.id_dte=dte.id_dte')
            ->where('transaccion.id_tipo_io',2)
            ->order_by('dte.fecha_emision','DESC');


        $query=$this->db->get();
        return $query->result();
    }





    public function fetch_egreso_dia(){

        date_default_timezone_set("America/Santiago");

        $this->db
            ->select('dte.monto')
            ->from('transaccion')
            ->join('dte','transaccion.id_dte=dte.id_dte')
            ->where('transaccion.id_tipo_io',2)
            ->where('dte.fecha_emision',date("Y-m-d"));

        $query=$this->db->get();
        return $query->result();
    }
}