<?php
class Producto_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_producto($data)
    {
        return $this->db->insert('producto',$data);
    }

    public function borrar_producto($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_producto',$id)
            ->update('producto');

        return $query;
    }

    public function actualizar_producto($data){

        return $this->db->replace('producto',$data);

    }

    public function get_lista_productos(){

        $this->db
            ->select('producto.*,tipo_producto.tipo')
            ->from('producto')
            ->join('tipo_producto','producto.id_tipo_producto=tipo_producto.id_tipo_producto')
            ->order_by('producto.nombre','ASC')
            ->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }

    public function actualizar_media($id,$v){
        $query=$this->db
            ->set('valor_medio',$v)
            ->where('id_producto',$id)
            ->update('producto');
        return $query;
    }

}