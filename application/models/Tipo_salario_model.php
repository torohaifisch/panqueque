<?php
class Tipo_salario_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function get_lista(){
        $query= $this->db
            ->select('*')
            ->from('tipo_salario')
            ->get();

        return $query->result();
    }
}