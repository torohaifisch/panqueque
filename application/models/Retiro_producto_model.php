<?php
class Retiro_producto_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('retiro_producto',$data);
        return $this->db->insert_id();

    }


    public function get_lista(){

        $this->db
            ->select('solicitud_pedido.*,sucursal.nombre as sucursal,usuario.nombre,usuario.ape_pat,area_trabajo.area')
            ->from('solicitud_pedido')
            ->join('usuario','solicitud_pedido.id_usuario=usuario.id_usuario')
            ->join('sucursal','solicitud_pedido.id_sucursal=sucursal.id_sucursal')
            ->join('area_trabajo','solicitud_pedido.id_area_trabajo=area_trabajo.id_area_trabajo');

        $query=$this->db->get();

        return $query->result();
    }

    public function get_lista_usuario($id)
    {

        $this->db
            ->select('solicitud_pedido.*,sucursal.nombre as sucursal,usuario.nombre,usuario.ape_pat,area_trabajo.area')
            ->from('solicitud_pedido')
            ->join('usuario', 'solicitud_pedido.id_usuario=usuario.id_usuario')
            ->join('sucursal', 'solicitud_pedido.id_sucursal=sucursal.id_sucursal')
            ->join('area_trabajo', 'solicitud_pedido.id_area_trabajo=area_trabajo.id_area_trabajo')
            ->where('estado', 2)
            ->where('solicitud_pedido.id_usuario',$id);

        $query=$this->db->get();

        return $query->result();

    }


    public function get_solicitud($id){

        $this->db
            ->select('solicitud_pedido.*,sucursal.nombre as sucursal,sucursal.id_sucursal,usuario.nombre,usuario.ape_pat,area_trabajo.area,area_trabajo.id_area_trabajo')
            ->from('solicitud_pedido')
            ->join('usuario','solicitud_pedido.id_usuario=usuario.id_usuario')
            ->join('sucursal','solicitud_pedido.id_sucursal=sucursal.id_sucursal')
            ->join('area_trabajo','solicitud_pedido.id_area_trabajo=area_trabajo.id_area_trabajo')
            ->where('id_solicitud_pedido',$id);


        $query=$this->db->get();

        return $query->row();
    }







}