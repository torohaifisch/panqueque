<?php
class Marca_producto_formato_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('marca_producto_formato',$data);
        return $this->db->insert_id();

    }

    public function actualizar($id,$data){
        $this->db->set('sku',$data);
        $this->db->where('id_marca_producto_formato', $id);
        return $this->db->update('marca_producto_formato');

    }



    public function borrar($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_marca_producto',$id)
            ->update('marca_producto');
        return $query;

    }




    public function combinacion_existe($id_m,$id_p){
        $this->db
            ->select('*')
            ->from('marca_producto_formato')
            ->where('id_producto_formato',$id_p)
            ->where('id_marca',$id_m);
        $query=$this->db->get();
        if ($query->num_rows()>0){
            return $query->row()->id_marca_producto_formato;
        }
        return null;

    }


    public function actualizar_media($id,$v){
        $query=$this->db
            ->set('valor_medio',$v)
            ->where('id_marca_producto_formato',$id)
            ->update('marca_producto_formato');
        return $query;
    }

    public function get($id){
        $this->db
            ->select('*')
            ->from('marca_producto_formato')
            ->join('producto_formato','marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato')
            ->where('id_marca_producto_formato',$id);

         $query=$this->db->get();

        return $query->row();
    }

    public function get_lista(){
        $this->db
            ->select('marca_producto_formato.id_marca_producto_formato as id,marca.nombre as marca, producto.nombre as producto, unidad_medida.medida as unidad, producto_formato.cantidad_contenida')
            ->from('marca_producto_formato')
            ->join('marca','marca.id_marca=marca_producto_formato.id_marca')
            ->join('producto_formato','marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato')
            ->join('producto','producto_formato.id_producto=producto.id_producto')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->order_by('marca','ASC');

        $query=$this->db->get();
        return $query->result();
    }

    public function get_producto_por_codigo($id){
        $this->db
            ->select('marca_producto_formato.id_marca_producto_formato as id, marca.nombre as marca, producto.nombre as producto, unidad_medida.medida as unidad, producto_formato.cantidad_contenida')
            ->from('marca_producto_formato')
            ->join('marca','marca.id_marca=marca_producto_formato.id_marca')
            ->join('producto_formato','marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato')
            ->join('producto','producto_formato.id_producto=producto.id_producto')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('sku',$id);

        $query=$this->db->get();

        return $query->row();

    }

    public function get_producto($id){
        $this->db
            ->select('marca_producto_formato.id_marca_producto_formato,marca_producto_formato.sku,marca.nombre as marca,producto.id_producto,producto.nombre as producto,unidad_medida.id_unidad_medida,unidad_medida.medida,producto_formato.cantidad_contenida')
            ->from('marca_producto_formato')
            ->join('marca','marca.id_marca=marca_producto_formato.id_marca')
            ->join('producto_formato','marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato')
            ->join('producto','producto_formato.id_producto=producto.id_producto')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida')

            ->where('marca_producto_formato.id_marca_producto_formato',$id);

        $query=$this->db->get();

        if ($query->num_rows()>0){
            return $query->row();
        }
        return null;
    }
}
