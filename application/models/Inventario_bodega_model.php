<?php
class Inventario_bodega_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        return $this->db->insert('inventario_bodega',$data);
    }



    public function actualizar($id,$data){

        $this->db->where('id_inventario_bodega', $id);
        return $this->db->update('inventario_bodega', $data);
    }


    public function actualizar_cantidad($id_producto,$id_unidad_medida,$id_sucursal,$cantidad){
        return   $this->db
                    ->set('cantidad',$cantidad)
                    ->where('id_producto',$id_producto)
                    ->where('id_unidad_medida',$id_unidad_medida)
                    ->where('id_sucursal',$id_sucursal)
                    ->update('inventario_bodega');



    }


    public function modificar_cantidad($id,$cantidad){
        return   $this->db
            ->set('cantidad',$cantidad)
            ->where('id_inventario_bodega',$id)
            ->update('inventario_bodega');

    }


    public function get_producto($id){
        $this->db
            ->select('unidad_medida.id_unidad_medida,inventario_bodega.id_producto,inventario_bodega.id_inventario_bodega,producto.nombre AS producto,inventario_bodega.cantidad,sucursal.nombre AS sucursal, unidad_medida.medida')
            ->from('inventario_bodega')
            ->where('inventario_bodega.id_inventario_bodega',$id)
            ->join('sucursal','inventario_bodega.id_sucursal=sucursal.id_sucursal')
            ->join('producto','inventario_bodega.id_producto=producto.id_producto')
            ->join('unidad_medida','inventario_bodega.id_unidad_medida=unidad_medida.id_unidad_medida');



        $query=$this->db->get();

        return $query->row();


    }

    public function get_productos(){
        $this->db
            ->select('inventario_bodega.cantidad_critica,inventario_bodega.cantidad_optima,inventario_bodega.id_inventario_bodega,producto.nombre AS producto,inventario_bodega.cantidad,sucursal.nombre AS sucursal,unidad_medida.medida')
            ->from('inventario_bodega')
            ->join('sucursal','inventario_bodega.id_sucursal=sucursal.id_sucursal')
            ->join('producto','inventario_bodega.id_producto=producto.id_producto')
            ->join('unidad_medida','inventario_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('inventario_bodega.activo',1);


        $query=$this->db->get();

        return $query->result();


    }


    public function get_productos_sucursal_marca($id_m,$id_s){

        $this->db
            ->select('inventario_bodega.id_inventario_bodega,producto.nombre AS producto,inventario_bodega.cantidad,unidad_medida.medida')
            ->from('inventario_bodega')
            ->join('producto','inventario_bodega.id_producto=producto.id_producto')
            ->join('unidad_medida','inventario_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('id_sucursal',$id_s)
            ->where('inventario_bodega.activo',1);



        $query=$this->db->get();

        return $query->result();


    }

    public function get_productos_sucursal($id_s){
        $this->db
            ->select('inventario_bodega.cantidad_optima,sucursal.nombre as sucursal,inventario_bodega.cantidad_critica,inventario_bodega.cantidad_normal,inventario_bodega.id_inventario_bodega,producto.nombre AS producto,inventario_bodega.cantidad, unidad_medida.medida')
            ->from('inventario_bodega')
            ->join('sucursal','sucursal.id_sucursal=inventario_bodega.id_sucursal')
            ->join('producto','inventario_bodega.id_producto=producto.id_producto')
            ->join('unidad_medida','inventario_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('inventario_bodega.id_sucursal',$id_s)
            ->where('inventario_bodega.activo',1)
            ->order_by('producto.nombre','ASC');




        $query=$this->db->get();

        return $query->result();

    }


    public function restar_cantidad($id,$cantidad){
       return $this->db
            ->set('cantidad','cantidad-'.$cantidad,FALSE)
            ->where('id_inventario_bodega',$id)
            ->update('inventario_bodega');

    }


    public function actualizar_cantidad_critica($id,$valor){
        return $this->db
            ->set('cantidad_critica',$valor)
            ->where('id_inventario_bodega',$id)
            ->update('inventario_bodega');
    }

    public function actualizar_cantidad_optima($id,$valor){
        return $this->db
            ->set('cantidad_optima',$valor)
            ->where('id_inventario_bodega',$id)
            ->update('inventario_bodega');
    }
    public function actualizar_cantidad_normal($id,$valor){
        return $this->db
            ->set('cantidad_normal',$valor)
            ->where('id_inventario_bodega',$id)
            ->update('inventario_bodega');
    }




    public function puede_retirar_producto($id,$cantidad){
        $this->db
            ->select('*')
            ->from('inventario_bodega')
            ->where('id_inventario_bodega',$id)
            ->where('cantidad >=',$cantidad);
        $query=$this->db->get();

        if ($query->num_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }


    public function existe_producto($id,$id_f,$id_sucursal){
        $this->db
            ->select('*')
            ->from('inventario_bodega')
            ->where('id_producto',$id)
            ->where('id_unidad_medida',$id_f)
            ->where('id_sucursal',$id_sucursal);
        return $query=$this->db->get();
    }

    public function valor_total_estimado($id=null){
        if ($id==null){
            $id=0;
        }
        if ($id!=0){
            $sql='SELECT FLOOR(sum(media*cantidad)) as valor from (SELECT AVG(valor_medio/cantidad_contenida) as media,producto_formato.id_producto
            FROM marca_producto_formato join producto_formato on marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato WHERE valor_medio>0
            GROUP BY producto_formato.id_producto,producto_formato.id_unidad_medida) a join (select sum(cantidad) as cantidad,id_producto
             from inventario_bodega where inventario_bodega.id_sucursal=? GROUP BY id_producto) b on a.id_producto=b.id_producto

';
            $query=$this->db->query($sql,array($id));


        }
        else{
        $query=$this->db->query('SELECT FLOOR(sum(media*cantidad))  as valor from (SELECT AVG(valor_medio/cantidad_contenida) as media,producto_formato.id_producto 
                                  FROM marca_producto_formato join producto_formato on marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato 
                                  WHERE valor_medio>0 GROUP BY producto_formato.id_producto,producto_formato.id_unidad_medida) a 
                                  join (select sum(cantidad) as cantidad,id_producto from inventario_bodega GROUP BY id_producto) b on a.id_producto=b.id_producto
');}
        if ($query->num_rows()>0){
            return $query->row()->valor;

        }
        return 0;
    }
//
//
//
   public function detalle_producto_proveedor($id){

        $sql='SELECT proveedor.nombre as proveedor,sum(cantidad) as cantidad_comprada,round(sum(cantidad*valor_unitario_neto)/sum(cantidad)) as precio_medio FROM transaccion join detalle_transaccion
              on detalle_transaccion.id_transaccion=transaccion.id_transaccion join proveedor on transaccion.id_proveedor=proveedor.id_proveedor WHERE id_marca_producto_formato= ? group by transaccion.id_proveedor';
        $query=$this->db->query($sql,array($id));

        return $query->result();

    }
}