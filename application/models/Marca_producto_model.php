<?php
class Marca_producto_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('marca_producto',$data);
        return $this->db->insert_id();
    }



    public function get_lista(){

        $this->db
            ->select('marca_producto.id_marca_producto,marca.nombre AS marca, producto.nombre AS producto')
            ->from('marca_producto')
            ->join('producto','marca_producto.id_producto=producto.id_producto')
            ->join('marca','marca_producto.id_marca=marca.id_marca');

        $query=$this->db->get();

        return $query->result();
    }


    public function borrar($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_marca_producto',$id)
            ->update('marca_producto');
        return $query;

    }


    public function get_lista_productos($id){
        $this->db
            ->select('marca_producto.*,producto.nombre')
            ->from('marca_producto')
            ->join('producto','marca_producto.id_producto=producto.id_producto')
            ->where('id_marca',$id);
        $query=$this->db->get();

        return $query->result();
    }

    public function combinacion_existe($id_m,$id_p){
        $this->db
            ->select('*')
            ->from('marca_producto')
            ->where('id_producto',$id_p)
            ->where('id_marca',$id_m);
        $query=$this->db->get();

        return $query->num_rows();
    }


    public function actualizar_media($id,$v){
        $query=$this->db
            ->set('valor_medio',$v)
            ->where('id_marca_producto',$id)
            ->update('marca_producto');
        return $query;
    }
}
