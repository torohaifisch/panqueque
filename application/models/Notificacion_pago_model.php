<?php
class Notificacion_pago_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('notificacion_pago',$data);
        return $this->db->insert_id();

    }

    public function get_lista(){
        $query=$this->db
            ->select('notificacion_pago.*,tipo_pago.tipo,usuario.nombre,usuario.ape_pat')
            ->from('notificacion_pago')
            ->join('tipo_pago','notificacion_pago.id_tipo_pago=tipo_pago.id_tipo_pago')
            ->join('usuario','usuario.id_usuario=notificacion_pago.responsable')
            ->get();
        return $query->result();
    }

    public function get_notificacion_pago($id){
        $query=$this->db
            ->select('notificacion_pago.*,tipo_pago.tipo,usuario.nombre,usuario.ape_pat')
            ->from('notificacion_pago')
            ->join('tipo_pago','notificacion_pago.id_tipo_pago=tipo_pago.id_tipo_pago')
            ->join('usuario','usuario.id_usuario=notificacion_pago.responsable')
            ->where('id_notificacion_pago',$id)
            ->get();
        return $query->row();
    }


}