<?php
class Area_trabajo_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function crear($data){
        return $this->db->insert('area_trabajo',$data);
    }



    public function get_lista(){
        $query= $this->db
            ->select('*')
            ->from('area_trabajo')
            ->get();

        return $query->result();
    }


    public function get_area($id){
        $query= $this->db
            ->select('*')
            ->from('area_trabajo')
            ->where('id_area_trabajo',$id)
            ->get();

        return $query->row();
    }
}