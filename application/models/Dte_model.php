<?php
class Dte_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_dte($data)
    {
        $this->db->insert('dte',$data);
        return $this->db->insert_id();
    }

    public function borrar_dte($id){

        return $this->db
            ->delete('dte')
            ->where('id',$id);


    }

    public function actualizar_dte($data){

        return $this->db->replace('dte',$data);

    }


    public function listar_dte(){
        return $this->db
            ->select('*')
            ->from('dte');
    }


    public function get_eventos_pagar($inicio,$fin){
        $query=$this->db
            ->select('COUNT(id_dte) as cantidad,fecha_vencimiento')
            ->from('dte')
            ->where('estado',0)
            ->group_by('fecha_vencimiento')
            ->where('fecha_vencimiento>=',$inicio)
            ->where('fecha_vencimiento<=',$fin)

            ->get();

        return $query;
    }

    public function estado_pagado($id){
        $query=$this->db
            ->set('estado',1)
            ->where('id_dte',$id)
            ->update('dte');

        return $query;


    }

    public function get_eventos_pagar_dia($dia){
        $query=$this->db
            ->select('dte.id_dte,dte.monto,dte.numero_dte,proveedor.nombre')
            ->from('dte')
            ->join ('transaccion','transaccion.id_dte=dte.id_dte')
            ->join('proveedor','transaccion.id_proveedor=proveedor.id_proveedor')
            ->where('fecha_vencimiento',$dia)
            ->get();

        return $query->result();
    }
}