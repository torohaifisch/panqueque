<?php
class Tipo_venta_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {

        return $this->db->insert('tipo_venta',$data);
    }



    public function get_lista_tipos(){

        $this->db
            ->select('*')
            ->from('tipo_venta')
            ->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }


}