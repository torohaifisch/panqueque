<?php
class Sucursal_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function get_lista(){

        $this->db
            ->select('*')
            ->from('sucursal');
            //->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }


    public function get_sucursal($id){
        $this->db
            ->select('*')
            ->from('sucursal')
            ->where('id_sucursal',$id);

        $query=$this->db->get();

        return $query->row();

    }



    public function get_criticos_sucursal_mini(){
        $sql='SELECT count(mini_bodega.id_producto) as cuenta, mini_bodega.id_sucursal, sucursal.nombre FROM mini_bodega 
        join sucursal on mini_bodega.id_sucursal=sucursal.id_sucursal where sub_stock<cantidad_critica group by sucursal.id_sucursal';


        $query=$this->db->query($sql);

        return $query->result();

    }


    public function get_criticos_mini(){
        $sql='SELECT count(mini_bodega.id_producto) as cuenta, sucursal.nombre FROM mini_bodega 
        join sucursal on mini_bodega.id_sucursal=sucursal.id_sucursal where sub_stock<cantidad_critica';


        $query=$this->db->query($sql);

        return $query->row()->cuenta;

    }


}