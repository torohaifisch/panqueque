<?php
class Alerta_sobreprecio_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        return $this->db->insert('alerta_sobreprecio',$data);
    }




    public function get_lista(){

        $this->db
            ->select('alerta_sobreprecio.*,producto.nombre as producto, marca_producto_formato.valor_medio,unidad_medida.medida,producto_formato.cantidad_contenida,marca.nombre as marca,')
            ->from('alerta_sobreprecio')
            ->join('marca_producto_formato','alerta_sobreprecio.id_marca_producto_formato=marca_producto_formato.id_marca_producto_formato')
            ->join('marca','marca_producto_formato.id_marca=marca.id_marca')
            ->join('producto_formato','marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato')
            ->join('producto','producto_formato.id_producto=producto.id_producto')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida');

        $query=$this->db->get();

        return $query->result();
    }



}