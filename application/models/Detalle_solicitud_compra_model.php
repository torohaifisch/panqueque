<?php
class Detalle_solicitud_compra_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('detalle_solicitud_compra',$data);

    }

    public function borrar($id){

        return $this->db
            ->where('id',$id)
            ->delete('detalle_solicitud_compra');



    }

    public function listar_detalles($id){
        $this->db
            ->select('detalle_solicitud_compra.cantidad,unidad_medida.medida,producto.nombre as producto,producto_formato.cantidad_contenida,marca.nombre as marca')
            ->from('detalle_solicitud_compra')
            ->join('marca_producto_formato','detalle_solicitud_compra.id_marca_producto_formato=marca_producto_formato.id_marca_producto_formato')
            ->join('marca','marca_producto_formato.id_marca=marca.id_marca')
            ->join('producto_formato','marca_producto_formato.id_producto_formato=producto_formato.id_producto_formato')
            ->join('producto','producto_formato.id_producto=producto.id_producto')
            ->join('unidad_medida','producto_formato.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('id_solicitud_compra',$id);

        $query=$this->db->get();
        return $query->result();
    }


    public function valor_total_detalle($id){

        $sql='SELECT SUM(cantidad*valor_medio) AS total FROM detalle_solicitud_compra JOIN marca_producto_formato 
        ON detalle_solicitud_compra.id_marca_producto_formato=marca_producto_formato.id_marca_producto_formato WHERE detalle_solicitud_compra.id_solicitud_compra = ?';
        $query=$this->db->query($sql,array($id));
        return $query->row()->total;
    }

}