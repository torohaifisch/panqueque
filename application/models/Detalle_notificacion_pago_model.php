<?php
class Detalle_notificacion_pago_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('detalle_notificacion_pago',$data);

    }

    public function get_lista($id){
        $query=$this->db
            ->select('dte.id_dte,tipo_dte.tipo as tipo_dte,proveedor.nombre as proveedor,dte.monto,dte.numero_dte')
            ->from('detalle_notificacion_pago')
            ->join('dte','detalle_notificacion_pago.id_dte=dte.id_dte')
            ->join('tipo_dte','dte.id_tipo_dte=tipo_dte.id_tipo_dte')
            ->join('transaccion','transaccion.id_dte=dte.id_dte')
            ->join('proveedor','transaccion.id_proveedor=proveedor.id_proveedor')
            ->where('id_notificacion_pago',$id)
            ->get();
        return $query->result();
    }

}