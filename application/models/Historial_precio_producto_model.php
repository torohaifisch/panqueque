<?php
class historial_precio_producto_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        return $this->db->insert('historial_precio_producto',$data);
    }


    public function precio_medio($id){

        $this->db->query('SET lc_time_names = \'es_ES\'');
        $sql='select AVG(valor) as media from historial_precio_producto WHERE fecha>= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH) AND  id_marca_producto_formato= ?';
        $query=$this->db->query($sql,array($id));
        return $query->row()->media;
    }

    public function get_ultimo($id){
        $this->db
            ->select('valor')
            ->from('historial_precio_producto')
            ->where('id_marca_producto_formato',$id)
            ->order_by('id_historial_precio_producto','DESC')
            ->limit(2);

        $query=$this->db->get();
        if ($query->num_rows()>0){
            return $query->row()->valor;
        }
        else{
            return 0;
        }
    }

}