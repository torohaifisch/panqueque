<?php
class Usuario_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_usuario($data){
        return $this->db->insert('usuario',$data);
    }

    public function borrar($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_usuario',$id)
            ->update('usuario');

        return $query;

    }

    public function actualizar($id,$data){

        $this->db->where('id_usuario', $id);
        return $this->db->update('usuario', $data);

    }

    public function get_lista(){


        $sql='SELECT usuario.*,sucursal.nombre as sucursal,area_trabajo.area,tipo_usuario.nombre as tipo FROM usuario LEFT join sucursal on usuario.id_sucursal=sucursal.id_sucursal left join area_trabajo on usuario.id_area= area_trabajo.id_area_trabajo join tipo_usuario on usuario.tipo=tipo_usuario.id_tipo_usuario WHERE usuario.activo=1
';
        $query=$this->db->query($sql);

        return $query->result();
    }

    public function get_usuario($id){

        $this->db
            ->select('*')
            ->from('usuario')
            ->where('id_usuario',$id);

        $query=$this->db->get();

        return $query->row();
    }




}