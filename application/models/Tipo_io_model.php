<?php
class Tipo_io_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function get_lista_tipo_io(){
        $query= $this->db
            ->select('*')
            ->from('tipo_io')
            ->get();

        return $query->result();
    }
}