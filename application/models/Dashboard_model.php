<?php
class Dashboard_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }


    public function fetch_ingreso_acum_mes(){

        $this->db->query('SET lc_time_names = \'es_ES\'');
        $query=$this->db->query('SELECT SUM(monto) as acum,MONTHNAME(fecha) as mes FROM(
                SELECT monto,fecha FROM venta_local UNION SELECT monto,fecha FROM venta_empresa) as res 
                WHERE fecha > DATE_SUB(CURRENT_DATE, INTERVAL 6 MONTH) GROUP BY YEAR(fecha), MONTH(fecha)');
        return $query->result();

    }

    public function fetch_egreso_acum_mes(){
        $this->db->query('SET lc_time_names = \'es_ES\'');
        $query=$this->db->query('select SUM(monto) as acum,MONTHNAME(fecha_emision) as mes
                                from transaccion JOIN dte on transaccion.id_dte=dte.id_dte 
                                WHERE fecha_emision > DATE_SUB(CURRENT_DATE, INTERVAL 6 MONTH) GROUP BY YEAR(fecha_emision), MONTH(fecha_emision)');
        return $query->result();
    }


}

