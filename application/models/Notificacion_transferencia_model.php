<?php
class Notificacion_transferencia_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('notificacion_transferencia',$data);
        return $this->db->insert_id();

    }




    public function get_lista(){

        $this->db
            ->select('notificacion_transferencia.*,o.nombre as origen, d.nombre as destino,usuario.nombre,usuario.ape_pat')
            ->from('notificacion_transferencia')
            ->join('usuario','notificacion_transferencia.id_usuario=usuario.id_usuario')
            ->join('sucursal as o','notificacion_transferencia.id_origen=o.id_sucursal')
            ->join('sucursal as d','notificacion_transferencia.id_destino=d.id_sucursal');



        $query=$this->db->get();

        return $query->result();
    }

    public function get_transferencia($id){

        $this->db
            ->select('notificacion_transferencia.*,o.nombre as origen,o.id_sucursal as id_origen, d.nombre as destino,d.id_sucursal as id_destino,usuario.nombre,usuario.ape_pat')
            ->from('notificacion_transferencia')
            ->join('usuario','notificacion_transferencia.id_usuario=usuario.id_usuario')
            ->join('sucursal as o','notificacion_transferencia.id_origen=o.id_sucursal')
            ->join('sucursal as d','notificacion_transferencia.id_destino=d.id_sucursal')
            ->where('id_notificacion_transferencia',$id);


        $query=$this->db->get();

        return $query->row();
    }


    public function transferencias_pendientes(){
        $this->db
            ->select('*')
            ->from('notificacion_transferencia')
            ->where('estado',2);
        $query=$this->db->get();

        return $query->num_rows();
    }

    public function aceptar($id){
        $query=$this->db
            ->set('estado',1)
            ->where('id_notificacion_transferencia',$id)
            ->update('notificacion_transferencia');

        return $query;

    }
    public function rechazar($id){
        $query=$this->db
            ->set('estado',0)
            ->where('id_notificacion_transferencia',$id)
            ->update('notificacion_transferencia');

        return $query;

    }

}