<?php
class Trabajador_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        return $this->db->insert('trabajador',$data);
    }

    public function borrar($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_trabajador',$id)
            ->update('trabajador');

        return $query;

    }

    public function actualizar($id,$data){

        $this->db->where('id_trabajador', $id);
        return $this->db->update('trabajador', $data);

    }

    public function get_lista(){

        $this->db
            ->select('trabajador.*,tipo_salario.tipo,area_trabajo.area')
            ->from('trabajador')
            ->join('tipo_salario','trabajador.id_tipo_salario=tipo_salario.id_tipo_salario')
            ->join('area_trabajo','trabajador.id_area_trabajo=area_trabajo.id_area_trabajo')
            ->where('activo',1);

        $query=$this->db->get();

        return $query->result();
    }

    public function get_trabajador($id){

        $this->db
            ->select('*')
            ->from('trabajador')
            ->where('id_trabajador',$id);

        $query=$this->db->get();

        return $query->row();
    }




}