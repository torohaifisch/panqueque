<?php
class Detalle_notificacion_transferencia_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('detalle_notificacion_transferencia',$data);

    }

    public function borrar($id){

        return $this->db
            ->where('id',$id)
            ->delete('detalle_notificacion_transferencia');



    }

    public function listar_detalles($id){
        $this->db
            ->select('detalle_notificacion_transferencia.cantidad,unidad_medida.medida,producto.nombre as producto,producto.id_producto,unidad_medida.id_unidad_medida')
            ->from('detalle_notificacion_transferencia')
            ->join('inventario_bodega','inventario_bodega.id_inventario_bodega=detalle_notificacion_transferencia.id_inventario_bodega')
            ->join('producto','inventario_bodega.id_producto=producto.id_producto')
            ->join('unidad_medida','inventario_bodega.id_unidad_medida=unidad_medida.id_unidad_medida')
            ->where('id_notificacion_transferencia',$id);

        $query=$this->db->get();
        return $query->result();
    }




}