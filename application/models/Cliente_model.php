<?php
class Cliente_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear_cliente($data)
    {
        return $this->db->insert('cliente',$data);
    }

    public function borrar_cliente($id){

        $query=$this->db
            ->set('activo',0)
            ->where('id_cliente',$id)
            ->update('cliente');

        return $query;

    }

    public function actualizar_cliente($id,$data){

        $this->db->where('id_cliente', $id);
        return $this->db->update('cliente', $data);

    }

    public function get_lista_clientes(){

         $this->db
            ->select('*')
            ->from('cliente')
            ->where('activo',1);

         $query=$this->db->get();

         return $query->result();
    }

    public function get_cliente($id){

        $this->db
            ->select('*')
            ->from('cliente')
            ->where('id_cliente',$id);

        $query=$this->db->get();

        return $query->row();
    }




}