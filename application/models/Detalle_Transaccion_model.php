<?php
class Detalle_Transaccion_model extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function crear($data)
    {
        $this->db->insert('detalle_transaccion',$data);

    }

    public function borrar($id){

        return $this->db
            ->where('id',$id)
            ->delete('detalle_transaccion');



    }




    public function listar($id_transaccion){
        return $this->db
            ->select('*')
            ->from('detalle_transaccion')
            ->where('id_transaccion',$id_transaccion);
    }
}