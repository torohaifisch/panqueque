<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta_empresa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }


        $this->load->model('Venta_empresa_model');

    }

    public function index()
    {
        $data['main_view']='venta_empresa';
        $data['titulo']= 'Venta Empresa';

        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {


        /* Transaccion */


        $data['id_cliente'] = $this->input->post('cliente');
        $data['monto'] = $this->input->post('monto');
        $data['fecha'] = $this->input->post('fecha');

        $this->db->trans_start();


        $this->Venta_empresa_model->crear($data);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al crear Transaccion ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Transaccion creada correctamente');

        }
        redirect('venta_empresa');



    }


    public function fetch_acum_mes(){
        $result=$this->Venta_empresa_model->fetch_acum_mes();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }

}