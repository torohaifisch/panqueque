<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }
        $this->load->model('Proveedor_model');
    }

    public function index()
    {
        $data['main_view']='proveedor';
        $data['titulo']= 'Proveedores';

        $data['proveedores']=$this->lista_proveedores();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('rut', 'Rut', 'trim|is_unique[proveedor.rut]');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('mail', 'Mail', 'trim|valid_email');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim');

        $data['nombre'] = $this->input->post('nombre');
        $data['rut'] = $this->input->post('rut');
        $data['telefono'] = $this->input->post('telefono');
        $data['mail'] = $this->input->post('mail');




        if ($this->form_validation->run() == FALSE) {

            if ($this->input->is_ajax_request()) {
                echo '0';
            }

            else {

                $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro proveedor.');
                redirect('proveedor');

                /*set flash y redirect*/
            }


        }
        elseif($result= $this->Proveedor_model->crear_proveedor($data)){

            if ($this->input->is_ajax_request()) {
                echo '1';
            }
            else {
                $this->session->set_flashdata('success_msg', 'Proveedor añadido correctamente');
                redirect('proveedor');
            }

        }

        else
        {
            if ($this->input->is_ajax_request()) {
                echo '0';
            }
            else {
                $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');
                redirect('proveedor');
            }
        }









    }

    public function editar($id){

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data['nombre'] = $this->input->post('nombre');
            $data['rut'] = $this->input->post('rut');
            $data['telefono'] = $this->input->post('telefono');
            $data['mail'] = $this->input->post('mail');
            $viejo_rut = $this->input->post('viejo_rut');





            if ($viejo_rut==$data['rut']){

                $this->Proveedor_model->actualizar_proveedor($id,$data);

                $this->session->set_flashdata('success_msg', 'Proveedor actualizado correctamente');
                redirect('proveedor');
            }



            $this->form_validation->set_rules('rut', 'Rut', 'trim|is_unique[proveedor.rut]');

            if ($this->form_validation->run() == FALSE) {


                $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro proveedor.');
                $result=$this->get_proveedor($id);
                $data['proveedor']=$result;
                $data['main_view'] = 'editar_proveedor';
                $this->load->view('layouts/main', $data);


            }
            else{

                $this->Proveedor_model->actualizar_proveedor($id,$data);

                $this->session->set_flashdata('success_msg', 'Proveedor actualizado correctamente');
                redirect('proveedor');

            }



        }
        else{
            $result=$this->get_proveedor($id);

            if ($result){
                $data['main_view'] = 'editar_proveedor';
                $data['proveedor']=$result;
                $this->load->view('layouts/main', $data);

            }
            else{
                redirect('proveedor');
            }



        }



    }

    public function borrar($id){
        $this->Proveedor_model->borrar_proveedor($id);
        redirect('proveedor');
    }


    public function lista_proveedores(){

        $result=$this->Proveedor_model->get_lista_proveedores();
        return $result;
    }

    public function fetch_proveedores(){
        $result=$this->Proveedor_model->get_lista_proveedores();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }


    function check_proveedor_rut($rut,$id) {

        $result = $this->Proveedor_model->check_unique_rut($id, $rut);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_proveedor_rut', 'Rut ya utilizado');
            $response = false;
        }
        return $response;
    }

    function get_proveedor($id){
        $result=$this->Proveedor_model->get_proveedor($id);
        return $result;
    }
}