<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alerta_sobreprecio extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Alerta_sobreprecio_model');


    }

    public function index()
    {
        $data['main_view']='alerta_sobreprecio';
        $data['titulo']= 'Alerta Sobreprecio';
        $data['alertas']=$this->get_lista_alertas();
        $this->load->view('layouts/main',$data);
    }




    function get_lista_alertas(){
        return $this->Alerta_sobreprecio_model->get_lista();

    }



}