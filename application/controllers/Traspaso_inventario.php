<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traspaso_inventario extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }


        $this->load->model('Inventario_bodega_model');
        $this->load->model('Notificacion_transferencia_model');
        $this->load->model('Detalle_notificacion_transferencia_model');
        $this->load->model('Sucursal_model');


    }

    public function index()
    {
        $data['main_view']='traspaso_inventario/nuevo';
        $data['titulo']= 'Traspaso de Inventario';
        $data['sucursales']=$this->fetch_sucursales();
        $this->load->view('layouts/main',$data);
    }

    public function lista()
    {
        $data['main_view']='traspaso_inventario/lista';
        $data['titulo']= 'Traspaso Inventario - Lista';
        $data['solicitudes']=$this->lista_notificaciones();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {
        date_default_timezone_set("America/Santiago");


        /* solicitud compra */
        $data['id_usuario'] = $this->session->userdata('id');
        $data['id_origen'] = $this->input->post('origen');
        $data['id_destino'] = $this->input->post('destino');
        $data['fecha'] = date("Y-m-d H:i:s");

        $this->db->trans_begin();
        /* crear solicitud para obtener id */
        $id_s=$this->Notificacion_transferencia_model->crear($data);
        $data_detalle['id_notificacion_transferencia']=$id_s;





        $producto=$this->input->post('producto');
        $cantidad=$this->input->post('cantidad');

        foreach($producto as $key=>$value){

            $k=$this->Inventario_bodega_model->puede_retirar_producto($value,$cantidad[$key]);
            if ($k==true){
                $this->Inventario_bodega_model->restar_cantidad($value,$cantidad[$key]);
            }
            else{
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_msg', 'Error No hay stock');
                redirect('traspaso_inventario');

            }


            /*crear detalles*/

            $data_detalle['id_inventario_bodega']=$value;
            $data_detalle['cantidad']=$cantidad[$key];


            $this->Detalle_notificacion_transferencia_model->crear($data_detalle);


        }


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_msg', 'Error al Ingresar datos');

        }
        else{
            $this->db->trans_commit();
            $this->session->set_flashdata('success_msg', 'Notificacion ingresada correctamente');

        }
        redirect('traspaso_inventario');



    }



    public function ver($id)
    {

        $result=$this->get_notificacion($id);

        if ($result){
            $data['titulo']= 'Notificacion de Transferencia - Ver';

            $data['main_view'] = 'traspaso_inventario/ver';
            $data['notificacion']=$result;
            $data['detalles']=$this->get_detalles($id);
            $this->load->view('layouts/main', $data);

        }
        else{
            redirect('traspaso_inventario/lista');
        }





    }






    function get_notificacion($id){
        $result=$this->Notificacion_transferencia_model->get_transferencia($id);
        return $result;
    }

    function lista_notificaciones(){
        $result=$this->Notificacion_transferencia_model->get_lista();
        return $result;
    }

    function get_detalles($id){
        $result=$this->Detalle_notificacion_transferencia_model->listar_detalles($id);
        return $result;

    }

    function aceptar($id){


        $notif=$this->get_notificacion($id);
        if ($notif->estado==1){
            $this->session->set_flashdata('error_msg', 'Transferencia ya aceptada ');
            redirect('traspaso_inventario/lista');

        }
        $this->db->trans_start();
        $this->Notificacion_transferencia_model->aceptar($id);
        $det=$this->get_detalles($id);
        foreach ($det as $detalle){
            $res=$this->Inventario_bodega_model->existe_producto($detalle->id_producto,$detalle->id_unidad_medida,$notif->id_destino);
            if ($res->num_rows()>0){
                $stock=$detalle->cantidad+$res->row()->cantidad;
                $this->Inventario_bodega_model->actualizar_cantidad($detalle->id_producto,$detalle->id_unidad_medida,$notif->id_destino,$stock);
            }
            else{
                $data_inv['id_sucursal']=$notif->id_destino;
                $data_inv['id_producto']=$detalle->id_producto;
                $data_inv['id_unidad_medida']=$detalle->id_unidad_medida;
                $data_inv['cantidad']=$detalle->cantidad;
                $this->Inventario_bodega_model->crear($data_inv);
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al crear transferencia ');
            redirect('traspaso_inventario/lista');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Transferencia creada correctamente');

            redirect('traspaso_inventario/lista');


        }



    }

    function rechazar($id){

        $notif=$this->get_notificacion($id);
        if ($notif->estado==0){
            $this->session->set_flashdata('error_msg', 'Transferencia ya rechazada ');
            redirect('traspaso_inventario/lista');

        }


        $this->db->trans_start();
        $this->Notificacion_transferencia_model->rechazar($id);
        $det=$this->get_detalles($id);
        foreach ($det as $detalle){
            $res=$this->Inventario_bodega_model->existe_producto($detalle->id_producto,$detalle->id_unidad_medida,$notif->id_origen);
            if ($res->num_rows()>0){
                $stock=$detalle->cantidad+$res->row()->cantidad;
                $this->Inventario_bodega_model->actualizar_cantidad($detalle->id_producto,$detalle->id_unidad_medida,$notif->id_origen,$stock);
            }
            else{
                $data_inv['id_sucursal']=$notif->id_origen;
                $data_inv['id_producto']=$detalle->id_producto;
                $data_inv['id_unidad_medida']=$detalle->id_unidad_medida;
                $data_inv['cantidad']=$detalle->cantidad;
                $this->Inventario_bodega_model->crear($data_inv);
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al rechazar transferencia ');
            redirect('traspaso_inventario/lista');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Transferencia rechazada correctamente');

            redirect('traspaso_inventario/lista');


        }









        redirect('traspaso_inventario/lista');
    }



    function fetch_sucursales(){
        $result=$this->Sucursal_model->get_lista();
        return $result;

    }

}