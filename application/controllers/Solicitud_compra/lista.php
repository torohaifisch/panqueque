<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class lista extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Solicitud_compra_model');

    }

    public function index()
    {
        $data['main_view']='solicitud_de_compra/lista';
        $data['titulo']= 'Solicitud Compra - Lista';
        $data['solicitudes']=$this->lista();
        $this->load->view('layouts/main',$data);
    }


    function lista(){
        $result=$this->Solicitud_compra_model->get_lista();
        return $result;
    }






}