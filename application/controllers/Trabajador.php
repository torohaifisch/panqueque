<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trabajador extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }
        $this->load->model('Trabajador_model');
        $this->load->model('Area_trabajo_model');
        $this->load->model('Tipo_salario_model');
    }

    public function index()
    {
        $data['main_view']='trabajador';
        $data['titulo']= 'Trabajadores';

        $data['trabajadores']=$this->lista();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('rut', 'Rut', 'trim|is_unique[trabajador.rut]');
            $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');


            $data['nombre'] = $this->input->post('nombre');
            $data['rut'] = $this->input->post('rut');
            $data['id_tipo_salario'] = $this->input->post('tipo_salario');
            $data['id_area_trabajo'] = $this->input->post('area');


            if ($this->form_validation->run() == FALSE) {

                if ($this->input->is_ajax_request()) {
                    echo '0';
                }

                else {
                    $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro trabajador.');
                    redirect('trabajador');
                    /*set flash y redirect*/
                }


            }

            elseif($result = $this->Trabajador_model->crear($data)){

                if ($this->input->is_ajax_request()) {
                    echo '1';
                }
                else {
                    $this->session->set_flashdata('success_msg', 'Trabajador ingresado correctamente');
                    redirect('trabajador');
                }

            }

            else
            {
                if ($this->input->is_ajax_request()) {
                    echo '0';
                }
                else {
                    $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');
                    redirect('trabajador');
                }
            }
    }

    public function borrar($id){
        $this->Trabajador_model->borrar($id);
        redirect('trabajador');
    }


    public function editar($id)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {


            $this->form_validation->set_rules('nombre', 'Nombre', 'trim');

            $data['nombre'] = $this->input->post('nombre');
            $data['rut'] = $this->input->post('rut');
            $viejo_rut=$this->input->post('viejo_rut');


            if ($viejo_rut==$data['rut']){

                $this->Trabajador_model->actualizar($id,$data);

                $this->session->set_flashdata('success_msg', 'Trabajador actualizado correctamente');
                redirect('trabajador');

            }


            $this->form_validation->set_rules('rut', 'Rut', 'trim|is_unique[trabajador.rut]');


            if ($this->form_validation->run() == FALSE) {


                $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro trabajador.');
                $result=$this->get_trabajador($id);
                $data['trabajador']=$result;
                $data['main_view'] = 'editar_trabajador';
                $data['titulo']= 'Editar Trabajador';
                $data['areas']=$this->get_areas();
                $data['tipos_salario']=$this->get_tipo_salario();
                $this->load->view('layouts/main', $data);


            }
            else{
                $this->Trabajador_model->actualizar($id,$data);

                $this->session->set_flashdata('success_msg', 'Trabajador actualizado correctamente');
                redirect('trabajador');

            }




        }
        else{
            $result=$this->get_trabajador($id);

            if ($result){
                $data['main_view'] = 'editar_trabajador';
                $data['trabajador']=$result;
                $data['titulo']= 'Editar Trabajador';
                $data['areas']=$this->get_areas();
                $data['tipos_salario']=$this->get_tipo_salario();
                $this->load->view('layouts/main', $data);

            }
            else{
                redirect('trabajador');
            }



        }

    }


    public function lista(){

        $result=$this->Trabajador_model->get_lista();
        return $result;
    }

    public function fetch_trabajadores(){
        $result=$this->Trabajador_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }


    function get_areas(){
        $result=$this->Area_trabajo_model->get_lista();
        return $result;
    }

    function get_tipo_salario(){
        $result=$this->Tipo_salario_model->get_lista();
        return $result;
    }

    function get_trabajador($id){
        $result=$this->Trabajador_model->get_trabajador($id);
        return $result;
    }


}