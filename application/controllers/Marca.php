<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marca extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }

        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }

        $this->load->model('Marca_model');
    }

    public function index()
    {
        $data['main_view']='marca';
        $data['titulo']= 'Marcas';

        $data['marcas']=$this->lista_marcas();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {

        $this->load->library('form_validation');



        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|is_unique[marca.nombre]');
        $data['nombre'] = $this->input->post('nombre');


        if ($this->form_validation->run() == FALSE) {

            if ($this->input->is_ajax_request()) {
                echo '0';
                return;
            } else {

                $this->session->set_flashdata('error_msg', 'Error: La marca ya existe');
                redirect('marca');
                /*set flash y redirect*/
            }
        }


        elseif($result= $this->Marca_model->crear_marca($data)){
            if ($this->input->is_ajax_request()) {
                echo '1';
                return;
            }
            else {
                $this->session->set_flashdata('success_msg', 'Marca añadida correctamente');
                redirect('marca');
            }
        }
        else{
            if ($this->input->is_ajax_request()) {
                echo '0';
                return;
            }

            else {
                $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');

                redirect('marca');
                /*set flash y redirect*/


            }

        }



    }




    public function borrar($id){
        $this->Marca_model->borrar_marca($id);
        redirect('marca');
    }


    public function lista_marcas(){

        $result=$this->Marca_model->get_lista_marcas();
        return $result;
    }


    public function fetch_marcas(){
        $result=$this->Marca_model->get_lista_marcas();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }
}