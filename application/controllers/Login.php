<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Area_trabajo_model');
        $this->load->model('Tipo_usuario_model');

    }

    public function index()
    {
        $this->load->view('login');
    }

    public function validar()
    {
        $user = $this->input->post('user');
        $pass = md5($this->input->post('pass'));

        $result = $this->Auth_model->validar_usuario($user, $pass);

        if ($result[0]>0) {

            $sesdata= array(
                'id'=>$result[1]->id_usuario,
              'nombre'=> $result[1]->nombre.' '.$result[1]->ape_pat,
                'user'=> $result[1]->nombre_usuario,
                'tipo' => $result[1]->tipo,
                'area'=>$result[1]->id_area,
                'sucursal'=>$result[1]->id_sucursal,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sesdata);
            redirect('home');
        }
        else{
            redirect('login');
            }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }





}
