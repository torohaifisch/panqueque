<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area_trabajo extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }
        $this->load->model('Area_trabajo_model');
    }



    public function index()
    {
        $data['main_view']='area_trabajo';
        $data['titulo']= 'Areas de Trabajo';

        $data['areas']=$this->get_areas();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {

        $this->load->library('form_validation');



        $this->form_validation->set_rules('area', 'Area', 'trim|is_unique[area_trabajo.area]');
        $data['area'] = $this->input->post('area');


        if ($this->form_validation->run() == FALSE) {

            if ($this->input->is_ajax_request()) {
                echo '0';
                return;
            } else {

                $this->session->set_flashdata('error_msg', 'Error: El Area ya existe');
                redirect('area_trabajo');
                /*set flash y redirect*/
            }
        }


        elseif($result= $this->Area_trabajo_model->crear($data)){
            if ($this->input->is_ajax_request()) {
                echo '1';
                return;
            }
            else {
                $this->session->set_flashdata('success_msg', 'Area añadida correctamente');
                redirect('area_trabajo');
            }
        }
        else{
            if ($this->input->is_ajax_request()) {
                echo '0';
                return;
            }

            else {
                $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');

                redirect('area_trabajo');
                /*set flash y redirect*/


            }

        }



    }








    function fetch_areas(){
        $result=$this->Area_trabajo_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    function get_areas(){
        return $result=$this->Area_trabajo_model->get_lista();

    }

}