<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1  ){
            redirect('home');
        }
        $this->load->model('Usuario_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Area_trabajo_model');
        $this->load->model('Tipo_usuario_model');
    }

    public function index()
    {
        $data['main_view']='usuario/lista';
        $data['titulo']= 'Usuarios - Lista';

        $data['usuarios']=$this->lista_usuarios();
        $this->load->view('layouts/main',$data);
    }


    public function nuevo_usuario(){
        if($this->session->userdata('tipo') != 1){
            redirect('home');
        }
        $data['main_view']='usuario/nuevo';
        $data['titulo']= 'Nuevo Usuario';
        $data['sucursales']=$this->get_sucursales();
        $data['areas']=$this->get_areas();
        $data['tipos']=$this->get_tipos();

        $this->load->view('layouts/main',$data);
    }

    public function crear_usuario(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('usuario', 'Nombre_usuario', 'trim|is_unique[usuario.nombre_usuario]');


        $data['nombre'] = $this->input->post('nombre');
        $data['ape_pat'] = $this->input->post('ape_pat');
        $data['pass'] = md5($this->input->post('pass'));
        $data['nombre_usuario'] = $this->input->post('usuario');
        $data['id_sucursal'] = $this->input->post('sucursal');
        $data['id_area'] = $this->input->post('area');
        $data['tipo'] = $this->input->post('tipo');

        if($this->input->post('sucursal')=='') {
            $data['id_sucursal'] = null;
        }

        if($this->input->post('area')=='') {
            $data['id_area'] = null;
        }


        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('error_msg', 'Error: Nombre de usuario ya utilizado.');
            redirect('Usuario/nuevo_usuario');
            /*set flash y redirect*/
        }


        if($result = $this->Usuario_model->crear_usuario($data)){

            $this->session->set_flashdata('success_msg', 'Usuario añadido correctamente');



        }

        else {
            $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');

        }

        redirect('Usuario/nuevo_usuario');
    }



    function get_areas(){
        $result=$this->Area_trabajo_model->get_lista();
        return $result;
    }

    function get_sucursales(){
        $result=$this->Sucursal_model->get_lista();
        return $result;

    }

    function get_tipos(){
        $result=$this->Tipo_usuario_model->get_lista();
        return $result;

    }






    public function lista_usuarios(){

        $result=$this->Usuario_model->get_lista();
        return $result;
    }




}