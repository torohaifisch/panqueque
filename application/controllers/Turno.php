<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turno extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }

        $this->load->model('Retiro_producto_model');
        $this->load->model('Inventario_bodega_model');

        $this->load->model('Detalle_retiro_producto_model');
        $this->load->model('Marca_producto_formato_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Area_trabajo_model');

        $this->no_cache();


    }

    protected function no_cache(){
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
    }


    public function index(){
        if($this->session->userdata('tipo') != 4 ){
            redirect('home');
        }

        $data['main_view']= 'turno/turno';
        $data['titulo']= 'Turno';
        $data['user_id']=$this->session->userdata('id');
        $data['sucursal']=$this->get_sucursal($this->session->userdata('sucursal'));
        $data['area']=$this->get_area($this->session->userdata('area'));


        $this->load->view('layouts/main',$data);


    }


    public function retirar_productos(){
        if($this->session->userdata('tipo') != 4 ){
            redirect('home');
        }

            $data['main_view']='turno/nueva_solicitud';
            $data['titulo']= 'Retirar Productos';
            $data['sucursal']=$this->get_sucursal($this->session->userdata('sucursal'));
            $data['area']=$this->get_area($this->session->userdata('area'));
            $this->load->view('layouts/main',$data);
    }




    public function crear_retiro_productos(){
        date_default_timezone_set("America/Santiago");


        /* solicitud pedido */
        $data['id_usuario'] = $this->session->userdata('id');
        $data['id_sucursal'] = $this->input->post('sucursal');
        $data['id_area_trabajo'] = $this->input->post('area');
        $data['fecha']=date('Y-m-d H:i:s');


        $this->db->trans_begin();


        /* crear solicitud para obtener id */
        $id_s=$this->Retiro_producto_model->crear($data);

        $data_detalle['id_retiro_producto']=$id_s;


        $productos=$this->input->post('producto');
        $cantidad=$this->input->post('cantidad');

        foreach($productos as $key=>$value){


            /*crear detalles*/
            $data_detalle['id_marca_producto_formato']=$value;
            $data_detalle['cantidad_retirada']=$cantidad[$key];


            /* Verificar si existe combinacion */
            $prd=$this->get_producto($value);
            $prd_inventario=$this->Inventario_bodega_model->existe_producto($prd->id_producto,$prd->id_unidad_medida,$data['id_sucursal']);

            if ($prd_inventario->num_rows()>0) {

                $id_inv = $prd_inventario->row()->id_inventario_bodega;
                $k = $this->Inventario_bodega_model->puede_retirar_producto($id_inv, $cantidad[$key]*$prd->cantidad_contenida);
                if ($k == true) {
                    $this->Inventario_bodega_model->restar_cantidad($id_inv, $cantidad[$key]*$prd->cantidad_contenida);
                } else {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('error_msg', 'Error No hay stocks');
                    redirect('turno');

                }


                $this->Detalle_retiro_producto_model->crear($data_detalle);
            }
            else {
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_msg', 'Error No existe producto en inventario');
                redirect('turno');

            }

        }


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_msg', 'Error al Ingresar Solicitud');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('success_msg', 'Solicitud ingresada correctamente');
        }

        redirect('turno');
    }


    public function get_sucursal($id){
        return $result=$this->Sucursal_model->get_sucursal($id);

    }

    public function get_area($id){
        return $result=$this->Area_trabajo_model->get_area($id);

    }

    function get_producto($id){
        return $this->Marca_producto_formato_model->get_producto($id);
    }
}