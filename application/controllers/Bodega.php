<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }

        $protected_methods = array('asignar_producto_area','stock_critico', 'stock_critico_sub_bodega', 'actualizar_stock_critico','actualizar_stock_critico_sub','modificar_cantidad','modificar_stock_mini');

        if($this->session->userdata('tipo')== 3) {

            //grab the controller/method name and compare with protected methods array
            if (in_array($this->router->method, $protected_methods)) {
                redirect('Bodega');
            }
        }


        elseif($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2){
            redirect('home');
        }

        $this->load->model('Inventario_bodega_model');
        $this->load->model('Mini_bodega_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Area_trabajo_model');
        $this->load->model('Asignar_producto_area_model');
        $this->load->model('Detalle_asignar_producto_area_model');
    }

    public function index()
    {
        $data['main_view']='inventario_bodega';
        $data['titulo']= 'Bodega';
        $data['sucursales']=$this->fetch_sucursales();
        $data['crit_mini']=$this->get_crit_mini();
        $data['productos']=$this->get_productos();
        $data['valor_total_estimado']=$this->get_valor_total(0);
        $data['valor_total_estimado_mini']=$this->get_valor_total_mini(0);
        $this->load->view('layouts/main',$data);
    }


    public function detalle_producto($id){
        $data['main_view']='prod/detalle_producto';
        $data['titulo']= 'Bodega - Detalle Producto';


        $data['producto']=$this->get_producto($id);
        $data['proveedores']=$this->get_detalle_producto($id);
        $this->load->view('layouts/main',$data);

    }

    public function stock_critico($id){
        $data['main_view']='prod/stock_critico';
        $data['titulo']= 'Bodega - Stock Critico';


        $data['productos']=$this->get_productos_sucursal($id);
        $data['sucursal']= $this->get_sucursal($id);
        $this->load->view('layouts/main',$data);

    }

    public function stock_critico_sub_bodega($id,$area){
        $data['main_view']='prod/stock_critico_mini';
        $data['titulo']= 'Bodega - Stock Critico Sub Bodega';


        $data['productos']=$this->get_productos_sucursal_area($id,$area);
        $data['sucursal']= $this->get_sucursal($id);
        $data['area']= $this->get_area($area);
        $this->load->view('layouts/main',$data);

    }




    public function generar_documento($id){
        $data['main_view']='prod/documento_compra';
        $data['titulo']= 'Bodega - Generar Documento';


        $data['productos']=$this->get_productos_sucursal($id);
        $data['sucursal']= $this->get_sucursal($id);
        $this->load->view('layouts/main',$data);
    }


    public function mini_bodega($id)
    {
        $data['main_view']='sub_bodega';
        $data['titulo']= 'Sub Bodega';
        $data['sucursal']=$this->get_sucursal($id);
        $data['productos']=$this->get_productos_sucursal_mini($id);
        $data['areas']=$this->get_areas();
        $this->load->view('layouts/main',$data);
    }


    public function actualizar_stock_critico(){
        $criticos= $this->input->post('cantidad_critica');
        $maximos=$this->input->post('cantidad_optima');
        $normal=$this->input->post('cantidad_normal');
        $ids= $this->input->post('id');

        if ($criticos==null){
            $this->session->set_flashdata('error_msg', 'Error: No hay productos');
            redirect('bodega');
        }

        $this->db->trans_start();

        foreach ($criticos as $key=>$value){
            $this->Inventario_bodega_model->actualizar_cantidad_critica($ids[$key],$value);
            $this->Inventario_bodega_model->actualizar_cantidad_optima($ids[$key],$maximos[$key]);
            $this->Inventario_bodega_model->actualizar_cantidad_normal($ids[$key],$normal[$key]);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al actualizar ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Stock Critico Actualizado correctamente');

        }
        redirect('bodega');

    }



    public function actualizar_stock_critico_sub(){
        $a=$this->input->post('area');
        $s=$this->input->post('sucursal');
        $criticos= $this->input->post('cantidad_critica');
        $maximos=$this->input->post('cantidad_optima');
        $ids= $this->input->post('id');
        $this->db->trans_start();

        if ($criticos==null){
            $this->session->set_flashdata('error_msg', 'Error: No hay productos');
            redirect('bodega');
        }

        foreach ($criticos as $key=>$value){
            $this->Mini_bodega_model->actualizar_cantidad_critica($ids[$key],$value);
            $this->Mini_bodega_model->actualizar_cantidad_optima($ids[$key],$maximos[$key]);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al actualizar ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Stock Critico Actualizado correctamente');

        }
        redirect('bodega/stock_critico_sub_bodega/'.$s.'/'.$a);

    }


    public function asignar_producto_area($id,$area){
        $data['main_view']='prod/asignacion_sub';
        $data['titulo']= 'Bodega - Stock Critico Sub Bodega';


        $data['productos']=$this->get_productos_sucursal_area_con_stock($id,$area);
        $data['sucursal']= $this->get_sucursal($id);
        $data['area']= $this->get_area($area);
        $this->load->view('layouts/main',$data);

    }


    public function asig_prod_area()
    {
        date_default_timezone_set("America/Santiago");


        /* solicitud pedido */
        $data['id_usuario'] = $this->session->userdata('id');
        $data['id_sucursal'] = $this->input->post('sucursal');
        $data['id_area_trabajo'] = $this->input->post('area');
        $data['fecha']=date('Y-m-d');

        $data_mini['id_sucursal'] = $this->input->post('sucursal');
        $data_mini['id_area'] = $this->input->post('area');
        $this->db->trans_begin();


        /* crear solicitud para obtener id */
        $id_s=$this->Asignar_producto_area_model->crear($data);

        $data_detalle['id_asignar_producto_area']=$id_s;


        $producto_inventario=$this->input->post('producto');
        $cantidad=$this->input->post('cantidad');

        if ($producto_inventario==null){
            $this->session->set_flashdata('error_msg', 'Error: No hay productos');
            redirect('bodega/mini_bodega/'.$data['id_sucursal']);
        }

        foreach($producto_inventario as $key=>$value){


            /* Verificar si existe combinacion */
            $prd=$this->get_producto($value);

            $k=$this->Inventario_bodega_model->puede_retirar_producto($value,$cantidad[$key]);
            if ($k==true){
                $this->Inventario_bodega_model->restar_cantidad($value,$cantidad[$key]);
            }
            else{
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_msg', 'Error No hay stock');
                redirect('Asignar_productos');

            }

            /*crear detalles*/
            $data_detalle['id_inventario_bodega']=$value;
            $data_detalle['cantidad_asignada']=$cantidad[$key];


            $this->Detalle_asignar_producto_area_model->crear($data_detalle);


            $data_mini['id_producto']=$prd->id_producto;
            $data_mini['id_unidad_medida']=$prd->id_unidad_medida;


            $res=$this->Mini_bodega_model->existe_producto($data_mini['id_producto'],$data_mini['id_unidad_medida'], $data_mini['id_sucursal'],$data_mini['id_area']);
            if ($res->num_rows()>0){
                $this->Mini_bodega_model->sumar_stock($res->row()->id_mini_bodega, $cantidad[$key]);
            }
            else{
                $data_mini['sub_stock']=$cantidad[$key];
                $this->Mini_bodega_model->crear($data_mini);



            }





        }


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_msg', 'Error al Asignar productos');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('success_msg', 'Asignacion ingresada correctamente');
        }

        redirect('bodega/mini_bodega/'.$data['id_sucursal']);



    }

    public function nueva_stock_cero($id,$area){
        $data['main_view']='asignar_productos/nueva_stock_cero';
        $data['titulo']= 'Bodega - Sub Bodega';

        $data['productos']=$this->get_productos_sucursal($id);
        $data['sucursal']= $this->get_sucursal($id);
        $data['area']= $this->get_area($area);
        $this->load->view('layouts/main',$data);

    }


    public function crear_nueva_cero(){
        $data_mini['id_sucursal'] = $this->input->post('sucursal');
        $data_mini['id_area'] = $this->input->post('area');

        $producto_inventario=$this->input->post('producto');
        $this->db->trans_start();

        foreach($producto_inventario as $key=>$value) {


            /* Verificar si existe combinacion */


            /*crear detalles*/
            $prd = $this->get_producto($value);

            $data_mini['id_producto'] = $prd->id_producto;
            $data_mini['id_unidad_medida'] = $prd->id_unidad_medida;
            $data_mini['sub_stock'] = 0;


            $res = $this->Mini_bodega_model->existe_producto($data_mini['id_producto'], $data_mini['id_unidad_medida'], $data_mini['id_sucursal'], $data_mini['id_area']);
            if ($res->num_rows() == 0) {
                $this->Mini_bodega_model->crear($data_mini);


            }
        }



        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al copiar ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Productos copiados correctamente');

        }

        redirect('bodega/mini_bodega/'.$data_mini['id_sucursal']);


    }



    public function get_producto($id){
        $result=$this->Inventario_bodega_model->get_producto($id);
        return $result;

    }


    function get_productos(){

        $result=$this->Inventario_bodega_model->get_productos();
        return $result;
    }

    function get_detalle_producto($id){
        $result=$this->Inventario_bodega_model->detalle_producto_proveedor($id);
        return $result;
    }


    public function fetch_productos_sucursal(){
        $s=$this->input->post('sucursal');
        $result=$this->Inventario_bodega_model->get_productos_sucursal($s);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));

    }

    public function fetch_productos_marca_sucursal(){
        $s=$this->input->post('sucursal');
        $result=$this->Inventario_bodega_model->get_productos_sucursal($s);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));

    }



    public function get_productos_sucursal($id){
        return $result=$this->Inventario_bodega_model->get_productos_sucursal($id);

    }


    public function get_productos_sucursal_area($id,$area){
        return $result=$this->Mini_bodega_model->get_productos_sucursal_area($id,$area);

    }

    public function get_productos_sucursal_area_con_stock($id,$area){
        return $result=$this->Mini_bodega_model->get_productos_sucursal_area_con_stock($id,$area);

    }



    public function get_productos_sucursal_mini($id){
        return $result=$this->Mini_bodega_model->get_productos_sucursal($id);

    }



    function get_valor_total($id){
        $result=$this->Inventario_bodega_model->valor_total_estimado($id);
            return $result;
    }

    function get_valor_total_mini($id){
        $result=$this->Mini_bodega_model->valor_total_estimado($id);
        return $result;
    }



    function fetch_valor_total(){
        $id= $this->input->post('id');
        $result=$this->Inventario_bodega_model->valor_total_estimado($id);
        echo $result;
    }


    function fetch_valor_total_mini(){
        $id= $this->input->post('id');
        $result=$this->Mini_bodega_model->valor_total_estimado($id);
        echo $result;
    }

    function fetch_total_completo(){
        $id= $this->input->post('id');
        $mini=$this->Mini_bodega_model->valor_total_estimado($id);
        $general=$this->Inventario_bodega_model->valor_total_estimado($id);
        echo $mini+$general;
    }


    function modificar_cantidad(){
        $id=$this->input->post('id_inventario');
        $cantidad=$this->input->post('cantidad');
        $res=$this->Inventario_bodega_model->modificar_cantidad($id,$cantidad);
        if ($res){
            $this->session->set_flashdata('success_msg', 'Stock Modificado correctamente');


        }
        else{
            $this->session->set_flashdata('error_msg', 'Error: El stock no se pudo modificar.');

        }
        redirect('bodega');

    }


    function modificar_stock_mini(){
        $id=$this->input->post('id_inventario');
        $cantidad=$this->input->post('cantidad');
        $res=$this->Mini_bodega_model->modificar_stock($id,$cantidad);
        if ($res){
            $this->session->set_flashdata('success_msg', 'Stock Modificado correctamente');


        }
        else{
            $this->session->set_flashdata('error_msg', 'Error: El stock no se pudo modificar.');

        }
        redirect('bodega');

    }




    function fetch_sucursales(){
        $result=$this->Sucursal_model->get_lista();
        return $result;

    }

    function get_sucursal($id){
        $result=$this->Sucursal_model->get_sucursal($id);
        return $result;
    }


    function get_area($id){
        $result=$this->Area_trabajo_model->get_area($id);
        return $result;
    }

    function get_areas(){
        $result=$this->Area_trabajo_model->get_lista();
        return $result;
    }

    function get_crit_mini(){
        $result=$this->Sucursal_model->get_criticos_sucursal_mini();
        return $result;
    }



}