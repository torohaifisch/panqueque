<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaccion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Transaccion_model');
    }

    public function index()
    {
        $data['main_view']='transaccion';
        $data['titulo']= 'Transacciones';

        $data['transacciones']=$this->fetch_transacciones();
        $this->load->view('layouts/main',$data);
    }



    public function fetch_transacciones(){

        $result=$this->Transaccion_model->fetch_transacciones();
        return $result;
    }



}