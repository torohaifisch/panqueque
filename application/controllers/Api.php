<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Inventario_bodega_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Producto_model');
        $this->load->model('Unidad_medida_model');

    }




    public function fetch_productos_sucursal(){
        $s=$this->input->post('sucursal');
        $result=$this->Inventario_bodega_model->get_productos_sucursal($s);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));

    }






}


