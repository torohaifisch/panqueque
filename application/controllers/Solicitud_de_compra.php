<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud_de_compra extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Marca_producto_formato_model');
        $this->load->model('Solicitud_compra_model');
        $this->load->model('Detalle_solicitud_compra_model');

    }

    public function index()
    {
        $data['main_view']='solicitud_de_compra/nueva';
        $data['titulo']= 'Solicitar Compra';
        $this->load->view('layouts/main',$data);
    }



    public function crear()
    {
        date_default_timezone_set("America/Santiago");


        /* solicitud compra */
        $data['id_usuario'] = $this->session->userdata('id');
        $data['id_sucursal'] = $this->input->post('sucursal');
        $data['id_area_trabajo'] = $this->input->post('area');
        $data['valor_estimado'] = 0;
        $data['fecha'] = date("Y-m-d");
        $data['comentario']=$this->input->post('comentario');

        $this->db->trans_start();
        /* crear solicitud para obtener id */
        $id_s=$this->Solicitud_compra_model->crear($data);
        $data_detalle['id_solicitud_compra']=$id_s;





        $marca=$this->input->post('marca');
        $producto_formato=$this->input->post('formato');
        $cantidad=$this->input->post('cantidad');

        foreach($producto_formato as $key=>$value){


            /* Verificar si existe combinacion */

            $data_f['id_marca']=$marca[$key];
            $data_f['id_producto_formato']=$value;
            $k=$this->Marca_producto_formato_model->combinacion_existe($marca[$key],$value);
            if ($k!=null){
                $id_f=$k;
            }
            else{
                $id_f=$this->Marca_producto_formato_model->crear($data_f);

            }

            /*crear detalles*/

            $data_detalle['id_marca_producto_formato']=$id_f;
            $data_detalle['cantidad']=$cantidad[$key];


            $this->Detalle_solicitud_compra_model->crear($data_detalle);


        }

        /* actualizar valor total estimado */
        $tot=$this->Detalle_solicitud_compra_model->valor_total_detalle($id_s);

        $this->Solicitud_compra_model->actualizar_total($id_s,$tot);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al Ingresar Solicitud');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Solicitud ingresada correctamente');

        }
        redirect('solicitud_de_compra');



    }



    public function ver($id)
    {

            $result=$this->get_solicitud($id);

            if ($result){
                $data['titulo']= 'Solicitud de compra - Ver';

                $data['main_view'] = 'solicitud_de_compra/ver';
                $data['solicitud']=$result;
                $data['detalles']=$this->get_detalles($id);
                $this->load->view('layouts/main', $data);

            }
            else{
                redirect('solicitud_compra/lista');
            }





    }


    function get_solicitud($id){
        $result=$this->Solicitud_compra_model->get_solicitud($id);
        return $result;
    }

    function get_detalles($id){
        $result=$this->Detalle_solicitud_compra_model->listar_detalles($id);
        return $result;

    }

    function aceptar_solicitud($id){
        $result=$this->Solicitud_compra_model->aceptar_solicitud($id);
        redirect('solicitud_compra/lista');


    }

    function rechazar_solicitud($id){
        $result=$this->Solicitud_compra_model->rechazar_solicitud($id);
        redirect('solicitud_compra/lista');
    }

}