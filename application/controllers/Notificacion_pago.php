<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacion_pago extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Transaccion_model');
        $this->load->model('Dte_model');
        $this->load->model('Notificacion_pago_model');
        $this->load->model('Detalle_notificacion_pago_model');

    }


    public function index(){
        $data['main_view']='calendario/calendario';
        $data['titulo']= 'Notificacion de Pago';
        $this->load->view('layouts/main',$data);
    }



    public function crear()
    {



        /* notificacion */
        $data_notif['responsable'] = $this->input->post('responsable');
        $data_notif['tipo'] = $this->input->post('tipo');
        $data_notif['numero_documento'] = $this->input->post('numero_doc');
        $data_notif['fecha_pago'] = $this->input->post('fecha');
        $data_notif['monto'] = $this->input->post('total');

        $this->db->trans_start();
        /* crear la DTE para obtener id */
        $id_notif=$this->Notificacion_pago_model->crear($data_notif);

        $data_detalle['id_notificacion_pago']=$id_notif;


        /*crear detalles*/
        $dte=$this->input->post('id_dte');

        foreach($dte as $key=>$value){


            $data_detalle['id_dte']=$value;

            $this->Dte_model->estado_pagado($value);
            $this->Detalle_notificacion_pago_model->crear($data_detalle);


        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al crear Notificacion ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Notificacion creada correctamente');

        }
        redirect('notificacion_pago');



    }


    public function ver_pagados(){
        $data['main_view']='calendario/ver_pagados';
        $data['titulo']= 'Lista Pagadas';
        $data['pagos']= $this->lista_pagados();
        $this->load->view('layouts/main',$data);
    }

    public function ver_detalle($id){
        $data['main_view']='calendario/detalle_pagado';
        $data['titulo']= 'Lista Pagadas';
        $data['notif']= $this->get_notificacion_pago($id);
        $data['detalles']= $this->get_detalle_pago($id);
        $this->load->view('layouts/main',$data);
    }


    public function fetch_eventos_pagar(){


        $start =$this->input->get("start");
        $end = $this->input->get("end");

        $events = $this->Dte_model->get_eventos_pagar($start, $end);
        $counter=0;
        $data_events = array();
        if ($events!=null){
            foreach($events->result() as $r) {

                $data_events[] = array(
                    'id'=>$counter,
                    'title' => 'Pago',
                    'description' => 'Pagos Pendientes',
                    'start' => $r->fecha_vencimiento
                );
                $counter++;
            }

        }


        echo json_encode($data_events);
        exit();


    }

    public function fetch_pagar_dia(){
        $dia=$this->input->post('dia');

        $events=$this->Dte_model->get_eventos_pagar_dia($dia);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($events));
    }


    public function lista_pagados(){
        return $this->Notificacion_pago_model->get_lista();
    }

    public function get_notificacion_pago($id){
        return $this->Notificacion_pago_model->get_notificacion_pago($id);
    }

    public function get_detalle_pago($id){
        return $this->Detalle_notificacion_pago_model->get_lista($id);

    }
}