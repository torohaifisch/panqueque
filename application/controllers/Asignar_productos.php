<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asignar_productos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }
        $this->load->model('Inventario_bodega_model');
        $this->load->model('Asignar_producto_area_model');
        $this->load->model('Detalle_asignar_producto_area_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Mini_bodega_model');

    }

    public function index()
    {
        $data['main_view']='asignar_productos/nueva';
        $data['titulo']= 'Asignar Productos';
        $data['sucursales']=$this->get_sucursales();
        $this->load->view('layouts/main',$data);
    }



    public function crear()
    {
        date_default_timezone_set("America/Santiago");


        /* solicitud pedido */
        $data['id_usuario'] = $this->session->userdata('id');
        $data['id_sucursal'] = $this->input->post('sucursal');
        $data['id_area_trabajo'] = $this->input->post('area');
        $data['fecha']=date('Y-m-d');

        $data_mini['id_sucursal'] = $this->input->post('sucursal');
        $data_mini['id_area'] = $this->input->post('area');
        $this->db->trans_begin();


        /* crear solicitud para obtener id */
        $id_s=$this->Asignar_producto_area_model->crear($data);

        $data_detalle['id_asignar_producto_area']=$id_s;


        $producto_inventario=$this->input->post('producto');
        $cantidad=$this->input->post('cantidad');

        foreach($producto_inventario as $key=>$value){


            /* Verificar si existe combinacion */
            $prd=$this->get_producto($value);

            $k=$this->Inventario_bodega_model->puede_retirar_producto($value,$cantidad[$key]);
            if ($k==true){
                $this->Inventario_bodega_model->restar_cantidad($value,$cantidad[$key]);
            }
            else{
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_msg', 'Error No hay stock');
                redirect('Asignar_productos');

            }

            /*crear detalles*/
            $data_detalle['id_inventario_bodega']=$value;
            $data_detalle['cantidad_asignada']=$cantidad[$key];


            $this->Detalle_asignar_producto_area_model->crear($data_detalle);


            $data_mini['id_producto']=$prd->id_producto;
            $data_mini['id_unidad_medida']=$prd->id_unidad_medida;


            $res=$this->Mini_bodega_model->existe_producto($data_mini['id_producto'],$data_mini['id_unidad_medida'], $data_mini['id_sucursal'],$data_mini['id_area']);
            if ($res->num_rows()>0){
                $this->Mini_bodega_model->sumar_stock($res->row()->id_mini_bodega, $cantidad[$key]);
            }
            else{
                $data_mini['sub_stock']=$cantidad[$key];
                $this->Mini_bodega_model->crear($data_mini);



            }





        }


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_msg', 'Error al Ingresar Solicitud');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('success_msg', 'Solicitud ingresada correctamente');
        }

        redirect('Asignar_productos');



    }






    public function get_sucursales(){
        return $result=$this->Sucursal_model->get_lista();

    }

    function get_producto($id){
        return $result=$this->Inventario_bodega_model->get_producto($id);
    }


}