<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }
        $this->load->model('Cliente_model');
    }

    public function index()
    {
        $data['main_view']='cliente';
        $data['titulo']= 'Clientes';

        $data['clientes']=$this->lista_clientes();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('rut', 'Rut', 'trim|is_unique[cliente.rut]');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('mail', 'Mail', 'trim|valid_email');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim');


        $data['nombre'] = $this->input->post('nombre');
        $data['rut'] = $this->input->post('rut');
        $data['telefono'] = $this->input->post('telefono');
        $data['mail'] = $this->input->post('mail');



        if ($this->form_validation->run() == FALSE) {

            if ($this->input->is_ajax_request()) {
                echo '0';
            }

            else {
                $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro cliente.');
                redirect('proveedor');
                    /*set flash y redirect*/
                }


        }

        elseif($result = $this->Cliente_model->crear_cliente($data)){

            if ($this->input->is_ajax_request()) {
                echo '1';
            }
            else {
                $this->session->set_flashdata('success_msg', 'Cliente añadido correctamente');
                redirect('cliente');
            }

        }

        else
        {
            if ($this->input->is_ajax_request()) {
                echo '0';
            }
            else {
                $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');
                redirect('cliente');
            }
        }


    }

    public function borrar($id){
        $this->Cliente_model->borrar_cliente($id);
        redirect('cliente');
    }


    public function editar($id)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data['nombre'] = $this->input->post('nombre');
            $data['rut'] = $this->input->post('rut');
            $viejo_rut = $this->input->post('viejo_rut');
            $data['telefono'] = $this->input->post('telefono');
            $data['mail'] = $this->input->post('mail');



            if ($viejo_rut==$data['rut']){

                $this->Cliente_model->actualizar_cliente($id,$data);

                $this->session->set_flashdata('success_msg', 'Cliente actualizado correctamente');
                redirect('cliente');

            }





            $this->form_validation->set_rules('rut', 'Rut', 'trim|is_unique[cliente.rut]');

            if ($this->form_validation->run() == FALSE) {


                $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro cliente.');
                $result=$this->get_cliente($id);
                $data['cliente']=$result;
                $data['main_view'] = 'editar_cliente';
                $this->load->view('layouts/main', $data);


            }

            else{

                $this->Cliente_model->actualizar_cliente($id,$data);

                $this->session->set_flashdata('success_msg', 'Cliente actualizado correctamente');
                redirect('cliente');

            }



        }
        else{
            $result=$this->get_cliente($id);

            if ($result){
                $data['main_view'] = 'editar_cliente';
                $data['cliente']=$result;
                $this->load->view('layouts/main', $data);

            }
            else{
                redirect('cliente');
            }



        }

    }


    public function lista_clientes(){

        $result=$this->Cliente_model->get_lista_clientes();
        return $result;
    }

    public function fetch_clientes(){
        $result=$this->Cliente_model->get_lista_clientes();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }


    function check_cliente_rut($rut,$id) {

        $result = $this->Cliente_model->check_unique_rut($id, $rut);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_cliente_rut', 'Rut ya utilizado');
            $response = false;
        }
        return $response;
    }

    function get_cliente($id){
        $result=$this->Cliente_model->get_cliente($id);
        return $result;
    }


}