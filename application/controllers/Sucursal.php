<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursal extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Sucursal_model');

    }



    public function fetch_sucursales(){
        $result=$this->Sucursal_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }



}