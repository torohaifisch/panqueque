<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaccion_compra extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }
        $this->load->model('Transaccion_model');
        $this->load->model('Dte_model');
        $this->load->model('Detalle_Transaccion_model');
        $this->load->model('Inventario_bodega_model');
        $this->load->model('Historial_precio_producto_model');
        $this->load->model('Marca_producto_model');
        $this->load->model('Marca_producto_formato_model');
        $this->load->model('Alerta_sobreprecio_model');

    }

    public function index()
    {
        $data['main_view']='transaccion_compra';
        $data['titulo']= 'Compra de Insumos';

        $data['transacciones']=$this->lista_transaccion();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {


        /* Transaccion */
        $data_t['id_tipo_io'] = $this->input->post('tipo_io');


        $data_t['id_proveedor'] = $this->input->post('proveedor');


        /* DTE */
        $data_dte['id_tipo_dte'] = $this->input->post('tipo_dte');
        $data_dte['id_tipo_documento'] = $this->input->post('tipo_documento');
        $data_dte['numero_dte'] = $this->input->post('numero_dte');
        $data_dte['monto'] = $this->input->post('monto');
        $data_dte['fecha_emision'] = $this->input->post('fecha_emision');
        $data_dte['fecha_vencimiento'] = $this->input->post('fecha_vencimiento');

        $this->db->trans_start();
        /* crear la DTE para obtener id */
        $id_dte=$this->Dte_model->crear_dte($data_dte);
        $data_t['id_dte']=$id_dte;

        /* Crear transaccion */

        $id_transaccion= $this->Transaccion_model->crear_transaccion($data_t);

        $data_detalle['id_transaccion']=$id_transaccion;

        /*crear detalles*/
        $producto=$this->input->post('producto');
        $cantidad=$this->input->post('cantidad');
        $valor_unitario=$this->input->post('valor_unitario');

        $destino=$this->input->post('destino');
        foreach($producto as $key=>$value){


            $id_f=$value;



            $data_detalle['id_marca_producto_formato']=$id_f;
            $data_detalle['cantidad']=$cantidad[$key];
            $data_detalle['valor_unitario_neto']=$valor_unitario[$key];
            $data_detalle['valor_total_neto']=$valor_unitario[$key]*$cantidad[$key];
            $data_detalle['sucursal_destino']=$destino[$key];

            $this->Detalle_Transaccion_model->crear($data_detalle);

            $data_historial['id_marca_producto_formato']=$id_f;
            $data_historial['valor']=$valor_unitario[$key];
            $data_historial['fecha']=$this->input->post('fecha_emision');

            $this->Historial_precio_producto_model->crear($data_historial);
            $media=$this->Historial_precio_producto_model->precio_medio($id_f);
            $this->Marca_producto_formato_model->actualizar_media($id_f,$media);

            $mpf=$this->Marca_producto_formato_model->get($id_f);


            //arreglar validacio ntiene que ser por producto formato no solo producto
            $res=$this->Inventario_bodega_model->existe_producto($mpf->id_producto,$mpf->id_unidad_medida,$destino[$key]);
            if ($res->num_rows()>0){
                $stock=$cantidad[$key]*$mpf->cantidad_contenida+$res->row()->cantidad;
                $this->Inventario_bodega_model->actualizar_cantidad($mpf->id_producto,$mpf->id_unidad_medida,$destino[$key],$stock);
            }
            else{
                $data_inv['id_sucursal']=$destino[$key];
                $data_inv['id_producto']=$mpf->id_producto;
                $data_inv['id_unidad_medida']=$mpf->id_unidad_medida;
                $data_inv['cantidad']=$cantidad[$key]*$mpf->cantidad_contenida;
                $this->Inventario_bodega_model->crear($data_inv);
            }

            if ($media*1.2<=$valor_unitario[$key]){
                $data_alerta['id_marca_producto_formato']=$id_f;
                $data_alerta['id_transaccion']=$id_transaccion;
                $data_alerta['monto_pagado']=$valor_unitario[$key];
                $this->Alerta_sobreprecio_model->crear($data_alerta);
            }



        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al crear Transaccion ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Transaccion creada correctamente');

        }
        redirect('transaccion_compra');



    }

    public function borrar($id){
        $this->Transaccion_model->borrar_transaccion($id);
        redirect('transaccion');
    }



    public function lista_transaccion(){

        $result=$this->Transaccion_model->get_lista_transaccion();
        return $result;
    }

    public function fetch_ultimo_precio(){
        $id=$this->input->post('id_mpf');
        $result=$this->Historial_precio_producto_model->get_ultimo($id);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


}