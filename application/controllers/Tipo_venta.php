<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_venta extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }
        $this->load->model('Tipo_venta_model');
    }



    public function ajax_crear()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('tipo', 'Tipo', 'trim|is_unique[tipo_venta.tipo]');
            $data['tipo'] = $this->input->post('tipo');


            if ($this->form_validation->run() == FALSE) {


                echo '0';

            } elseif ($result = $this->Tipo_venta_model->crear($data)) {

                echo '1';


            } else {

                echo '0';


            }
        }
    }



    public function fetch_tipos(){
        $result=$this->Tipo_venta_model->get_lista_tipos();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }
}