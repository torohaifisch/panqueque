<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }

        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3){
            redirect('home');
        }

        $this->load->model('Producto_model');
        $this->load->model('Tipo_producto_model');
        $this->load->model('Unidad_medida_model');

    }

    public function index()
    {
        $data['main_view']='producto';
        $data['titulo']= 'Productos';

        $data['productos']=$this->lista_productos();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|is_unique[producto.nombre]');

        $data['nombre'] = $this->input->post('nombre');
        $data['id_tipo_producto'] = $this->input->post('tipo');



        if ($this->form_validation->run() == FALSE) {

            if ($this->input->is_ajax_request()) {
                echo '0';
            } else {

                $this->session->set_flashdata('error_msg', 'Error: El producto ya existe');
                redirect('producto');
                /*set flash y redirect*/
            }
        }


        elseif($result= $this->Producto_model->crear_producto($data)){
            if ($this->input->is_ajax_request()) {
                echo '1';
            }
            else {
                $this->session->set_flashdata('success_msg', 'Producto añadido correctamente');
                redirect('producto');
            }
        }
        else{
            if ($this->input->is_ajax_request()) {
                echo '0';
            }

            else {
                $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');

                redirect('producto');
                /*set flash y redirect*/


            }

        }



    }




    public function borrar($id){
        $this->Producto_model->borrar_producto($id);
        redirect('producto');
    }


    public function lista_productos(){

        $result=$this->Producto_model->get_lista_productos();
        return $result;
    }

    public function fetch_productos(){
        $result=$this->Producto_model->get_lista_productos();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function fetch_tipos(){
        $result=$this->Tipo_producto_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


    public function fetch_unidades(){
        $result=$this->Unidad_medida_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }



}