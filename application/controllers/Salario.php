<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salario extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }
        $this->load->model('Historial_pago_salario_model');
        $this->load->model('Tipo_salario_model');
        $this->load->model('Sucursal_model');

    }

    public function index()
    {
        $data['main_view']='salario';
        $data['titulo']= 'Salarios';
        $data['sucursales']=$this->Sucursal_model->get_lista();


        $data['salarios']=$this->get_lista();
        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {





            $data['id_trabajador'] = $this->input->post('trabajador');
            $data['id_tipo_salario'] = $this->input->post('tipo_salario');
            $data['monto'] = $this->input->post('monto');
            $data['fecha'] = $this->input->post('fecha');


            if($result = $this->Historial_pago_salario_model->ingresar_salario($data)){

                if ($this->input->is_ajax_request()) {
                    echo '1';
                }
                else {
                    $this->session->set_flashdata('success_msg', 'Pago ingresado correctamente');
                    redirect('salario');
                }

            }

            else
            {
                if ($this->input->is_ajax_request()) {
                    echo '0';
                }
                else {
                    $this->session->set_flashdata('error_msg', 'Error: Problema en la base de datos');
                    redirect('salario');
                }
            }

    }




    public function borrar($id){
        $this->Historial_pago_salario_model->borrar($id);
        redirect('salario');
    }


    public function editar($id)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data['id_trabajador'] = $this->input->post('trabajador');
            $data['id_tipo_salario'] = $this->input->post('tipo_salario');
            $data['monto'] = $this->input->post('monto');



            $this->Historial_pago_salario_model->actualizar($id,$data);

            $this->session->set_flashdata('success_msg', 'Pago actualizado correctamente');
            redirect('salario');

            }




        else{
            $result=$this->get_salario($id);

            if ($result){
                $data['main_view'] = 'editar_salario';
                $data['salario']=$result;
                $this->load->view('layouts/main', $data);

            }
            else{
                redirect('salario');
            }



        }

    }


    public function get_lista(){

        $result=$this->Historial_pago_salario_model->get_lista();
        return $result;
    }


    function get_salario($id){
        $result=$this->Historial_pago_salario_model->get_salario($id);
        return $result;
    }

    function fetch_tipos(){
        $result=$this->Tipo_salario_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}