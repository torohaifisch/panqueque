<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marca_producto extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }

        $unprotected_methods = array('busca_prod','fetch_marca_prod_form');

        if($this->session->userdata('tipo')== 4) {

            //grab the controller/method name and compare with protected methods array
            if (!in_array($this->router->method, $unprotected_methods)) {
                redirect('Bodega');
            }
        }



        elseif($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2  and $this->session->userdata('tipo') != 3){
            redirect('home');
        }
        $this->load->model('Marca_producto_model');
        $this->load->model('Marca_model');
        $this->load->model('Marca_producto_formato_model');
        $this->load->model('Producto_model');
        $this->load->model('Producto_formato_model');

    }

    public function index()
    {
        $data['main_view'] = 'marca_producto';
        $data['items'] = $this->lista();
        $data['marcas']=$this->lista_marcas();
        $data['productos']=$this->lista_productos();

        $data['titulo']= 'Marcas y Productos';

        $this->load->view('layouts/main', $data);
    }

    public function crear()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('sku', 'Sku', 'trim|is_unique[marca_producto_formato.sku]');


        $data_mp['id_marca']=$this->input->post('marca');
        $data_mp['id_producto']=$this->input->post('producto');

        $data_pf['id_producto']=$this->input->post('producto');
        $data_pf['cantidad_contenida']=$this->input->post('cantidad');
        $data_pf['id_unidad_medida']=$this->input->post('unidad');

        $data_fin['id_marca']=$this->input->post('marca');
        $data_fin['sku']=$this->input->post('sku');

        if ($this->form_validation->run() == FALSE) {

            if ($this->input->is_ajax_request()) {
                echo '0';
            }

            else {
                $this->session->set_flashdata('error_msg', 'Error: rut ya utilizado por otro cliente.');
                redirect('proveedor');
                /*set flash y redirect*/
            }


        }

        else{

            $this->db->trans_start();

            if ($this->combinacion_existe($data_mp['id_marca'],$data_mp['id_producto'])<=0){

                $this->Marca_producto_model->crear($data_mp);

            }

            $id=$this->combinacion_existe_pf($data_pf['id_producto'],$data_pf['id_unidad_medida'],$data_pf['cantidad_contenida']);
            if ($id==null){

                $id = $this->Producto_formato_model->crear($data_pf);

            }



            $data_fin['id_producto_formato']=$id;
            $k=$this->Marca_producto_formato_model->combinacion_existe($data_fin['id_marca'],$data_fin['id_producto_formato']);
            if ($k!=null){
                if ($this->input->is_ajax_request()) {
                    echo '0';
                }
            }
            else{
                $id_f=$this->Marca_producto_formato_model->crear($data_fin);
                if ($this->input->is_ajax_request()) {
                    echo '1';
                }

            }

            $this->db->trans_complete();

        }



    }


    public function editar_marca_producto($id)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $sku= $this->input->post('sku');
            $viejo_sku=$this->input->post('viejo_sku');

            if ($viejo_sku==$sku){

                $this->Marca_producto_formato_model->actualizar($id,$sku);

                $this->session->set_flashdata('success_msg', 'Actualizado correctamente');
                redirect('marca_producto');

            }





            $this->form_validation->set_rules('sku', 'Sku', 'trim|is_unique[marca_producto_formato.sku]');

            if ($this->form_validation->run() == FALSE) {


                $this->session->set_flashdata('error_msg', 'Error: Sku ya utilizado por otro producto.');
                $result=$this->get_marca_producto($id);
                $data['marca_producto']=$result;
                $data['main_view'] = 'editar_marca_producto';
                $data['titulo']= 'Editar Producto';

                $this->load->view('layouts/main', $data);


            }

            else{

                $this->Marca_producto_formato_model->actualizar($id,$sku);

                $this->session->set_flashdata('success_msg', 'Actualizado correctamente');
                redirect('marca_producto');

            }



        }
        else{
            $result=$this->get_marca_producto($id);

            if ($result){
                $data['main_view'] = 'editar_marca_producto';
                $data['marca_producto']=$result;
                $data['titulo']= 'Editar Producto';

                $this->load->view('layouts/main', $data);

            }
            else{
                redirect('marca_producto');
            }



        }

    }






    public function borrar($id)
    {
        $this->Marca_producto_model->borrar($id);
        redirect('producto');
    }


    public function lista()
    {

        $result = $this->Marca_producto_formato_model->get_lista();
        return $result;
    }

    public function lista_marcas(){

        $result=$this->Marca_model->get_lista_marcas();
        return $result;
    }

    public function lista_productos(){

        $result=$this->Producto_model->get_lista_productos();
        return $result;
    }

    public function get_marca_producto($id){
        return $this->Marca_producto_formato_model->get_producto($id);
    }

    public function fetch_producto(){

        $id= $this->input->post('id');


        $result=$this->Marca_producto_model->get_lista_productos($id);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }

    public function busca_prod(){
        $sku= $this->input->post('sku');


        $result=$this->Marca_producto_formato_model->get_producto_por_codigo($sku);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function fetch_marca_prod_form(){

        $result=$this->Marca_producto_formato_model->get_lista();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


    public function combinacion_existe($id_m,$id_p){
       return $this->Marca_producto_model->combinacion_existe($id_m,$id_p);
    }
    public function combinacion_existe_pf($p,$u,$c){
        return $this->Producto_formato_model->combinacion_existe($p,$u,$c);
    }
}