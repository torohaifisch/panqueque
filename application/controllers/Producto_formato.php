<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto_formato extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3){
            redirect('home');
        }
        $this->load->model('Producto_formato_model');
    }


    public function crear()
    {

        $data['id_producto']=$this->input->post('producto');
        $data['cantidad_contenida']=$this->input->post('cantidad');
        $data['id_unidad_medida']=$this->input->post('unidad');

        if ($this->combinacion_existe($data['id_producto'],$data['id_unidad_medida'],$data['cantidad_contenida'])>0){
            if ($this->input->is_ajax_request()) {
                echo '0';
            }

        }
        else {
            if ($result = $this->Producto_formato_model->crear($data)) {
                if ($this->input->is_ajax_request()) {
                    echo '1';
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    echo '0';
                }

            }
        }


    }


    public function borrar($id)
    {
        $this->Producto_formato_model->borrar($id);
        redirect('producto');
    }


    public function lista()
    {

        $result = $this->Producto_formato_model->get_lista();
        return $result;
    }



    public function combinacion_existe($p,$u,$c){
        return $this->Producto_formato_model->combinacion_existe($p,$u,$c);
    }

    public function fetch_producto_formato(){
        $id= $this->input->post('id');


        $result=$this->Producto_formato_model->get_lista_productos($id);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }
}