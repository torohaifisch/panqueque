<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar_producto_general extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 and $this->session->userdata('tipo') != 3 ){
            redirect('home');
        }
        $this->load->model('Inventario_bodega_model');
        $this->load->model('Sucursal_model');
        $this->load->model('Producto_model');
        $this->load->model('Producto_formato_model');
        $this->load->model('Unidad_medida_model');

    }

    public function index()
    {
        $data['main_view']='asignar_productos/nueva_principal';
        $data['titulo']= 'Asignar Productos General';
        $data['sucursales']=$this->get_sucursales();
        $this->load->view('layouts/main',$data);
    }



    public function crear()
    {
        date_default_timezone_set("America/Santiago");


        /* solicitud pedido */

        $data['id_sucursal'] = $this->input->post('sucursal');



        $this->db->trans_begin();


        /* crear solicitud para obtener id */

        $producto_inventario=$this->input->post('producto');
        $cantidad=$this->input->post('cantidad');
        $unidad=$this->input->post('unidad');

        foreach($producto_inventario as $key=>$value){


            /* Verificar si existe combinacion */
            $prd=$this->get_producto($value);



            /*crear detalles*/


            $data['id_producto']=$value;
            $data['id_unidad_medida']=$unidad[$key];
            $data['cantidad']=$cantidad[$key];


            $res=$this->Inventario_bodega_model->existe_producto($data['id_producto'],$data['id_unidad_medida'], $data['id_sucursal']);
            if ($res->num_rows()>0){

            }
            else{

                $this->Inventario_bodega_model->crear($data);

            }





        }


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_msg', 'Error al Ingresar Solicitud');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('success_msg', 'Solicitud ingresada correctamente');
        }

        redirect('Agregar_producto_general');



    }




    public function get_productos(){
        return $this->Producto_formato_model->get_lista_productos();
    }

    public function fetch_productos(){
        $result= $this->Producto_formato_model->get_lista_productos();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function get_sucursales(){
        return $result=$this->Sucursal_model->get_lista();

    }

    function get_producto($id){
        return $result=$this->Inventario_bodega_model->get_producto($id);
    }

    function get_unidades(){
        return $result=$this->Unidad_medida_model->get_lista();
    }


}