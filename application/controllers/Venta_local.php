<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta_local extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }

        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }
        $this->load->model('Venta_local_model');

    }

    public function index()
    {
        $data['main_view']= 'venta_local';
        $data['titulo']= 'Nueva Venta';

        $this->load->view('layouts/main',$data);
    }

    public function crear()
    {


        /* Transaccion */


        $data['id_tipo_venta'] = $this->input->post('tipo_venta');
        $data['monto'] = $this->input->post('monto');
        $data['id_sucursal']= $this->input->post('sucursal');
        $data['fecha'] = $this->input->post('fecha');
        $data['comentario'] = $this->input->post('comentario');

        $this->db->trans_start();


        $this->Venta_local_model->crear($data);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->session->set_flashdata('error_msg', 'Error al crear Transaccion ');

        }
        else{
            $this->session->set_flashdata('success_msg', 'Transaccion creada correctamente');

        }
        redirect('Venta_local');



    }


    public function fetch_acum_mes(){
        $result=$this->Venta_local_model->fetch_acum_mes();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));


    }




}