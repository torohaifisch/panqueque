<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        if($this->session->userdata('tipo') != 1 and $this->session->userdata('tipo') != 2 ){
            redirect('home');
        }
        $this->load->model('Transaccion_model');
        $this->load->model('Venta_local_model');
        $this->load->model('Venta_empresa_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Solicitud_compra_model');
        $this->load->model('Solicitud_pedido_model');
        $this->load->model('Inventario_bodega_model');
        $this->load->model('Sucursal_model');


    }

    public function index()
    {
        $data['main_view']='dashboard';
        $data['titulo']= 'Dashboard';

        $data['transacciones']=$this->fetch_ultimas_transacciones(10);
        $data['egresos']=$this->fetch_egreso_dia();
        $data['ingreso_dia']=$this->fetch_ingreso_dia();
        $data['solicitud_compra_pendiente']=$this->fetch_solicitudes_compra_pendientes();
        $data['critico']=$this->get_inventario_critico();
        $data['critico_mini']=$this->get_critico_mini();
        $this->load->view('layouts/main',$data);
    }


    function fetch_ultimas_transacciones($n){
        return $this->Transaccion_model->fetch_ultimas_transacciones($n);

    }

    function fetch_egreso_dia(){
        return $this->Transaccion_model->fetch_egreso_dia();
    }

    function fetch_ingreso_dia(){
        $l=0;
        $e=0;
        $local=$this->Venta_local_model->fetch_ingreso_dia();
        $empresa=$this->Venta_empresa_model->fetch_ingreso_dia();

        if ($local[0]->monto!=null){
            $l=$local[0]->monto;
        }
        if ($empresa[0]->monto!=null){
            $e=$empresa[0]->monto;

        }
        return $l+$e;
    }

    function fetch_ingreso_acum_mes()
    {

        $result = $this->Dashboard_model->fetch_ingreso_acum_mes();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }


    function fetch_egreso_acum_mes()
    {

        $result = $this->Dashboard_model->fetch_egreso_acum_mes();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    function fetch_solicitudes_compra_pendientes(){
        return $this->Solicitud_pedido_model->solicitudes_pendientes();
    }

    function get_inventario_critico(){

        $res=$this->Inventario_bodega_model->get_productos();
        $counter=0;
        foreach ($res as $data){
            if ($data->cantidad <= $data->cantidad_critica){
                $counter=$counter+1;
            }
        }
        return $counter;

    }

    function get_critico_mini(){
        return $this->Sucursal_model->get_criticos_mini();
    }
}