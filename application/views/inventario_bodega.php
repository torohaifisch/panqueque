
<div class="wrapper">
    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item active">Inventario</li>

                        </ol>
                    </div>
                    <h4 class="page-title">Inventario</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->



        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="sucursal">Sucursal</label>
                    <select class="custom-select sucursal" id="sucursal" name="sucursal">
                        <option value="0">Todas</option>
                        <?php if($sucursales):
                            foreach($sucursales as $sucursal):
                                echo '<option value="'.$sucursal->id_sucursal.'">'. $sucursal->nombre.'</option>';

                            endforeach;
                        endif


                        ?>
                    </select>
                </div>
            </div>

        </div>



        <div class="row">
            <div class="col-md-4 col-xl-4">
                <div class="card-box tilebox-one">
                    <i class="fa fa-dollar float-right text-success"></i>
                    <h6 class="text-muted text-uppercase mt-0">Valorizacion Estimada</h6>
                    <?php echo '<h2 class="m-b-20">$<span id="num">'.number_format($valor_total_estimado+$valor_total_estimado_mini,0,",",".").'</span></h2>' ?>
                </div>
            </div>



        </div>


        <div class="row">
            <div class="col-sm-3">
                <button class="btn btn-custom dropdown-toggle btn-block" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Modificar Stock Crítico
                </button>


                <div class="dropdown-menu" aria-labelledby="dropdownMenu2" style="font-size:14px">
                    <?php
                        if ($sucursales):
                            foreach ($sucursales as $sucursal):
                                echo  "<a class=\"dropdown-item\" href=\"".base_url().'bodega/stock_critico/'.$sucursal->id_sucursal."\" >".$sucursal->nombre."</a>";

                            endforeach;

                        endif

                    ?>



                </div>
            </div>

            <div class="col-sm-3">
                <button class="btn btn-custom dropdown-toggle btn-block" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Generar Documento de Compra
                </button>


                <div class="dropdown-menu" aria-labelledby="dropdownMenu2" style="font-size:14px">
                    <?php
                    if ($sucursales):
                        foreach ($sucursales as $sucursal):
                            echo  "<a class=\"dropdown-item\" href=\"".base_url().'bodega/generar_documento/'.$sucursal->id_sucursal."\" >".$sucursal->nombre."</a>";

                        endforeach;

                    endif

                    ?>



                </div>
            </div>


            <div class="col-sm-3">

                <?php

                echo  "<a class=\"btn btn-custom btn-block \" href=\"".base_url().'Agregar_producto_general'."\" >Agregar Producto a Bodega General</a>";



                ?>




            </div>

        </div>

        <br>




        <br>

        <div class="row">
            <div class="col-12">
                <div class="card-box">

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Producto</th>
                            <th>Cantidad Disponible</th>

                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($productos):
                            foreach ($productos as $producto):
                                echo "<tr>";
                                echo "<td>".$producto->sucursal."</td>";
                                echo "<td>".$producto->producto."</td>";
                                echo "<td>".(float)$producto->cantidad.' '.$producto->medida."</td>";
                                if($producto->cantidad>=$producto->cantidad_optima):
                                    echo '<td><span class="badge badge-success">Optimo</span></td>';

                                elseif($producto->cantidad<=$producto->cantidad_critica):
                                    echo '<td><span class="badge badge-warning">Crítico</span></td>';

                                else:
                                    echo '<td><span class="badge badge-success">Normal</span></td>';
                                endif;
                                echo "<td>
                                            <div class=\"btn-group dropdown\">
                                            <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                            <div class=\"dropdown-menu dropdown-menu-right\">
                                                <a class=\"dropdown-item\" href=\"".base_url().'bodega/detalle_producto/'.$producto->id_inventario_bodega."\" >Ver Detalles</a>
                                                <a class=\"dropdown-item\" href=\"\" id=\"modificar_stock\" data-id='".$producto->id_inventario_bodega."' data-stock='".(float)$producto->cantidad."' data-suc='".$producto->sucursal."' data-info='".$producto->producto."' data-medida='".$producto->medida."'>Modificar Cantidad</a>

                                            </div>
                                        </div>
                                        </td>";
                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>




            <div id="modificar_stock_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30 text-custom">
                                Modificar Stock
                            </h2>
                            <h4 class="text-center m-b-30" id="m_info">
                            </h4>

                            <form  id="modificar_stock_form" role="form" method="post" action="<?php echo base_url();?>Bodega/modificar_cantidad">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <label for="sucursal">Sucursal :</label>
                                        <p id="m_suc"></p>


                                    </div>

                                    <div class="col-sm-6">
                                        <label for="sucursal">Cantidad Actual:</label>
                                        <p id="m_stock"></p>


                                    </div>


                                    <input class="form-control" type="number" id="id_inventario" name="id_inventario"  hidden required>



                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="cantidad">Nuevo Cantidad*</label>
                                            <input class="form-control" type="number" id="cantidad" name="cantidad" step="1" min="1" required>

                                        </div>
                                    </div>




                                </div>





                                <button type="submit" class="btn btn-primary">Modificar</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>



<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>





<script type="text/javascript">
    $(document).ready(function() {
        $('#transferir_producto_form i').hide();


        // Default Datatable
        var table=$('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });


        $('#sucursal').on('change', function(){
            var val=$('#sucursal option:selected').text();
            if (val=='Todas'){
                val='';
            }
            table.search(val).draw();
            get_valorizacion(this.value);
        });






    } );



    $('a#modificar_stock').click(function (event) {
        event.preventDefault();
        $('#modificar_stock_modal').modal('toggle');
        $('#m_info').text($(this).attr("data-info"));
        $('#m_suc').text($(this).attr("data-suc"));
        $('#m_stock').text($(this).attr("data-stock")+' '+$(this).attr("data-medida"));
        $('#id_inventario').val($(this).attr("data-id"))

    });






    function get_valorizacion(val) {

        $.ajax({

            url: "<?php echo base_url(); ?>Bodega/fetch_total_completo",
            method: "POST",
            dataType: 'json',
            data: {id:val},
            success: function (data) {


                // Add options

                    $('#num').empty();
                    $('#num').append(data.toLocaleString('es-ES'));



            }
        });
    }






    function swal_error(){
        swal({
            type: 'error',
            title: 'Ups...',
            text: 'Error: no se pudo crear.'
        })
    }

    function swal_success(){
        swal(
            '¡Todo bien!',
            'Creado correctamente!',
            'success'
        );

    }


</script>