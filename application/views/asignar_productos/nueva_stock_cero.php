
<link href="<?php echo base_url();?>/plugins/footable/css/footable.standalone.min.css" rel="stylesheet">

<style>
    .footable-filtering-search .btn.dropdown-toggle {
        display: none;
    }
</style>

<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item active">Copiar a Sub</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Copiar Productos a Sub Bodega (stock 0)</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">


                        <div class="col-sm-6">
                            <h5 class="text-custom">Sucursal</h5>
                            <?php echo $sucursal->nombre?>

                        </div>

                        <div class="col-sm-6">
                            <h5 class="text-custom" >Area</h5>
                            <?php echo $area->area
                            ?>


                        </div>




                    </div>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card-box">

                    <form id="stock_critico_form" action="<?php echo base_url();?>bodega/crear_nueva_cero" method="post" >

                        <select class="form-control" id="sucursal" name="sucursal" required hidden>
                            <?php
                            if ($sucursal):

                                echo '<option value="'.$sucursal->id_sucursal.'">'.$sucursal->nombre.'</option>';

                            endif;


                            ?>

                        </select>

                        <select class="form-control" id="area" name="area" required hidden>
                            <?php
                            if ($area):

                                echo '<option value="'.$area->id_area_trabajo.'">'.$area->area.'</option>';

                            endif;


                            ?>

                        </select>




                        <table class="table toggle-arrow-tiny" data-sorting="true" >
                            <thead>
                            <tr>


                                <th>Id</th>
                                <th>Producto</th>
                                <th>Transferir</th>
                            </tr>
                            </thead>


                            <tbody>
                            <?php if ($productos):
                                foreach ($productos as $producto):
                                        echo "<tr>";
                                        echo "<td>".$producto->id_inventario_bodega."</td>";
                                        echo "<td>".$producto->producto."</td>";
                                        echo "<td>
                                        <input class=\"form-check-input\" type=\"checkbox\" id=\"producto[]\" name=\"producto[]\" value='".$producto->id_inventario_bodega."' ></td>";
                                        echo "</tr>";

                                endforeach;
                            endif;
                            ?>
                            </tbody>

                        </table>

                        <br>

                        <div class="row ">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-custom">Asignar</button>

                            </div>

                        </div>

                    </form>


                </div>
            </div>








        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->







<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<!--FooTable-->
<script src="<?php echo base_url();?>/plugins/footable/js/footable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        jQuery(function($){
            $('.table').footable();
        });

    } );





    function imprimir() {
        window.print();
    }



</script>