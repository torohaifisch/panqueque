
<style>

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }

</style>

<div class="wrapper">


    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>



    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item"><a href="#">Inventario</a></li>
                            <li class="breadcrumb-item active">Asignar Productos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Asignar Productos</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row dynamic-element" style="display:none">



            <div class="col-md-3">
                <div class="form-group">
                    <label for="producto">Producto *</label>
                    <select class="custom-select producto" id="producto[]" name="producto[]" required>


                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="stock">Stock Bodega Principal*</label>
                    <select class="custom-select stock" id="stock[]" name="stock[]" disabled>


                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group ">
                    <label for="cantidad">Cantidad a Asignar*</label>
                    <input class="form-control" type="number" id="cantidad[]" name="cantidad[]" min="1" step="1" required>

                </div>
            </div>



            <div class="col-md-1">
                <div class="form-group ">

                    <label for="cantidad">Eliminar</label>


                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger delete-row"> <i class="fa fa-remove"></i> </button>
                </div>
            </div>


        </div>

        <!-- end dynamic -->



        <div class="row">
            <div class="col-md-12">
                <div class="card-box">



                    <form id="solicitud_form" action="<?php echo base_url();?>Asignar_productos/crear" method="post">


                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sucursal" >Sucursal</label>

                                    <select class="form-control" id="sucursal" name="sucursal" required>
                                        <?php
                                            if ($sucursales):
                                            foreach($sucursales as $sucursal):
                                                echo '<option value="'.$sucursal->id_sucursal.'">'.$sucursal->nombre.'</option>';
                                            endforeach;
                                            endif;


                                        ?>

                                    </select>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="area" >Area</label>

                                    <select class="form-control" id="area" name="area" required>

                                    </select>
                                </div>
                            </div>



                        </div>


                        <button type="button" class="btn btn-info waves-effect waves-light add-dynamic" id="add-dynamic" >Agregar detalle</button>
                        <br></br>
                        <div class="dynamic-stuff">

                            <div class="row dynamic-element" >



                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="producto">Producto *</label>
                                        <select class="custom-select producto" id="producto[0]" name="producto[0]" required>


                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="stock">Stock Bodega Principal *</label>
                                        <select class="custom-select stock" id="stock[0]" name="stock[0]" disabled>


                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label for="cantidad">Cantidad a Asignar*</label>
                                        <input class="form-control" type="number" id="cantidad[0]" name="cantidad[0]" min="0.001" step="0.001" required>

                                    </div>
                                </div>




                            </div>



                        </div>
                        <button type="button" class="btn btn-info waves-effect waves-light add-dynamic" id="add-dynamic2" >Agregar detalle</button>

                        <br></br>
                        <div class="row ">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-primary">Finalizar </i></button>

                            </div>

                        </div>

                    </form>





                </div>
                <!-- end col -->


            </div>




        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>


<script>

    $( document ).ready(function() {


        cargar_marcas();
        cargar_areas();
        attach_change();



        var counter=1;


        //Clone the hidden element and shows it
        $('.add-dynamic').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="producto["+counter+"]"| this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();


            attach_delete();
            attach_change();

            counter++;
            //$(".select2").select2();


        });


        //Attach functionality to delete buttons
        function attach_delete() {
            $('.delete-row').off();
            $('.delete-row').click(function () {

                $(this).closest('.dynamic-element').remove();
            });
        }

        $('#sucursal').on('change',function(){

            $('.dynamic-stuff').empty();
            cargar_marcas();

        })


    });


    function attach_change(){
        $('.producto').off();
        $('.producto').on('change',function(){
            producto=$(this);
            var selected_val =this.value;
            var p_val=null;

            producto.closest('.dynamic-element').find('.stock').empty();

            producto.closest('.dynamic-element').find('.stock').append('<option value="">' + $(this).find(':selected').attr('data-stock')+' '+ $(this).find(':selected').attr('data-medida')+'</option>');
            producto.closest('.dynamic-element').find('.cantidad').attr({"max" : $(this).find(':selected').attr("data-stock")});




        })}



    $( "#solicitud_form" ).submit(function( event ) {
        if($('.dynamic-stuff > div ').length===0){
            event.preventDefault();
        }

    });
</script>

<script>







    function cargar_marcas(){
        $("#add-dynamic2").attr("disabled", true);
        $("#add-dynamic").attr("disabled", true);


        $.ajax({

            url: "<?php echo base_url(); ?>bodega/fetch_productos_sucursal",
            method: "POST",
            dataType: 'json',
            data:{sucursal:$('#sucursal').val()},
            success: function (data) {

                $('.producto').each(function(){
                    var selected_val=$(this).val();
                    var producto=$(this);

                    producto.empty();

                    // Add options
                    $.each(data, function (index, data) {
                        producto.append('<option data-stock="'+parseFloat(data['cantidad'])+'" data-medida="'+data['medida']+'" value="' + data['id_inventario_bodega'] + '">' + data['producto']+' '+data['medida']+'</option>');

                        if (index==0){
                            selected_val=data['id_inventario_bodega'];
                            producto.closest('.dynamic-element').find('.stock').empty();
                            producto.closest('.dynamic-element').find('.stock').append('<option value="">' + parseFloat(data['cantidad'])+' '+data['medida']+'</option>');


                        }


                    });

                    if (selected_val!=null){
                        producto.val(selected_val);
                        producto.closest('.dynamic-element').find('.cantidad').attr({"max" : $(this).find(':selected').attr("data-stock")});

                    }
                });


                $("#add-dynamic2").attr("disabled", false);
                $("#add-dynamic").attr("disabled", false);


            }
        });





    }

    function cargar_areas(){


        var tipo=$('#area');
        $.ajax({

            url: "<?php echo base_url(); ?>area_trabajo/fetch_areas",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                tipo.empty();

                // Add options
                $.each(data, function (index, data) {
                    tipo.append('<option value="' + data['id_area_trabajo'] + '">' + data['area'] + '</option>');
                });



            }
        });


    }



</script>