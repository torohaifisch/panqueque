
<style>

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }

</style>

<div class="wrapper">


    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>



    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item"><a href="#">Inventario</a></li>
                            <li class="breadcrumb-item active">Agregar Productos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Agregar Productos a Bodega General</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row dynamic-element" style="display:none">



            <div class="col-md-3">
                <div class="form-group">
                    <label for="producto">Producto *</label>
                    <select class="custom-select producto" id="producto[]" name="producto[]" required>


                    </select>
                </div>
            </div>
                <div class="form-group">
                    <select class="custom-select unidad" id="unidad[]" name="unidad[]"hidden>


                    </select>
                </div>

            <div class="col-md-3">
                <div class="form-group ">
                    <label for="cantidad">Cantidad*</label>
                    <input class="form-control" type="number" id="cantidad[]" name="cantidad[]" min="0.001" step="0.001" required>

                </div>
            </div>



            <div class="col-md-1">
                <div class="form-group ">

                    <label for="cantidad">Eliminar</label>


                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger delete-row"> <i class="fa fa-remove"></i> </button>
                </div>
            </div>


        </div>

        <!-- end dynamic -->



        <div class="row">

            <div class="col-12">

                    <div class="alert alert-warning" role="alert">
                        <h4>Importante</h4>
                        <ul>
                            <li>Esta Funcionalidad solo permite agregar productos que no existen en el inventario seleccionado, <strong>no es para agregar stock</strong>.</li>
                            <li>Al finalizar se agregara el producto si no existe, Si ya existe no sucedera nada y no se agregara mas stock</li>
                        </ul>
                    </div>



            </div>



            <div class="col-md-12">
                <div class="card-box">



                    <form id="solicitud_form" action="<?php echo base_url();?>Agregar_producto_general/crear" method="post">


                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sucursal" >Sucursal</label>

                                    <select class="form-control" id="sucursal" name="sucursal" required>
                                        <?php
                                            if ($sucursales):
                                            foreach($sucursales as $sucursal):
                                                echo '<option value="'.$sucursal->id_sucursal.'">'.$sucursal->nombre.'</option>';
                                            endforeach;
                                            endif;


                                        ?>

                                    </select>
                                </div>

                            </div>



                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-block btn-info waves-effect waves-light add-dynamic" id="add-dynamic" >Agregar</button>

                            </div>
                        </div>
                        <hr>
                        <div class="dynamic-stuff">

                            <div class="row dynamic-element" >



                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="producto">Producto *</label>
                                        <select class="custom-select producto" id="producto[0]" name="producto[0]" required>


                                        </select>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <select class="custom-select unidad" id="unidad[0]" name="unidad[0]" hidden>


                                        </select>
                                    </div>

                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label for="cantidad">Cantidad*</label>
                                        <input class="form-control" type="number" id="cantidad[0]" name="cantidad[0]" min="0.001" step="0.001" required>

                                    </div>
                                </div>




                            </div>



                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-block btn-info waves-effect waves-light add-dynamic" id="add-dynamic" >Agregar</button>

                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-primary">Finalizar </i></button>

                            </div>

                        </div>

                    </form>





                </div>
                <!-- end col -->


            </div>




        </div>
        <!-- end row -->





    </div> <!-- end container -->
</div>
<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>


<script>

    $( document ).ready(function() {

        $('#producto_form i').hide();


        cargar_marcas();
        attach_change();



        var counter=1;


        //Clone the hidden element and shows it
        $('.add-dynamic').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="producto["+counter+"]"| this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();


            attach_delete();
            attach_change();

            counter++;
            //$(".select2").select2();


        });


        //Attach functionality to delete buttons
        function attach_delete() {
            $('.delete-row').off();
            $('.delete-row').click(function () {

                $(this).closest('.dynamic-element').remove();
            });
        }



    });


    function attach_change(){
        $('.producto').off();
        $('.producto').on('change',function(){
            producto=$(this);
            var selected_val =this.value;
            var p_val=null;

            producto.closest('.dynamic-element').find('.unidad').empty();

            producto.closest('.dynamic-element').find('.unidad').append('<option value="'+$(this).find(':selected').attr('data-medida')+'">' + $(this).find(':selected').attr('data-medida')+'</option>');




        })}



    $( "#solicitud_form" ).submit(function( event ) {
        if($('.dynamic-stuff > div ').length===0){
            event.preventDefault();
        }

    });




</script>

<script>







    function cargar_marcas(){
        $("#add-dynamic2").attr("disabled", true);
        $("#add-dynamic").attr("disabled", true);


        $.ajax({

            url: "<?php echo base_url(); ?>agregar_producto_general/fetch_productos",
            method: "POST",
            dataType: 'json',
            success: function (data) {

                $('.producto').each(function(){
                    var selected_val=$(this).val();
                    var producto=$(this);

                    producto.empty();

                    // Add options
                    $.each(data, function (index, data) {
                        producto.append('<option value="' + data['id_producto'] + '" data-medida="'+ data['id_unidad_medida']+'">' + data['nombre']+' '+data['medida']+'</option>');

                        if (index==0){
                            selected_val=data['id_producto_formato'];
                            producto.closest('.dynamic-element').find('.unidad').append('<option value="'+data['id_unidad_medida']+'">' + data['id_unidad_medida']+'</option>');


                        }


                    });

                    if (selected_val!=null){
                        producto.val(selected_val);

                    }
                });


                $("#add-dynamic2").attr("disabled", false);
                $("#add-dynamic").attr("disabled", false);


            }
        });





    }


    function swal_error(){
        swal({
            type: 'error',
            title: 'Ups...',
            text: 'Error: no se pudo crear.'
        })
    }

    function swal_success(){
        swal(
            '¡Todo bien!',
            'Creado correctamente!',
            'success'
        );

    }

</script>