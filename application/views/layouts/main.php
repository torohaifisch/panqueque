<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Panqueque - <?php if ($titulo): echo $titulo; endif ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Panqueque sistema de bodegas" name="description" />
    <meta content="Wenic" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">

    <link href="<?php echo base_url();?>plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" />

    <link href="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plugins/jquery.steps/css/jquery.steps.css" />

    <!-- Spinkit css -->
    <link href="<?php echo base_url();?>plugins/spinkit/spinkit.css" rel="stylesheet" />

    <!-- select2 -->
    <link href="<?php echo base_url();?>plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>


    <!-- Sweet Alert css -->
    <link href="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />


    <!-- Plugins css -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet">




</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <!-- Text Logo -->
                <!-- <a href="index.html" class="logo">
                    <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                    <span class="logo-large"><i class="mdi mdi-radar"></i> Highdmin</span>
                </a> -->
                <!-- Image Logo -->
                <a href="<?php echo base_url();?>home" class="logo">
                    <img src="<?php echo base_url();?>assets/images/logo_negro.png" alt="" height="35" class="logo-small">
                    <img src="<?php echo base_url();?>assets/images/logo_negro.png" alt="" height="40" class="logo-large">
                </a>

            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>






                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="<?php echo base_url();?>assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name"><?php echo $this->session->userdata('nombre');?> <i class="mdi mdi-chevron-down"></i> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h6 class="text-overflow m-0">Opciones</h6>
                            </div>



                            <!-- item-->
                            <a href="<?php echo base_url();?>login/logout" class="dropdown-item notify-item">
                                <i class="fi-power"></i> <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="<?php echo base_url();?>Dashboard"><i class="icon-speedometer"></i>Dashboard</a>
                    </li>
                    <?php
                    if($this->session->userdata('tipo') == 1 ):
                        echo"
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"fa fa-user-circle-o\"></i>Usuarios</a>
                        <ul class=\"submenu\">
                            <li><a href=\"".base_url()."Usuario/nuevo_usuario\">Crear</a></li>
                            <li><a href=\"".base_url()."Usuario\">Lista</a></li>


                        </ul>
                    </li>";
                    endif;
                    ?>

                    <?php

                    if($this->session->userdata('tipo') == 1 or $this->session->userdata('tipo') == 2 ):

                        echo "
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"icon-layers\"></i>Administrar</a>
                        <ul class=\"submenu\">
                            <li><a href=\"".base_url()."Area_trabajo\">Areas de Trabajo</a></li>
                            <li><a href=\"".base_url()."Cliente\">Clientes</a></li>
                            <li><a href=\"".base_url()."Proveedor\">Proveedores</a></li>
                            <li><a href=\"".base_url()."Marca_producto\">Marcas y Productos</a></li>


                        </ul>
                    </li>
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\" mdi mdi-cash-multiple\"></i>Transacciones</a>
                        <ul class=\"submenu\">
                            <li class=\"has-submenu\">
                                <a href=\"#\">Ingresos</a>
                                <ul class=\"submenu\">
                                    <li><a href=\"".base_url()."Venta_local\">Venta Local</a></li>
                                    <li><a href=\"".base_url()."Venta_empresa\">Venta Empresas</a></li>
                                    <li><a href=\"#\">Otros</a></li>
                                </ul>
                            </li>
                            <li class=\"has-submenu\">
                                <a href=\"#\">Egresos</a>
                                <ul class=\"submenu\">
                                    <li><a href=\"".base_url()."Transaccion_compra\">Registrar Compra</a></li>
                                    <li><a href=\"".base_url()."Salario\">Pago de Salario</a></li>
                                    <li><a href=\"#\">Otros</a></li>


                                </ul>
                            </li>
                            
                            

                        </ul>
                    </li>
                                 
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\" mdi mdi-cash-multiple\"></i>Pagos y Cobros</a>
                        <ul class=\"submenu\">
                            <li class=\"has-submenu\">
                                <a href=\"#\">Pagos</a>
                                <ul class=\"submenu\">
                                    <li><a href=\"".base_url()."Notificacion_pago\">Ver Pendientes de pago</a></li>
                                    <li><a href=\"".base_url()."Notificacion_pago/ver_pagados\">Ver Pagados</a></li>
                                </ul>
                            </li>
                            <li class=\"has-submenu\">
                                <a href=\"#\">Cobros</a>
                                <ul class=\"submenu\">
                                    <li><a href=\"#\">Ver pendientes de cobro</a></li>
                                    <li><a href=\"#\">Ver Cobrados</a></li>


                                </ul>
                            </li>
                            
                            

                        </ul>
                    </li>
                    
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"icon-social-dropbox\"></i>Bodega</a>
                        <ul class=\"submenu\">
                            <li><a href=\"".base_url()."Bodega\">Inventario</a></li>

                            <li class=\"has-submenu\">
                                <a href=\"#\">Transferir Productos</a>
                                <ul class=\"submenu\">
                                    <li><a href=\"".base_url()."traspaso_inventario/lista\">Ver Transferencias</a></li>
                                    <li><a href=\"".base_url()."traspaso_inventario\">Nueva transferencia</a></li>
                                </ul>
                            </li>


                        </ul>
                    </li>
                    
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"  mdi mdi-account-multiple-outline\"></i>Personal</a>
                        <ul class=\"submenu\">
                             <li><a href=\"".base_url()."Trabajador\">Trabajadores</a></li>

                        </ul>
                    </li>
                    
                    
                    
                    ";
                    elseif($this->session->userdata('tipo') == 3):
                        echo "
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"icon-layers\"></i>Administrar</a>
                        <ul class=\"submenu\">
                            <li><a href=\"".base_url()."Cliente\">Clientes</a></li>
                            <li><a href=\"".base_url()."Proveedor\">Proveedores</a></li>
                            <li><a href=\"".base_url()."Marca_producto\">Marcas y Productos</a></li>


                        </ul>
                    </li>
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\" mdi mdi-cash-multiple\"></i>Transacciones</a>
                        <ul class=\"submenu\">
                            <li class=\"has-submenu\">
                                <a href=\"#\">Egresos</a>
                                <ul class=\"submenu\">
                                    <li><a href=\"".base_url()."Transaccion_compra\">Registrar Compra</a></li>
                                

                                </ul>
                            </li>

                        </ul>
                    </li>
                    
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"icon-social-dropbox\"></i>Bodega</a>
                        <ul class=\"submenu\">
                            <li><a href=\"".base_url()."Bodega\">Inventario</a></li>

                        </ul>
                    </li>
                    
                    
                    
                    
                    ";
                    else:
                        echo " 
                    <li class=\"has-submenu\">
                        <a href=\"#\"><i class=\"  mdi mdi-account-multiple-outline\"></i>Personal</a>
                        <ul class=\"submenu\">
                           
                            <li><a href=\"".base_url()."Turno\">Turno</a></li>
                        </ul>
                    </li>";

                    endif;


                    ?>

                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->




<!-- jQuery  -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.rut.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js"></script>



<?php $this->load->view($main_view)?>

<!-- App js -->
<script src="<?php echo base_url();?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.app.js"></script>

<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                2019 © Wenic
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->





</body>
</html>