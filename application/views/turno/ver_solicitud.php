
<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Personal</a></li>
                            <li class="breadcrumb-item"><a href="#">Turno</a></li>
                            <li class="breadcrumb-item active">Ver detalle Pedido</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Detalle Pedido N°<?php echo $solicitud->id_solicitud_pedido?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">

                        <div class="col-sm-3">
                            <h5 class="text-custom">Nombre de Usuario</h5>
                            <?php echo $solicitud->nombre.' '.$solicitud->ape_pat?>

                        </div>


                        <div class="col-sm-3">
                            <h5 class="text-custom">Sucursal</h5>
                            <?php echo $solicitud->sucursal?>

                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom" >Area</h5>
                            <?php echo $solicitud->area
                            ?>


                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom" >Fecha</h5>
                            <?php echo $solicitud->fecha
                            ?>


                        </div>


                    </div>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card-box">



                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>

                            <th>Producto</th>
                            <th>Stock</th>
                            <th>Cantidad Solicitada</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($detalles):
                            foreach ($detalles as $detalle):
                                echo "<tr>";
                                echo "<td>".$detalle->producto.' '.$detalle->medida."</td>";
                                echo "<td>".(float)$detalle->cantidad."</td>";
                                echo "<td>".(float)$detalle->cantidad_solicitada."</td>";

                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>












        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>


<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>





<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        $('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });

    } );





</script>