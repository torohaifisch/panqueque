
<style>

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }

</style>

<div class="wrapper">


    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>



    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Personal</a></li>
                            <li class="breadcrumb-item"><a href="#">Turno</a></li>
                            <li class="breadcrumb-item active">Retirar Productos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Retirar Productos</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row dynamic-element" style="display:none">


            <div class="col-md-4">
                <div class="form-group">
                    <label for="producto">Producto *</label>
                    <select class="custom-select producto" id="producto[]" name="producto[]" required>


                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group ">
                    <label for="cantidad">Cantidad*</label>
                    <input class="form-control cantidad" type="number" id="cantidad[]" name="cantidad[]" min="1" step="1" required>

                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group ">

                    <label>Eliminar</label>

                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger delete-row"> <i class="fa fa-remove"></i> </button>
                </div>
            </div>


        </div>

        <!-- end dynamic -->



        <div class="row">
            <div class="col-md-12">
                <div class="card-box">



                    <form id="solicitud_form" action="<?php echo base_url();?>Turno/crear_retiro_productos" method="post">


                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sucursal" >Sucursal</label>

                                    <select class="form-control" id="sucursal" name="sucursal" required>
                                        <?php
                                            if ($sucursal):
                                                echo '<option value="'.$sucursal->id_sucursal.'">'.$sucursal->nombre.'</option>';

                                            endif;


                                        ?>

                                    </select>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="area" >Area</label>

                                    <select class="form-control" id="area" name="area" required>
                                        <?php
                                        if ($area):
                                            echo '<option value="'.$area->id_area_trabajo.'">'.$area->area.'</option>';

                                        endif;


                                        ?>

                                    </select>
                                </div>
                            </div>



                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-info waves-effect waves-light btn-block" data-toggle="modal" data-target="#producto_por_codigo_modal" >Agregar producto por codigo</button>

                            </div>


                        </div>


                        <hr>
                        <div class="dynamic-stuff">





                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-info waves-effect waves-light btn-block" data-toggle="modal" data-target="#producto_por_codigo_modal" >Agregar producto por codigo</button>

                            </div>


                        </div>

                        <br></br>
                        <div class="row ">
                            <div class="col-sm-2 offset-sm-10">
                                <button type="submit" class="btn btn-primary">Finalizar </i></button>

                            </div>

                        </div>

                    </form>





                </div>
                <!-- end col -->


            </div>

            <div id="producto_por_codigo_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Agregar Producto
                            </h2>

                            <form  id="producto_por_codigo_form" role="form" method="post" action="">
                                <div class="row">



                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="codigo">Codigo*</label>
                                            <input type="text" class="form-control" id="codigo" name="codigo" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="prd" >Producto*</label>

                                            <input type="text" class="form-control" data-id="" data-name="" id="prd" name="prd" readonly required>
                                        </div>


                                    </div>




                                </div>


                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <div id="producto_por_nombre_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Agregar Producto
                            </h2>

                            <form id="producto_por_nombre_form" role="form" method="post" action="">
                                <div class="row">


                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="pro">Producto*</label>
                                            <select class="select2" id="prd2" name="prd2" required>


                                            </select>
                                        </div>
                                    </div>




                                </div>


                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->





        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<script src="<?php echo base_url();?>plugins/select2/js/select2.min.js" type="text/javascript"></script>


<script>
    var counter=1;
    $( document ).ready(function() {

        $('#prd2').select2({
            dropdownParent: $('#producto_por_nombre_modal')
        });

        $('#proveedor').select2();

        $("#codigo").keyup(function(){
            var search = $(this).val();
            $("#prd").val('');
            $("#prd").attr('data-id','');
            $("#prd").attr('data-name','');
            if(search != ""){

                $.ajax({

                    url: "<?php echo base_url(); ?>marca_producto/busca_prod",
                    method: "POST",
                    dataType: 'json',
                    data: {sku:$(this).val()},
                    success: function (data) {


                        // Add options


                        $("#prd").val(data['marca'] +' '+ data['producto'] +' '+ data['cantidad_contenida']+' '+ data['unidad']);
                        $("#prd").attr('data-id',data['id']);
                        $("#prd").attr('data-name',data['marca'] +' '+ data['producto'] +' '+ data['cantidad_contenida']+' '+ data['unidad']);




                    }
                });

            }

        });


        c_marca_prod_form($('#prd2'));






        //Clone the hidden element and shows it
        $('.add-dynamic').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();
            clon.find('.producto').append('<option value="' + $('#prd').attr("data-id") + '">' + $('#prd').attr("data-name") + '</option>');


            attach_delete();
            counter++;
            $(".select2").select2();


        });

        $('.add-dynamic2').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();
            clon.find('.producto').append('<option value="' + $('#prd').attr("data-id") + '">' + $('#prd').attr("data-name") + '</option>');


            attach_delete();
            counter++;
            $(".select2").select2();


        });


        //Attach functionality to delete buttons


        $('#origen').on('change',function(){

            $('.dynamic-stuff').empty();
            cargar_marcas();

        })


    });


    function attach_delete() {
        $('.delete-row').off();
        $('.delete-row').click(function () {

            $(this).closest('.dynamic-element').remove();
        });
    }


    $( "#solicitud_form" ).submit(function( event ) {
        if($('.dynamic-stuff > div ').length===0){
            event.preventDefault();
        }


    });


    $("#producto_por_codigo_form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        if ($('#prd').attr('data-name')!='') {
            var clon = $('.dynamic-element').first().clone();
            clon.find("input").each(function () {
                this.name = this.name.replace('[]', '[' + counter + ']');
                this.id = this.id.replace('[]', '[' + counter + ']')
            });

            clon.find("select").each(function () {
                this.name = this.name.replace('[]', '[' + counter + ']');
                this.id = this.id.replace('[]', '[' + counter + ']')

                if (this.name == "marca[" + counter + "]") {
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();
            clon.find('.producto').append('<option value="' + $('#prd').attr("data-id") + '">' + $('#prd').attr("data-name") + '</option>');


            attach_delete();
            counter++;
        }



    });

    $("#producto_por_nombre_form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var clon=$('.dynamic-element').first().clone();
        clon.find("input").each(function(){
            this.name=this.name.replace('[]','['+counter+']');
            this.id=this.id.replace('[]','['+counter+']')
        });

        clon.find("select").each(function(){
            this.name=this.name.replace('[]','['+counter+']');
            this.id=this.id.replace('[]','['+counter+']')

            if (this.name=="marca["+counter+"]"){
                $(this).addClass('select2');
            }
        });

        clon.appendTo('.dynamic-stuff').show();
        clon.find('.producto').append('<option value="' + $('#prd2').val()+ '">' + $('#prd2 option:selected').text() + '</option>');


        attach_delete();
        counter++;
    });


    function c_marca_prod_form(selector){
        var product_selector=selector;
        $.ajax({

            url: "<?php echo base_url(); ?>marca_producto/fetch_marca_prod_form",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                product_selector.empty();

                // Add options
                $.each(data, function (index, data) {
                    product_selector.append('<option value="' + data['id'] + '">' + data['marca'] +' '+data['producto']+' '+data['cantidad_contenida']+' '+data['unidad']+ '</option>');
                });


            }
        });


    }

</script>



