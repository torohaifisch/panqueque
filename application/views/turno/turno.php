
<div class="wrapper">
    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Personal</a></li>
                            <li class="breadcrumb-item active">Turno</li>

                        </ol>
                    </div>
                    <h4 class="page-title">Turno</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">

                        <div class="col-sm-3">
                            <h5 class="text-custom">Usuario</h5>
                            <?php echo $this->session->userdata['nombre']?>

                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom">Fecha</h5>
                            <?php echo date('d-m-Y')?>

                        </div>
                        <div class="col-sm-3">
                            <h5 class="text-custom">Sucursal</h5>
                            <?php echo $sucursal->nombre?>

                        </div>
                        <div class="col-sm-3">
                            <h5 class="text-custom">Area</h5>
                            <?php echo $area->area?>

                        </div>




                    </div>

                </div>
            </div>
        </div>







        <div class="row">


            <div class="col-sm-4 offset-sm-4 text-center">
                <a class="btn btn-outline-danger waves-light waves-effect" href="<?php echo base_url().'Turno/retirar_productos'?>">Retirar Productos</a>


            </div>



        </div>


        <br>

        <div class="row">
            <div class="col-12">
                <div class="card-box">

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>

                            <th>N°</th>
                            <th>Sucursal</th>
                            <th>Area</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($solicitudes):
                            foreach ($solicitudes as $solicitud):
                                echo "<tr>";
                                echo "<td>".$solicitud->id_solicitud_pedido."</td>";
                                echo "<td>".$solicitud->sucursal."</td>";
                                echo "<td>".$solicitud->area."</td>";
                                echo "<td>".$solicitud->fecha."</td>";
                                if($solicitud->estado==2):
                                    echo '<td><span class="badge badge-warning">Pendiente de revision</span></td>';

                                elseif($solicitud->estado==1):
                                    echo '<td><span class="badge badge-success">Completa</span></td>';
                                else:
                                    echo '<td><span class="badge badge-success">Rechazada</span></td>';

                                endif;
                                echo "<td>
                                    <div class=\"btn-group dropdown\">
                                    <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                    <div class=\"dropdown-menu dropdown-menu-right\">
                                        <a class=\"dropdown-item\" href=\"".base_url().'turno/ver_pedido_urgente/'.$solicitud->id_solicitud_pedido."\"><i class=\"fa fa-search mr-2 text-muted font-18 vertical-middle\"></i>Ver Detalle</a>";


                                 echo "</div>
                                       </div>


                                      </td>";
                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>












        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>


<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>





<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        $('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });

    } );


    $('a#delete').click(function (event) {
        event.preventDefault();
        var url=$(this).attr('href');

        swal({
            title: '¿Estas seguro?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Si'
        }).then(function () {

            window.location.href=url;
        }).catch(swal.noop)
    });





</script>