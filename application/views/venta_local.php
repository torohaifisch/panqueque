
<style>

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }

</style>

<div class="wrapper">


    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>



    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Transacciones</a></li>
                            <li class="breadcrumb-item active">Venta Local</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Venta Local</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->





        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-block btn-custom waves-effect waves-light " data-toggle="modal" data-target="#nuevo_tipo_modal">Nuevo tipo de venta</button>

                        </div>
                    </div>

                </div>

            </div>

        </div>
            <!-- end col -->




            <!-- cliente modal content -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <form id="venta_form" role="form" method="post" action="<?php echo base_url();?>venta_local/crear">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="tipo_venta" >Tipo Venta</label>

                                    <select class="form-control" id="tipo_venta" name="tipo_venta" required>

                                    </select>
                                </div>

                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="monto" >Monto *</label>

                                    <input type="number" class="form-control" id="monto" name="monto" min="0" step="1"required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="sucursal" >Sucursal</label>

                                    <select class="form-control" id="sucursal" name="sucursal" required>

                                    </select>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="datepicker">Fecha *</label>
                                        <input type="text" class="form-control hasDatepicker" placeholder="aaaa-mm-dd"  id="datepicker" name="fecha" required>

                                    </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form-group">
                                    <label for="comentario">Comentario</label>
                                    <textarea class="form-control" type="text" id="comentario" name="comentario" ></textarea>

                                </div>

                            </div>

                        </div>



                        <div class="col-sm-12 text-right">
                            <button type="submit" class="btn btn-custom">Ingresar <i class="fa fa-spinner fa-spin" ></i></button>
                        </div>

                    </form>




                </div>




            </div>



            </div>
            <!-- end col -->



        <!-- Signup modal content -->
        <div id="nuevo_tipo_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <h2 class="text-center m-b-30">
                            Nuevo Tipo de Venta
                        </h2>

                        <form id="nuevo_tipo_form" role="form" method="post" action="<?php echo base_url();?>Tipo_venta/ajax_crear">
                            <div class="form-group">
                                <label for="tipo" >Nombre</label>

                                <input type="text" class="form-control" id="tipo" name="tipo" required>
                            </div>


                            <button type="submit" class="btn btn-primary">Crear <i class="fa fa-spinner fa-spin" ></i></button>
                        </form>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



    </div> <!-- end container -->

</div>

<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>
<script src="<?php echo base_url();?>plugins/select2/js/select2.min.js" type="text/javascript"></script>


<!--wizard initialization-->


<script>

    $( document ).ready(function() {

        cargar_tipos();
        cargar_sucursales();


        $('#venta_form i').hide();
        $('#nuevo_tipo_form i').hide();

        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: "es",
            todayHighlight: true



        });

    });


</script>

<script>
    // MODAL FORMS

    $("#venta_form").submit(function(e) {

        $('#venta_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("venta_form").reset();


                if(data){
                    swal_success();

                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#venta_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#nuevo_tipo_form").submit(function(e) {

        $('#nuevo_tipo_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("nuevo_tipo_form").reset();


                if(data){
                    swal_success();
                    $('#nuevo_tipo_modal').modal('toggle');
                    cargar_tipos();

                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#nuevo_tipo_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });




</script>


<script>

    function swal_error(){
        swal({
            type: 'error',
            title: 'Ups...',
            text: 'Error: no se pudo crear.'
        })
    }

    function swal_success(){
        swal(
            '¡Todo bien!',
            'Creado correctamente!',
            'success'
        );

    }


    function cargar_tipos(){


            var selected_val=$('#tipo_venta').val();
            var tipo=$('#tipo_venta');
            $.ajax({

                url: "<?php echo base_url(); ?>tipo_venta/fetch_tipos",
                method: "POST",
                dataType: 'json',
                success: function (data) {
                    tipo.empty();

                    // Add options
                    $.each(data, function (index, data) {
                        if (selected_val==null && index==0){
                            selected_val=data['id_tipo_venta'];
                        }
                        tipo.append('<option value="' + data['id_tipo_venta'] + '">' + data['tipo'] + '</option>');
                    });



                }
            });

    }

    function cargar_sucursales(){

        var selected_val=$('#sucursal').val();
        var tipo=$('#sucursal');
        $.ajax({

            url: "<?php echo base_url(); ?>sucursal/fetch_sucursales",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                tipo.empty();

                // Add options
                $.each(data, function (index, data) {
                    if (selected_val==null && index==0){
                        selected_val=data['id_sucursal'];
                    }
                    tipo.append('<option value="' + data['id_sucursal'] + '">' + data['nombre'] + '</option>');
                });



            }
        });

    }

</script>