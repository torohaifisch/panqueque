<style>


     input.error {
        background: rgb(251, 227, 228);
        border: 1px solid #fbc2c4;
        color: #8a1f11;
    }

     label.error
    {
        color: #f1556c;
        margin-left: 0;
    }

</style>

<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>






    <div class="container">


        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administrar</a></li>
                            <li class="breadcrumb-item"><a href="#">Proveedores</a></li>
                            <li class="breadcrumb-item active">Editar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Proveedor</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->




        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">

                    <form id="proveedor_form" role="form" method="post" action="<?php echo base_url();?>proveedor/editar/<?php echo $proveedor->id_proveedor?>">
                        <div class="form-group">
                            <label for="nombre" >Nombre</label>

                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $proveedor->nombre;?>" required>
                        </div>
                        <div class="form-group">
                            <label for="rut" >Rut</label>
                            <input type="text" class="form-control rut" id="viejo_rut" name="viejo_rut" value="<?php echo $proveedor->rut;?>" hidden required>

                            <input type="text" class="form-control rut" id="rut" name="rut" value="<?php echo $proveedor->rut;?>" >
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono</label>
                            <input class="form-control" type="number" id="telefono" name="telefono" value="<?php echo $proveedor->telefono;?>" >

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email@ejemplo.com" name="mail" value="<?php echo $proveedor->mail;?>">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>


                        <button type="submit" class="btn btn-primary">Actualizar </button>
                    </form>


                </div>

            </div>


        </div>




    </div>



</div>

<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/localization/messages_es.min.js"></script>

<script>




    $("#rut").rut();

    $.validator.addMethod("rut", function(value, element) {
        return this.optional(element) || $.validateRut(value);
    }, "Este campo debe ser un rut valido.");

    $("#proveedor_form").validate();
</script>
