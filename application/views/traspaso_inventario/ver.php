
<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item"><a href="#">Notificacion Transferencia</a></li>
                            <li class="breadcrumb-item active">Ver detalle</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Detalle Notificacion Transferencia N°<?php echo $notificacion->id_notificacion_transferencia?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">

                        <div class="col-sm-3">
                            <h5 class="text-custom">Nombre de Usuario</h5>
                            <?php echo $notificacion->nombre.' '.$notificacion->ape_pat?>

                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom">Origen</h5>
                            <?php echo $notificacion->origen?>

                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom" >Destino</h5>
                            <?php echo $notificacion->destino
                            ?>


                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom" >Estado</h5>
                            <?php
                            if($notificacion->estado==1):
                                echo '<span class="badge badge-success">Aceptada</span>';
                            endif;
                            if($notificacion->estado==2):
                                echo '<span class="badge badge-warning">En Transito</span>';
                            endif;
                            if($notificacion->estado==0):
                                echo '<span class="badge badge-dark">Rechazada</span>';
                            endif;
                            ?>


                        </div>



                    </div>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card-box">



                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>

                            <th>Producto</th>
                            <th>Formato</th>
                            <th>Cantidad</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($detalles):
                            foreach ($detalles as $detalle):
                                echo "<tr>";
                                echo "<td>".$detalle->producto."</td>";
                                echo "<td>".$detalle->medida."</td>";
                                echo "<td>".(float)$detalle->cantidad.' '.$detalle->medida."</td>";

                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>












        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>


<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>





<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        $('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });

    } );


    $('a#delete').click(function (event) {
        event.preventDefault();
        var url=$(this).attr('href');

        swal({
            title: '¿Estas seguro?',
            text: "No podrás revertir este cambio",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Si'
        }).then(function () {

            window.location.href=url;
        }).catch(swal.noop)
    });





</script>