
<style>

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }


     .modal input.error {
         background: rgb(251, 227, 228);
         border: 1px solid #fbc2c4;
         color: #8a1f11;
     }

    .modal label.error
    {
        color: #f1556c;
        margin-left: 0;
    }





</style>

<div class="wrapper">


    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>



    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Transacciones</a></li>
                            <li class="breadcrumb-item active">Registrar Compra</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Registrar Compra</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row dynamic-element" style="display:none">


            <div class="col-md-3">
                <div class="form-group">
                    <label for="producto">Producto *</label>
                    <select class="custom-select producto" id="producto[]" name="producto[]" required>


                    </select>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group ">
                    <label for="cantidad">Cantidad*</label>
                    <input class="form-control cantidad" type="number" id="cantidad[]" name="cantidad[]" min="1" step="1" required>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label for="valor_unitario">Valor Unitario Neto*</label>
                    <input class="form-control valor_unitario" type="number" id="valor_unitario[]" name="valor_unitario[]" min="0" step="1" required>

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label >Valores Previos</label>
                    <input class="form-control" type="number" id="valor_previo" readonly>

                </div>
            </div>

            <div class="col-md-2">

                <div class="form-group">
                    <label for="producto">Destino</label>
                    <select class="custom-select" id="destino[]" name="destino[]" required>
                        <option value="1">Reñaca</option>
                        <option value="2">Viña</option>

                    </select>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group ">

                    <br></br>

                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger delete-row"> <i class="fa fa-remove"></i> </button>
                </div>
            </div>


        </div>

        <!-- end dynamic -->



        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <div class="row">

                        <div class="col-sm-3">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#crear_marca_modal">Ingresar Marca</button>

                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#crear_producto_modal">Ingresar Producto</button>

                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#asignar_marca_modal">Asignar Producto a Marca</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#proveedor_modal">Crear Proveedor</button>
                        </div>


                    </div>
                </div>


                <form id="example-advanced-form" action="<?php echo base_url();?>Transaccion_compra/crear" method="post">

                    <h3>SII</h3>
                    <fieldset>
                        <legend>¿La cuenta a registrar se encuentra en el SII?</legend>


                        <div class="form-group">


                            <select class="custom-select mt-3" id="tipo_io" name="tipo_io" required >
                                <option value="2" selected >Compra</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <select class="custom-select mt-3" id="sii" name="sii" required>
                                <option value="1">Si</option>
                                <option value="2">No</option>

                            </select>
                        </div>


                    </fieldset>

                    <h3>DTE</h3>
                    <fieldset>
                        <legend>Informacion DTE</legend>

                        <div class="form-group">
                            <label for="proveedor">Proveedor</label>
                            <select class="form-control select2 " id="proveedor" name="proveedor" required>


                            </select>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tipo_documento">Tipo de documento *</label>
                                    <select class="custom-select mt-3" id="tipo_documento" name="tipo_documento" required>
                                        <option value="1">Afecto</option>
                                        <option value="2">Exento</option>


                                    </select>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tipo_dte">Tipo DTE *</label>
                                    <select class="custom-select mt-3" id="tipo_dte" name="tipo_dte" required>
                                        <option value="1">Boleta</option>
                                        <option value="2">Factura</option>

                                    </select>
                                </div>
                            </div>

                        </div>




                        <div class="row">




                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="numero_dte">Numero DTE *</label>
                                    <input class="form-control" type="number" id="numero_dte" name="numero_dte" min="0" required>

                                </div>
                            </div>


                        </div>


                        <div class="form-group">
                            <label for="datepicker">Fecha de Emision *</label>
                            <input type="text" class="form-control hasDatepicker" placeholder="aaaa-mm-dd"  id="datepicker" name="fecha_emision" required>

                        </div>



                        <div class="form-group">
                            <label for="datepicker2">Fecha de Vencimiento *</label>
                            <input type="text" class="form-control hasDatepicker" placeholder="aaaa-mm-dd"  id="datepicker2" name="fecha_vencimiento" required>

                        </div>







                    </fieldset>

                    <h3>Detalle</h3>
                    <fieldset>
                        <legend>Detalle de Productos</legend>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="monto">Monto Total Neto *</label>

                                    <input class="form-control" type="number" name="monto" id="monto" min="0" step="1" readonly required>

                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-block btn-info waves-effect waves-light" data-toggle="modal" data-target="#producto_por_codigo_modal" >Agregar producto por codigo</button>

                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-block btn-info waves-effect waves-light" data-toggle="modal" data-target="#producto_por_nombre_modal" >Agregar producto por nombre</button>

                            </div>
                        </div>
                        <hr>
                        <div class="dynamic-stuff">





                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-block btn-info waves-effect waves-light" data-toggle="modal" data-target="#producto_por_codigo_modal" >Agregar producto por codigo</button>

                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-block btn-info waves-effect waves-light" data-toggle="modal" data-target="#producto_por_nombre_modal" >Agregar producto por nombre</button>

                            </div>
                        </div>







                    </fieldset>
                </form>





            </div>
            <!-- end col -->


            <!-- Signup modal content -->
            <div id="asignar_marca_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Asignar Producto a Marca
                            </h2>

                            <form id="asignar_marca_form"role="form" method="post" action="<?php echo base_url();?>marca_producto/crear">

                                <div class="form-group">
                                    <label for="marca">Marca *</label>
                                    <select class="custom-select marca" id="marca" name="marca" required>


                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="cantidad" >SKU*</label>

                                    <input type="number" class="form-control" id="sku" name="sku"  min="0" required>
                                </div>





                                <div class="form-group">
                                    <label for="producto">Producto *</label>
                                    <select class="custom-select producto" id="producto" name="producto" required>


                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="cantidad" >Cantidad Contenida*</label>

                                            <input type="number" class="form-control" id="cantidad" name="cantidad"  min="0" step="0.001" required>
                                        </div>


                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="unidad" >Unidad de Medida*</label>

                                            <select class="form-control" id="unidad" name="unidad" required>

                                            </select>
                                        </div>

                                    </div>
                                </div>




                                <button type="submit" class="btn btn-primary">Asignar <i class="fa fa-spinner fa-spin" ></i></button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Signup modal content -->
            <div id="crear_marca_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Nueva Marca
                            </h2>

                            <form id="marca_form" role="form" method="post" action="<?php echo base_url();?>marca/crear">
                                <div class="form-group">
                                    <label for="nombre" >Nombre</label>

                                    <input type="text" class="form-control" id="nombre" name="nombre" required>
                                </div>


                                <button type="submit" class="btn btn-primary">Crear <i class="fa fa-spinner fa-spin" ></i></button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <div id="crear_producto_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Nuevo Producto
                            </h2>

                            <form id="producto_form" role="form" method="post" action="<?php echo base_url();?>producto/crear">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="nombre" >Nombre</label>

                                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                                        </div>


                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="tipo" >Tipo</label>

                                            <select class="form-control" id="tipo" name="tipo" required>

                                            </select>
                                        </div>

                                    </div>





                                </div>


                                <button type="submit" class="btn btn-primary">Crear <i class="fa fa-spinner fa-spin" ></i></button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Signup modal content -->
            <div id="proveedor_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Nuevo Proveedor
                            </h2>

                            <form role="form" id="proveedor_form" method="post" action="<?php echo base_url();?>proveedor/crear">
                                <div class="form-group">
                                    <label for="nombre" >Nombre</label>

                                    <input type="text" class="form-control" id="nombre" name="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label for="rut" >Rut</label>

                                    <input type="text" class="form-control rut" id="rut" name="rut" required>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Teléfono</label>
                                    <input class="form-control" type="number" id="telefono" name="telefono" >

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email@ejemplo.com" name="mail">
                                </div>


                                <button type="submit" class="btn btn-primary">Crear</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <div id="producto_por_codigo_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Agregar Producto
                            </h2>

                            <form  id="producto_por_codigo_form" role="form" method="post" action="">
                                <div class="row">



                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="codigo">Codigo*</label>
                                            <input type="text" class="form-control" id="codigo" name="codigo" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="prd" >Producto*</label>

                                            <input type="text" class="form-control" data-id="" data-name="" id="prd" name="prd" readonly required>
                                        </div>


                                    </div>




                                </div>


                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <div id="producto_por_nombre_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Agregar Producto
                            </h2>

                            <form id="producto_por_nombre_form" role="form" method="post" action="">
                                <div class="row">


                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="pro">Producto*</label>
                                            <select class="select2" id="prd2" name="prd2" required>


                                            </select>
                                        </div>
                                    </div>




                                </div>


                                <button type="submit" class="btn btn-primary">Agregar</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->




        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!--Form Wizard-->
<script src="<?php echo base_url();?>plugins/jquery.steps/js/jquery.steps.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/localization/messages_es.min.js"></script>

<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>
<script src="<?php echo base_url();?>plugins/select2/js/select2.min.js" type="text/javascript"></script>



<!--wizard initialization-->
<script >


    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        transitionEffectSpeed:100,
        enableKeyNavigation:false,
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";

            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {

        },
        onFinishing: function (event, currentIndex)
        {

            if($('.dynamic-stuff > div ').length===0){
                return false;
            }
            form.validate().settings.ignore = ":disabled";

            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            $('#tipo_documento').removeAttr('disabled');
            form.submit();
        },


        labels: {
            cancel: "Cancelar",
            current: "Paso Actual:",
            pagination: "Paginación",
            finish: "Finalizar",
            next: "Siguiente",
            previous: "Anterior",
            loading: "Cargando ..."
        }
    });


</script>



<script>

    var counter=1;

    $( document ).ready(function() {
        $('#prd2').select2({
            dropdownParent: $('#producto_por_nombre_modal')
        });

        $('#proveedor').select2();

        $("#codigo").keyup(function(){
            var search = $(this).val();
            $("#prd").val('');
            $("#prd").attr('data-id','');
            $("#prd").attr('data-name','');
            if(search != ""){

                $.ajax({

                    url: "<?php echo base_url(); ?>marca_producto/busca_prod",
                    method: "POST",
                    dataType: 'json',
                    data: {sku:$(this).val()},
                    success: function (data) {


                        // Add options


                        $("#prd").val(data['marca'] +' '+ data['producto'] +' '+ data['cantidad_contenida']+' '+ data['unidad']);
                        $("#prd").attr('data-id',data['id']);
                        $("#prd").attr('data-name',data['marca'] +' '+ data['producto'] +' '+ data['cantidad_contenida']+' '+ data['unidad']);




                    }
                });

            }

        });




        $("#rut").rut();

        $.validator.addMethod("rut", function(value, element) {
            return this.optional(element) || $.validateRut(value);
        }, "Este campo debe ser un rut valido.");

        $("#proveedor_form").validate();




        $("#tipo_io").parent('.form-group').hide();

        $(document).on('input','.cantidad',function(){ $('#monto').val(calcular_monto());});
        $(document).on('input','.valor_unitario',function(){ $('#monto').val(calcular_monto());});

        cargar_tipo();
        cargar_proveedores();
        cargar_marcas();
        cargar_unidades();
        c_marca_prod_form($('#prd2'));
        c_prod($('#producto'));
        c_prod($('#producto2'));

        $('#producto_form i').hide();
        $('#proveedor_form i').hide();
        $('#marca_form i').hide();
        $('#asignar_formato_form i').hide();
        $('#asignar_marca_form i').hide();

        $('.marca').select2({
            dropdownParent: $('#asignar_marca_modal')
        });

        $('#producto').select2({
            dropdownParent: $('#asignar_marca_modal')
        });


        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: "es",
            todayHighlight: true


        });

        $('#datepicker2').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: "es",
            todayHighlight: true

        });


        $("#example-advanced-form").validate({

            errorPlacement: function (error, element) {
                if(element.hasClass('select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                }
                else {
                    error.insertAfter(element);
                }
            }



        });




        //Clone the hidden element and shows it
        $('.add-dynamic').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();
            clon.find('.producto').append('<option value="' + $('#prd').attr("data-id") + '">' + $('#prd').attr("data-name") + '</option>');


            attach_delete();
            counter++;
            $(".select2").select2();


        });

        $('.add-dynamic2').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();
            clon.find('.producto').append('<option value="' + $('#prd').attr("data-id") + '">' + $('#prd').attr("data-name") + '</option>');


            attach_delete();
            counter++;
            $(".select2").select2();


        });


        //Attach functionality to delete buttons








    });


    function attach_delete() {
        $('.delete-row').off();
        $('.delete-row').click(function () {

            $(this).closest('.dynamic-element').remove();
        });
    }

    $('#sii').on('change', function(){

        var valor = this.value;
        if (valor == '1') {


            $('#tipo_documento').removeAttr('disabled');
            $('#numero_dte').attr('required',true);
            $('label[for=numero_dte]').show();
            $('#numero_dte').show();
            $("#tipo_dte option[value='3']").remove();
            $('label[for=tipo_dte]').show();
            $('#tipo_dte').show();

        } else if (valor == '2') {
            $('#numero_dte').removeAttr('required');
            $('#numero_dte').hide();
            $('label[for=numero_dte]').hide();
            $("#tipo_documento").val('2').change();
            $("#tipo_documento").attr('disabled', 'disabled');

            $('#tipo_dte').append('<option value="3" selected="selected">Negro</option>');
            $('label[for=tipo_dte]').hide();
            $('#tipo_dte').hide();


        }
        return false;
    });


    // MODAL FORMS

    $("#producto_form").submit(function(e) {

        $('#producto_form i').show();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("producto_form").reset();
                $('#producto_modal').modal('toggle');

                if(data==1){

                    swal_success();



                }
                else{

                    swal_error();
                }

            },
            complete: function(data){

                $('#producto_form i').hide();
                c_prod($('#producto'));
                c_prod($('#producto2'));

            },
            error: function (data) {

                swal_error();

            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });




    $("#marca_form").submit(function(e) {

        $('#marca_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("marca_form").reset();
                $('#marca_modal').modal('toggle');

                if(data==1){
                    swal_success();
                    cargar_marcas();
                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#marca_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    $("#asignar_marca_form").submit(function(e) {

        $('#asignar_marca_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("asignar_marca_form").reset();
                $("#producto").val(null).trigger("change");
                $(".marca").val(null).trigger("change");

                $('#asignar_marca_modal').modal('toggle');

                if(data==1){
                    swal_success();
                    c_marca_prod_form($('#prd2'));


                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#asignar_marca_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    $("#proveedor_form").submit(function(e) {

        $('#proveedor_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("proveedor_form").reset();
                $('#proveedor_modal').modal('toggle');

                if(data==1){
                    swal_success();
                    cargar_proveedores();
                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#proveedor_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });



    $("#producto_por_codigo_form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        if ($('#prd').attr('data-name')!='') {
            var clon = $('.dynamic-element').first().clone();
            clon.find("input").each(function () {
                this.name = this.name.replace('[]', '[' + counter + ']');
                this.id = this.id.replace('[]', '[' + counter + ']')
            });

            clon.find("select").each(function () {
                this.name = this.name.replace('[]', '[' + counter + ']');
                this.id = this.id.replace('[]', '[' + counter + ']')

                if (this.name == "marca[" + counter + "]") {
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();
            clon.find('.producto').append('<option value="' + $('#prd').attr("data-id") + '">' + $('#prd').attr("data-name") + '</option>');


            attach_delete();
            counter++;

        }


    });

    $("#producto_por_nombre_form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var clon=$('.dynamic-element').first().clone();
        clon.find("input").each(function(){
            this.name=this.name.replace('[]','['+counter+']');
            this.id=this.id.replace('[]','['+counter+']')
        });

        clon.find("select").each(function(){
            this.name=this.name.replace('[]','['+counter+']');
            this.id=this.id.replace('[]','['+counter+']')

            if (this.name=="marca["+counter+"]"){
                $(this).addClass('select2');
            }
        });

        clon.appendTo('.dynamic-stuff').show();
        clon.find('.producto').append('<option value="' + $('#prd2').val()+ '">' + $('#prd2 option:selected').text() + '</option>');
        fetch_ultimo($('#prd2').val(),clon.find('#valor_previo'));

        attach_delete();
        counter++;
    });


</script>


<script>
    // Helpers, cargar dato en selects


    function cargar_proveedores() {

        var selected_val=$('#proveedor').val();

        $.ajax({

            url: "<?php echo base_url(); ?>Proveedor/fetch_proveedores",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                $('#proveedor').empty();

                // Add options
                $.each(data, function (index, data) {
                    $('#proveedor').append('<option value="' + data['id_proveedor'] + '">' + data['nombre'] + '</option>');
                });

                if (selected_val!=null){
                    $('#proveedor').val(selected_val);
                }

            }
        });
    }

    function cargar_marcas(){


        $.ajax({

            url: "<?php echo base_url(); ?>Marca/fetch_marcas",
            method: "POST",
            dataType: 'json',
            success: function (data) {

                $('.marca').each(function(){
                    var selected_val=$(this).val();
                    var marca=$(this);

                    marca.empty();

                    // Add options
                    $.each(data, function (index, data) {
                        if (selected_val==null && index==0){
                            selected_val=data['id_marca'];
                        }
                        marca.append('<option value="' + data['id_marca'] + '">' + data['nombre'] + '</option>');
                    });

                });

            }
        });

    }


    function calcular_monto(){
        var total=0;
        $('.cantidad').each(function(){
           var valor=$(this).closest('.dynamic-element').find('.valor_unitario').val();
           var cantidad =$(this).val();
           total+=valor*cantidad;

       });
        return total;
    }




    function cargar_tipo(){

        var selected_val=$('#tipo').val();
        var tipo=$('#tipo');
        $.ajax({

            url: "<?php echo base_url(); ?>producto/fetch_tipos",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                tipo.empty();

                // Add options
                $.each(data, function (index, data) {
                    if (selected_val==null && index==0){
                        selected_val=data['id_tipo_producto'];
                    }
                    tipo.append('<option value="' + data['id_tipo_producto'] + '">' + data['tipo'] + '</option>');
                });



            }
        });

    }


    function c_prod(selector){
        var product_selector=selector;
        $.ajax({

            url: "<?php echo base_url(); ?>producto/fetch_productos",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                product_selector.empty();

                // Add options
                $.each(data, function (index, data) {
                    product_selector.append('<option value="' + data['id_producto'] + '">' + data['nombre'] + '</option>');
                });


            }
        });


    }

    function c_marca_prod_form(selector){
        var product_selector=selector;
        $.ajax({

            url: "<?php echo base_url(); ?>marca_producto/fetch_marca_prod_form",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                product_selector.empty();

                // Add options
                $.each(data, function (index, data) {
                    product_selector.append('<option value="' + data['id'] + '">' + data['marca'] +' '+data['producto']+' '+data['cantidad_contenida']+' '+data['unidad']+ '</option>');
                });


            }
        });


    }



    function cargar_unidades(){

        var unidad=$('#unidad');
        $.ajax({

            url: "<?php echo base_url(); ?>producto/fetch_unidades",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                unidad.empty();

                // Add options
                $.each(data, function (index, data) {

                    unidad.append('<option value="' + data['id_unidad_medida'] + '">' + data['medida'] + '</option>');
                });



            }
        });

    }

    function fetch_ultimo(id,selector){

        $.ajax({

            url: "<?php echo base_url(); ?>transaccion_compra/fetch_ultimo_precio",
            method: "POST",
            data: {id_mpf:id},
            dataType: 'json',
            success: function (data) {
                selector.empty();

                // Add options
                selector.val(data)



            }
        });
    }

    function swal_error(){
        swal({
            type: 'error',
            title: 'Ups...',
            text: 'Error: no se pudo crear.'
        })
    }

    function swal_success(){
        swal(
            '¡Todo bien!',
            'Creado correctamente!',
            'success'
        );

    }




</script>