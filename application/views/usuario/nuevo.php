<style>


    input.error {
        background: rgb(251, 227, 228);
        border: 1px solid #fbc2c4;
        color: #8a1f11;
    }

    label.error
    {
        color: #f1556c;
        margin-left: 0;
    }

</style>

<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>






    <div class="container">


        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administrar</a></li>
                            <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
                            <li class="breadcrumb-item active">Crear</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Nuevo Usuario</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->




        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">

                    <form id="cliente_form" role="form" method="post" action="<?php echo base_url();?>Usuario/crear_usuario">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre" >Nombre de Usuario</label>

                                    <input type="text" class="form-control" id="usuario" name="usuario"  required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="rut" >Password</label>
                                    <input type="text" class="form-control rut" id="pass" name="pass"  required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre" >Nombre</label>

                                    <input type="text" class="form-control" id="nombre" name="nombre"  required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre" >Apellido</label>

                                    <input type="text" class="form-control" id="ape_pat" name="ape_pat"  required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sucursal" >Sucursal</label>
                                    <select class="custom-select " id="sucursal" name="sucursal">
                                        <option value=""></option>
                                        <?php if($sucursales):
                                            foreach($sucursales as $sucursal):
                                                echo '<option value="'.$sucursal->id_sucursal.'">'. $sucursal->nombre.'</option>';

                                            endforeach;
                                        endif


                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="area" >Area</label>
                                    <select class="custom-select area" id="area" name="area">
                                        <option value=""></option>
                                        <?php if($areas):
                                            foreach($areas as $area):
                                                echo '<option value="'.$area->id_area_trabajo.'">'. $area->area.'</option>';

                                            endforeach;
                                        endif


                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>













                        <div class="form-group">
                            <label for="tipo" >Tipo de Usuario</label>
                            <select class="custom-select " id="tipo" name="tipo">
                                <?php if($tipos):
                                    foreach($tipos as $tipo):
                                        echo '<option value="'.$tipo->id_tipo_usuario.'">'. $tipo->nombre.'</option>';

                                    endforeach;
                                endif


                                ?>
                            </select>
                        </div>

                        <div class="col-sm-12 text-right">
                            <button type="submit" class="btn btn-custom">Crear </button>

                        </div>

                    </form>


                </div>

            </div>


        </div>




    </div>



</div>

<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/localization/messages_es.min.js"></script>

