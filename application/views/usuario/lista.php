<style>


    .modal input.error {
        background: rgb(251, 227, 228);
        border: 1px solid #fbc2c4;
        color: #8a1f11;
    }

    .modal label.error
    {
        color: #f1556c;
        margin-left: 0;
    }

</style>




<div class="wrapper">
    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administrar</a></li>
                            <li class="breadcrumb-item active">Clientes</li>

                        </ol>
                    </div>
                    <h4 class="page-title">Usuarios</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>

                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Sucursal</th>
                            <th>Area</th>
                            <th>tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($usuarios):
                            foreach ($usuarios as $usuario):
                                echo "<tr>";
                                echo "<td>".$usuario->nombre_usuario."</td>";
                                echo "<td>".$usuario->nombre."</td>";
                                echo "<td>".$usuario->sucursal."</td>";
                                echo "<td>".$usuario->area."</td>";
                                echo "<td>".$usuario->tipo."</td>";
                                echo "<td>
                                            <div class=\"btn-group dropdown\">
                                            <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                            <div class=\"dropdown-menu dropdown-menu-right\">
                                                <a class=\"dropdown-item\" href=\"".base_url()."\"><i class=\"mdi mdi-pencil mr-2 text-muted font-18 vertical-middle\"></i>Editar</a>
                                                <a class=\"dropdown-item\" id='delete' href=\"".base_url()."\"><i class=\"mdi mdi-delete mr-2 text-muted font-18 vertical-middle\"></i>Desactivar</a>
                                            </div>
                                        </div>
                                        </td>";
                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>





            <!-- Signup modal content -->
            <div id="login-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Nuevo Cliente
                            </h2>

                            <form role="form" id="cliente_form" method="post" action="<?php echo base_url();?>cliente/crear">
                                <div class="form-group">
                                    <label for="nombre" >Nombre</label>

                                    <input type="text" class="form-control" id="nombre" name="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label for="rut" >Rut</label>

                                    <input type="text" class="form-control rut" id="rut" name="rut" required>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">Teléfono</label>
                                    <input class="form-control" type="number" id="telefono" name="telefono" >

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email@ejemplo.com" name="mail">
                                </div>


                                <button type="submit" class="btn btn-primary">Crear</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->






        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>



<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/localization/messages_es.min.js"></script>

<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#rut").rut();

        // Default Datatable
        $('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });


        $.validator.addMethod("rut", function(value, element) {
            return this.optional(element) || $.validateRut(value);
        }, "Este campo debe ser un rut valido.");

        $("#cliente_form").validate();




    } );


    $('a#delete').click(function (event) {
        event.preventDefault();
        var url=$(this).attr('href');

        swal({
            title: '¿Estas seguro?',
            text: "No podrás revertir este cambio",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Si'
        }).then(function () {

            window.location.href=url;
        }).catch(swal.noop)
    });




</script>