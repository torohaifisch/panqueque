<!doctype html>

<style>
    @media (max-width: 768px) {
        .account-page-full {
            width: 100% !important;
        }
    }
</style>


<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Panqueque - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">

    <!-- App css -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>

</head>

<body>

<!-- Begin page -->
<div class="accountbg" style="background: url('assets/images/bg-1.jpg');background-size: cover;background-position: center;"></div>

<div class="wrapper-page account-page-full">

    <div class="card">
        <div class="card-block">

            <div class="account-box">

                <div class="card-box p-5">
                    <h2 class="text-uppercase text-center pb-4">
                        <a href="#" class="text-success">
                            <span><img src="<?php echo base_url();?>assets/images/logo_negro.png" alt="" height="50"></span>
                        </a>
                    </h2>

                    <form class="" action="<?php echo base_url();?>Login/validar" method="post">

                        <div class="form-group m-b-20 row">
                            <div class="col-12">
                                <label for="emailaddress">Nombre de Usuario</label>
                                <input class="form-control" type="text" id="user" name='user' required="" placeholder="Usuario">
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <a href="page-recoverpw.html" class="text-muted float-right"><small>¿Olvidaste tu  clave?</small></a>
                                <label for="password">Password</label>
                                <input class="form-control" type="password" required="" id="password" name='pass' placeholder="Password">
                            </div>
                        </div>



                        <div class="form-group row text-center m-t-10">
                            <div class="col-12">
                                <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                    </form>



                </div>
            </div>

        </div>
    </div>

    <div class="m-t-40 text-center">
        <p class="account-copyright">2019 © Wenic.</p>
    </div>

</div>


<!-- jQuery  -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>

<!-- App js -->
<script src="<?php echo base_url();?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.app.js"></script>

</body>
</html>