
<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administrar</a></li>
                            <li class="breadcrumb-item active">Marcas y Productos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Marcas y Productos</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="card-box">

            <div class="row">

                        <div class="col-sm-4">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#crear_marca_modal">Ingresar Marca</button>

                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#crear_producto_modal">Ingresar Producto</button>

                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-custom btn-block  " data-toggle="modal" data-target="#asignar_marca_modal">Asignar Producto a Marca</button>
                        </div>



            </div>
        </div>


            <div class="card-box">


                <ul class="nav nav-pills navtab-bg nav-justified pull-in ">
                    <li class="nav-item">
                        <a href="#home1" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <i class="fa fa-tags mr-2"></i> Marcas
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#profile1" data-toggle="tab" aria-expanded="true" class="nav-link ">
                            <i class="fa fa-product-hunt mr-2"></i>Productos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#messages1" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <i class="fa fa-cubes mr-2"></i> Marcas y Productos
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane show active" id="home1">

                        <table id="marcas" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>

                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>


                            <tbody>
                            <?php if ($marcas):
                                foreach ($marcas as $marca):
                                    echo "<tr>";
                                    echo "<td>".$marca->nombre."</td>";
                                    echo "<td>
                                   <div class=\"btn-group dropdown\">
                                        <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                        <div class=\"dropdown-menu dropdown-menu-right\">
                                            <a class=\"dropdown-item\" href=\"#\"><i class=\"mdi mdi-pencil mr-2 text-muted font-18 vertical-middle\"></i>Editar Marca</a>
                                            <a class=\"dropdown-item\" id='delete' href=\"".base_url().'marca/borrar/'.$marca->id_marca."\"><i class=\"mdi mdi-delete mr-2 text-muted font-18 vertical-middle\"></i>Borrar</a>
                                        </div>
                                    </div>
                                  </td>";
                                    echo "</tr>";
                                endforeach;
                            endif;
                            ?>
                            </tbody>
                        </table>


                    </div>
                    <div class="tab-pane" id="profile1">
                        <table id="productos" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>

                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>


                            <tbody>
                            <?php if ($productos):
                                foreach ($productos as $producto):
                                    echo "<tr>";
                                    echo "<td>".$producto->nombre."</td>";
                                    echo "<td>".$producto->tipo."</td>";
                                    echo "<td>
                                <div class=\"btn-group dropdown\">
                                <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                <div class=\"dropdown-menu dropdown-menu-right\">
                                    <a class=\"dropdown-item\" href=\"#\"><i class=\"mdi mdi-pencil mr-2 text-muted font-18 vertical-middle\"></i>Editar Producto</a>
                                    <a class=\"dropdown-item\" id='delete' href=\"".base_url().'producto/borrar/'.$producto->id_producto."\"><i class=\"mdi mdi-delete mr-2 text-muted font-18 vertical-middle\"></i>Borrar</a>
                                </div>
                                </div>


                                  </td>";
                                    echo "</tr>";
                                endforeach;
                            endif;
                            ?>
                            </tbody>
                        </table>


                    </div>
                    <div class="tab-pane" id="messages1">

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>

                                <th>Marca</th>
                                <th>Producto</th>
                                <th>Formato</th>
                                <th>Acciones</th>

                            </tr>
                            </thead>


                            <tbody>
                            <?php if ($items):
                                foreach ($items as $item):
                                    echo "<tr>";
                                    echo "<td>".$item->marca."</td>";
                                    echo "<td>".$item->producto."</td>";
                                    echo "<td>".$item->cantidad_contenida.' '.$item->unidad."</td>";
                                    echo "<td>
                                <div class=\"btn-group dropdown\">
                                <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                <div class=\"dropdown-menu dropdown-menu-right\">
                                    <a class=\"dropdown-item\" href=\"".base_url().'marca_producto/editar_marca_producto/'.$item->id."\"><i class=\"mdi mdi-pencil mr-2 text-muted font-18 vertical-middle\"></i>Editar</a>
                                </div>
                                </div>


                                  </td>";
                                    echo "</tr>";
                                endforeach;
                            endif;
                            ?>
                            </tbody>
                        </table>


                    </div>

                </div>
            </div>

        </div>
        <!-- end row -->









            <!-- Signup modal content -->
            <div id="asignar_marca_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Asignar Producto a Marca
                            </h2>

                            <form id="asignar_marca_form"role="form" method="post" action="<?php echo base_url();?>marca_producto/crear">

                                    <div class="form-group">
                                        <label for="marca">Marca *</label>
                                        <select class="custom-select marca" id="marca" name="marca" required>


                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="cantidad" >SKU*</label>

                                        <input type="number" class="form-control" id="sku" name="sku"  min="0" required>
                                    </div>





                                    <div class="form-group">
                                        <label for="producto">Producto *</label>
                                        <select class="custom-select producto" id="producto" name="producto" required>


                                        </select>
                                    </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="cantidad" >Cantidad Contenida*</label>

                                            <input type="number" class="form-control" id="cantidad" name="cantidad"  min="0" step="0.001" required>
                                        </div>


                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="unidad" >Unidad de Medida*</label>

                                            <select class="form-control" id="unidad" name="unidad" required>

                                            </select>
                                        </div>

                                    </div>
                                </div>




                                <button type="submit" class="btn btn-primary">Asignar <i class="fa fa-spinner fa-spin" ></i></button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Signup modal content -->
            <div id="crear_marca_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Nueva Marca
                            </h2>

                            <form id="marca_form" role="form" method="post" action="<?php echo base_url();?>marca/crear">
                                <div class="form-group">
                                    <label for="nombre" >Nombre</label>

                                    <input type="text" class="form-control" id="nombre" name="nombre" required>
                                </div>


                                <button type="submit" class="btn btn-primary">Crear <i class="fa fa-spinner fa-spin" ></i></button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <div id="crear_producto_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30">
                                Nuevo Producto
                            </h2>

                            <form id="producto_form" role="form" method="post" action="<?php echo base_url();?>producto/crear">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="nombre" >Nombre</label>

                                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                                        </div>


                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="tipo" >Tipo</label>

                                            <select class="form-control" id="tipo" name="tipo" required>

                                            </select>
                                        </div>

                                    </div>





                                </div>


                                <button type="submit" class="btn btn-primary">Crear <i class="fa fa-spinner fa-spin" ></i></button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>


<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>




<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>
<script src="<?php echo base_url();?>plugins/select2/js/select2.min.js" type="text/javascript"></script>


<script type="text/javascript">
    $(document).ready(function() {

        $('#producto_form i').hide();
        $('#asignar_formato_form i').hide();
        $('#asignar_marca_form i').hide();
        $('#marca_form i').hide();

        $('.marca').select2({
            dropdownParent: $('#asignar_marca_modal')
        });

        $('#producto').select2({
            dropdownParent: $('#asignar_marca_modal')
        });

        $('#producto2').select2({
            dropdownParent: $('#asignar_formato_modal')
        });


        cargar_tipo();

        // Default Datatable
        $('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });

        $('#marcas').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });



        $('#productos').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });


        cargar_marcas();
        cargar_productos($('#producto'));
        cargar_productos($('#producto2'));
        cargar_unidades();


    } );


    $("#producto_form").submit(function(e) {

        $('#producto_form i').show();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("producto_form").reset();
                $('#producto_modal').modal('toggle');

                if(data==1){

                    swal_success();



                }
                else{

                    swal_error();
                }

            },
            complete: function(data){

                $('#producto_form i').hide();
                cargar_productos($('#producto'));
                cargar_productos($('#producto2'));

            },
            error: function (data) {

                swal_error();

            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });




    $("#marca_form").submit(function(e) {

        $('#marca_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("marca_form").reset();
                $('#marca_modal').modal('toggle');

                if(data==1){
                    swal_success();
                    cargar_marcas();
                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#marca_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    $("#asignar_marca_form").submit(function(e) {

        $('#asignar_marca_form i').show();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                document.getElementById("asignar_marca_form").reset();
                $("#producto").val(null).trigger("change");
                $(".marca").val(null).trigger("change");

                $('#asignar_marca_modal').modal('toggle');

                if(data==1){
                    swal_success();

                }
                else{
                    swal_error();
                }


            },
            complete: function(data){

                $('#asignar_marca_form i').hide();
            },
            error: function (data) {

                swal_error();
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });






    function cargar_marcas(){

        $('.marca').each(function(){
            var selected_val=$(this).val();
            var marca=$(this);
            $.ajax({

                url: "<?php echo base_url(); ?>Marca/fetch_marcas",
                method: "POST",
                dataType: 'json',
                success: function (data) {
                    marca.empty();

                    // Add options
                    $.each(data, function (index, data) {
                        if (selected_val==null && index==0){
                            selected_val=data['id_marca'];
                        }
                        marca.append('<option value="' + data['id_marca'] + '">' + data['nombre'] + '</option>');
                    });



                }
            });

        });



    }


    function cargar_productos(selector){
        var product_selector=selector;
        $.ajax({

            url: "<?php echo base_url(); ?>producto/fetch_productos",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                product_selector.empty();

                // Add options
                $.each(data, function (index, data) {
                    product_selector.append('<option value="' + data['id_producto'] + '">' + data['nombre'] + '</option>');
                });


            }
        });


    }



    function cargar_tipo(){

        var selected_val=$('#tipo').val();
        var tipo=$('#tipo');
        $.ajax({

            url: "<?php echo base_url(); ?>producto/fetch_tipos",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                tipo.empty();

                // Add options
                $.each(data, function (index, data) {
                    if (selected_val==null && index==0){
                        selected_val=data['id_tipo_producto'];
                    }
                    tipo.append('<option value="' + data['id_tipo_producto'] + '">' + data['tipo'] + '</option>');
                });



            }
        });

    }

    function cargar_unidades(){

        var unidad=$('#unidad');
        $.ajax({

            url: "<?php echo base_url(); ?>producto/fetch_unidades",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                unidad.empty();

                // Add options
                $.each(data, function (index, data) {

                    unidad.append('<option value="' + data['id_unidad_medida'] + '">' + data['medida'] + '</option>');
                });



            }
        });

    }


    $('a#delete').click(function (event) {
        event.preventDefault();
        var url=$(this).attr('href');

        swal({
            title: '¿Estas seguro?',
            text: "No podrás revertir este cambio",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Si'
        }).then(function () {

            window.location.href=url;
        }).catch(swal.noop)
    });




    function swal_error(){
        swal({
            type: 'error',
            title: 'Ups...',
            text: 'Error: no se pudo crear.'
        })
    }

    function swal_success(){
        swal(
            '¡Todo bien!',
            'Creado correctamente!',
            'success'
        );

    }
</script>