
<div class="wrapper">
    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>

                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->



    <div class ="row">


        <div class="col-sm-6 col-xl-4">
            <div class="card-box widget-flat border-success bg-success text-white">
                <i class="fa fa-arrow-up "></i>
                <?php
                    $con=0;
                    if ($ingreso_dia):
                        $con=$ingreso_dia;

                    endif;
                     echo "<h3 class=\"m-b-10\">$".number_format($con,0,",",".")."</h3>";

                ?>
                <p class="text-uppercase m-b-5 font-13 font-600">Ingreso Del Día</p>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4">
            <div class="card-box bg-danger widget-flat border-danger text-white">
                <i class="fa fa-arrow-down"></i>

                <?php
                    $contador=0;
                    if ($egresos):

                    foreach ($egresos as $egreso):
                        $contador+=$egreso->monto;
                    endforeach;



                    endif;
                    echo "<h3 class=\"m-b-10\">$".number_format($contador,0,",",".")."</h3>";

                ?>
                <p class="text-uppercase m-b-5 font-13 font-600">Egreso Del Día</p>
            </div>
        </div>







    </div>

        <div class="row">
            <div class="col-sm-6 col-md-4 ">
                <div class="card-box tilebox-one">
                    <i class="fa fa-warning float-right text-warning"></i>
                    <h6 class="text-muted text-uppercase ">Pedidos Urgentes</h6>
                    <?php echo '<h2 class="m-b-20 text-center "><span id="num">'.number_format($solicitud_compra_pendiente,0,",",".").'</span></h2>' ?>
                    <a class="btn btn-block btn-primary waves-light waves-effect" href="<?php echo base_url();?>turno/lista_pedido_urgente" >Ver</a>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 ">
                <div class="card-box tilebox-one">
                    <i class="fa fa-warning float-right text-warning"></i>
                    <h6 class="text-muted text-uppercase ">Productos críticos (Bodegas Generales)</h6>
                    <?php echo '<h2 class="m-b-20 text-center "><span id="num">'.number_format($critico,0,",",".").'</span></h2>' ?>
                    <a class="btn btn-block btn-primary waves-light waves-effect" href="<?php echo base_url();?>bodega" >Ver</a>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 ">
                <div class="card-box tilebox-one">
                    <i class="fa fa-warning float-right text-warning"></i>
                    <h6 class="text-muted text-uppercase ">Productos críticos (Sub Bodegas)</h6>
                    <?php echo '<h2 class="m-b-20 text-center "><span id="num">'.number_format($critico_mini,0,",",".").'</span></h2>' ?>
                    <a class="btn btn-block btn-primary waves-light waves-effect" href="<?php echo base_url();?>bodega" >Ver</a>
                </div>
            </div>


        </div>




    <div class="row">

        <div class="col-sm-12 col-md-6">
            <canvas id="myChart"></canvas>


        </div>

    </div>
        <br></br>

<!--        <div class="row">-->
<!---->
<!---->
<!--            <div class="col-sm-12 col-md-4">-->
<!--                <div class="card-box">-->
<!--                    <h4 class="header-title mb-4">Ultimas Transacciones</h4>-->
<!---->
<!--                    <ul class="list-unstyled transaction-list slimscroll mb-0" style="max-height: 370px;">-->
<!---->
<!--                        --><?php //if($transacciones):
//                            foreach($transacciones as $transaccion):
//                                echo "<li>";
//                                if ($transaccion->id_tipo_io==1):
//
//                                    echo "<i class=\"dripicons-arrow-down text-success\"></i>";
//                                    echo "<span class=\"tran-text\">Advertising</span>";
//
//                                    echo "<span class=\"float-right text-success tran-price\">+$".$transaccion->monto."</span>";
//                                else:
//
//                                    echo "<i class=\"dripicons-arrow-up text-danger\"></i>";
//                                    echo "<span class=\"tran-text\">".$transaccion->proveedor."</span>";
//                                    echo "<span class=\"float-right text-danger tran-price\">-$".number_format($transaccion->monto,0,",",".")."</span>";
//                                endif;
//
//
//                                echo "<span class=\"float-right text-muted\">".$transaccion->fecha_emision."</span>";
//                                echo "<span class=\"clearfix\"></span>";
//                                echo "</li>";
//                            endforeach;
//                        endif;
//                        ?>
<!---->
<!--                    </ul>-->
<!---->
<!--                </div>-->
<!--            </div>-->
<!---->
<!---->
<!---->
<!---->
<!---->
<!--        </div>-->



    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- Chart JS -->
<script src="<?php echo base_url();?>plugins/chart.js/chart.bundle.js"></script>


<script>

    $( document ).ready(function() {

        var today = new Date();

        var aMonth = today.getMonth();
        var months = [], i;
        var acum = [];
        var egreso=[]
        var month = new Array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

        for (i = 6; i < 12; i++) {
            months.push(month[aMonth]);
            acum.push(0);
            aMonth--;
            if (aMonth < 0) {
                aMonth = 11;
            }
        }
        months=months.reverse();

        $.ajax({

            url: "<?php echo base_url(); ?>dashboard/fetch_egreso_acum_mes",
            method: "POST",
            dataType: 'json',
            success: function (data) {

                // Add options
                $.each(data, function (index, data) {

                    var idx = months.indexOf(data['mes']);
                    if (idx != null) {

                        egreso[idx] = data['acum'];
                    }
                });


                }



        });



        $.ajax({

            url: "<?php echo base_url(); ?>dashboard/fetch_ingreso_acum_mes",
            method: "POST",
            dataType: 'json',
            success: function (data) {

                // Add options
                $.each(data, function (index, data) {

                    var idx = months.indexOf(data['mes']);
                    if (idx != null) {

                        acum[idx] = data['acum'];
                    }
                });





            }
        });


        $(document).ajaxStop(function () {


            var ctx = document.getElementById('myChart').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'bar',

                // The data for our dataset
                data: {
                    labels: months,
                    datasets: [{
                        label: "Ingreso",
                        backgroundColor: 'rgb(10,207,151)',
                        borderColor: 'rgb(10,207,151)',
                        data: acum
                    },{
                        label: "Egreso",
                        backgroundColor: 'rgb(224,0,0)',
                        borderColor: 'rgb(224,20,0)',
                        data: egreso
                    }]
                },

                // Configuration options go here
                options: {
                    title: {
                        display: true,
                        text: 'Ingreso y Egreso Mensual Acumulado'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return '$' + value.toLocaleString('es-ES');
                                }
                            }
                        }]
                    }


                }
            });






            // 0 === $.active
        });


        })





</script>