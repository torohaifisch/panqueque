
<link href="<?php echo base_url();?>/plugins/footable/css/footable.standalone.min.css" rel="stylesheet">

<style>
    .footable-filtering-search .btn.dropdown-toggle {
        display: none;
    }
</style>

<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item"><a href="#">Productos</a></li>
                            <li class="breadcrumb-item active">Documento de Compra</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Documento de Compra</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">Documento de Compra: Sucursal <?php         date_default_timezone_set("America/Santiago");
                        echo $sucursal->nombre ?></h4>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 class="text-custom">Fecha</h5>
                            <?php echo date("Y-m-d") ?>
                        </div> <div class="col-sm-3">
                            <h5 class="text-custom">Hora</h5>
                            <?php echo date("H:i:s") ?>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn btn-custom float-right" onclick="imprimir()"><i class="mdi mdi-printer"></i> Imprimir</button>
                        </div>
                    </div>


                    <br></br>

                        <table class="table toggle-arrow-tiny" data-sorting="true" >
                            <thead>
                            <tr>


                                <th>Id</th>
                                <th>Producto</th>
                                <th>Cantidad Actual</th>
                                <th>Cantidad Optima</th>
                                <th>Cantidad a Comprar</th>
                            </tr>
                            </thead>


                            <tbody>
                            <?php if ($productos):
                                foreach ($productos as $producto):
                                    $comprar=$producto->cantidad_optima-$producto->cantidad;
                                    if ($comprar>0):
                                        echo "<tr>";
                                        echo "<td><input class=\"form-control\" type=\"number\" id=\"id[]\" name=\"id[]\" value='".$producto->id_inventario_bodega."' hidden required>".$producto->id_inventario_bodega."</td>";
                                        echo "<td>".$producto->producto."</td>";
                                        echo "<td>".(float)$producto->cantidad.' '.$producto->medida."</td>";
                                        echo "<td>".(float)$producto->cantidad_optima.' '.$producto->medida."</td>";
                                        echo "<td>".(float)$comprar.' '.$producto->medida."</td>";

                                        echo "</tr>";
                                        endif;
                                endforeach;
                            endif;
                            ?>
                            </tbody>

                        </table>

                        <br>


                </div>
            </div>








        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->







<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<!--FooTable-->
<script src="<?php echo base_url();?>/plugins/footable/js/footable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        jQuery(function($){
            $('.table').footable();
        });

    } );





    function imprimir() {
        window.print();
    }



</script>