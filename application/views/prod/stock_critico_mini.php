
<link href="<?php echo base_url();?>/plugins/footable/css/footable.standalone.min.css" rel="stylesheet">

<style>
    .footable-filtering-search .btn.dropdown-toggle {
        display: none;
    }
</style>

<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item"><a href="#">Sub Bodega</a></li>
                            <li class="breadcrumb-item active">Stock Critico</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Modificar Stock Critico y Optimo: Sucursal <?php  echo $sucursal->nombre ?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">


                        <div class="col-sm-3">
                            <h5 class="text-custom">Sucursal</h5>
                            <?php echo $sucursal->nombre?>

                        </div>

                        <div class="col-sm-3">
                            <h5 class="text-custom" >Area</h5>
                            <?php echo $area->area
                            ?>


                        </div>




                    </div>

                </div>
            </div>
        </div>





        <div class="row">
            <div class="col-12">
                <div class="card-box">

                    <form id="stock_critico_form" action="<?php echo base_url();?>Bodega/actualizar_stock_critico_sub" method="post" >

                        <input class="form-control" type="number" id="area" name="area" value="<?php echo $area->id_area_trabajo?>" hidden required>
                        <input class="form-control" type="number" id="sucursal" name="sucursal" value="<?php echo$sucursal->id_sucursal?>" hidden required>

                        <table class="table toggle-arrow-tiny"  data-filtering="true" data-sorting="true" >
                        <thead>
                        <tr>


                            <th>Id</th>
                            <th>Sucursal</th>
                            <th>Area</th>
                            <th>Producto</th>
                            <th>Cantidad Actual</th>
                            <th>Cantidad Crítica</th>
                            <th>Cantidad Optima</th>

                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($productos):
                            foreach ($productos as $producto):
                                echo "<tr>";
                                echo "<td><input class=\"form-control\" type=\"number\" id=\"id[]\" name=\"id[]\" value='".$producto->id_mini_bodega."' hidden required>".$producto->id_mini_bodega."</td>";
                                echo "<td>".$producto->sucursal."</td>";
                                echo "<td>".$producto->area."</td>";
                                echo "<td>".$producto->producto."</td>";
                                echo "<td>". (float) $producto->sub_stock.' '.$producto->medida."</td>";
                                echo "<td><input class=\"form-control\" type=\"number\" id=\"cantidad_critica[]\" name=\"cantidad_critica[]\" min=\"0\" step=\"0.001\" value='".(float)$producto->cantidad_critica."' required></td>";
                                echo "<td><input class=\"form-control\" type=\"number\" id=\"cantidad_optima[]\" name=\"cantidad_optima[]\" min=\"0\" step=\"0.001\" value='".(float)$producto->cantidad_optima."' required></td>";
                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                        <tfoot>
                        <tr class="active">
                            <td colspan="5">
                                <div class="text-right">
                                    <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                        <br>
                        <div class="row ">
                            <div class="col-sm-2 offset-sm-10">
                                <button type="submit" class="btn btn-primary">Actualizar</button>

                            </div>

                        </div>

                    </form>



                </div>
            </div>








        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->







<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<!--FooTable-->
<script src="<?php echo base_url();?>/plugins/footable/js/footable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        jQuery(function($){
            $('.table').footable();
        });

    } );


    $( "#stock_critico_form" ).submit(function( event ) {

           /* event.preventDefault();*/


    });




</script>