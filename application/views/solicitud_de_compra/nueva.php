
<style>

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }

</style>

<div class="wrapper">


    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>



    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item active">Solicitud de Compra</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Solicitud de Compra</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row dynamic-element" style="display:none">


            <div class="col-md-3">
                <div class="form-group">
                    <label for="producto">Marca *</label>
                    <select class="custom-select marca" id="marca[]" name="marca[]" required>


                    </select>
                </div>
            </div>


            <div class="col-md-3">
                <div class="form-group">
                    <label for="producto">Producto *</label>
                    <select class="custom-select producto" id="producto[]" name="producto[]" required>


                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="formato">Formato *</label>
                    <select class="custom-select formato" id="formato[]" name="formato[]" required>


                    </select>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group ">
                    <label for="cantidad">Cantidad*</label>
                    <input class="form-control" type="number" id="cantidad[]" name="cantidad[]" min="1" required>

                </div>
            </div>



            <div class="col-md-1">
                <div class="form-group ">

                    <label for="cantidad">Eliminar</label>


                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger delete-row"> <i class="fa fa-remove"></i> </button>
                </div>
            </div>


        </div>

        <!-- end dynamic -->



        <div class="row">
            <div class="col-md-12">
                <div class="card-box">



                <form id="example-advanced-form" action="<?php echo base_url();?>Solicitud_de_compra/crear" method="post">


                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="sucursal" >Sucursal</label>

                                <select class="form-control" id="sucursal" name="sucursal" required>

                                </select>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="area" >Area</label>

                                <select class="form-control" id="area" name="area" required>

                                </select>
                            </div>
                        </div>



                        <div class="col-sm-12">

                            <div class="form-group">
                                <label for="comentario">Comentario</label>
                                <textarea class="form-control" type="text" id="comentario" name="comentario" ></textarea>

                            </div>

                        </div>

                    </div>


                    <button type="button" class="btn btn-info waves-effect waves-light add-dynamic" id="add-dynamic" >Agregar detalle</button>
                    <br></br>
                        <div class="dynamic-stuff">

                            <div class="row dynamic-element" >

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="producto">Marca *</label>
                                        <select class="custom-select marca" id="marca[0]" name="marca[0]" required>


                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="producto">Producto *</label>
                                        <select class="custom-select producto" id="producto[0]" name="producto[0]" required>


                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="formato">Formato *</label>
                                        <select class="custom-select formato" id="formato[0]" name="formato[0]" required>


                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="form-group ">
                                        <label for="cantidad">Cantidad*</label>
                                        <input class="form-control" type="number" id="cantidad[0]" name="cantidad[0]" min="1" required>

                                    </div>
                                </div>




                            </div>



                        </div>
                        <button type="button" class="btn btn-info waves-effect waves-light add-dynamic" id="add-dynamic2" >Agregar detalle</button>

                    <br></br>
                    <div class="row ">
                        <div class="col-sm-12 text-right">
                            <button type="submit" class="btn btn-custom">Finalizar </i></button>

                        </div>

                    </div>

                </form>





            </div>
            <!-- end col -->


            </div>




        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>


<script>

    $( document ).ready(function() {

        cargar_marcas();
        cargar_sucursales();
        cargar_areas();
        attach_change();



        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: "es",
            todayHighlight: true


        });


        var counter=1;


        //Clone the hidden element and shows it
        $('.add-dynamic').click(function(){
            //$('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
            var clon=$('.dynamic-element').first().clone();
            clon.find("input").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')
            });

            clon.find("select").each(function(){
                this.name=this.name.replace('[]','['+counter+']');
                this.id=this.id.replace('[]','['+counter+']')

                if (this.name=="producto["+counter+"]"| this.name=="marca["+counter+"]"){
                    $(this).addClass('select2');
                }
            });

            clon.appendTo('.dynamic-stuff').show();


            attach_delete();
            attach_change();
            counter++;
            //$(".select2").select2();


        });


        //Attach functionality to delete buttons
        function attach_delete() {
            $('.delete-row').off();
            $('.delete-row').click(function () {

                $(this).closest('.dynamic-element').remove();
            });
        }




    });

    function attach_change(){
        $('.marca').off();
        $('.marca').on('change',function(){
            marca=$(this);
            var selected_val =this.value;
            var p_val=null;
            marca.closest('.dynamic-element').find('.producto').empty();
            marca.closest('.dynamic-element').find('.formato').empty();

            $.ajax({

                url: "<?php echo base_url(); ?>Marca_producto/fetch_producto",
                method: "POST",
                dataType: 'json',
                data:{id:selected_val},
                success: function (data) {


                    // Add options
                    $.each(data, function (index, data) {
                        if (p_val==null && index==0){
                            p_val=data['id_producto'];
                        }
                        marca.closest('.dynamic-element').find('.producto').append('<option value="' + data['id_producto'] + '">' + data['nombre'] + '</option>');
                    });

                    cargar_formato(p_val,marca.closest('.dynamic-element').find('.formato'))


                }
            });


            });



        $('.producto').off();
        $('.producto').on('change',function() {
            producto = $(this);
            var selected_val = this.value;
            producto.closest('.dynamic-element').find('.formato').empty();
            $.ajax({

                url: "<?php echo base_url(); ?>producto_formato/fetch_producto_formato",
                method: "POST",
                dataType: 'json',
                data: {id: selected_val},
                success: function (data) {


                    // Add options
                    $.each(data, function (index, data) {
                        producto.closest('.dynamic-element').find('.formato').append('<option value="' + data['id_producto_formato'] + '">' + data['cantidad_contenida'] + data['medida'] + '</option>');
                    });


                }
            });})



        }


</script>

<script>


    function cargar_marcas(){


        $.ajax({

            url: "<?php echo base_url(); ?>Marca/fetch_marcas",
            method: "POST",
            dataType: 'json',
            success: function (data) {

                $('.marca').each(function(){
                    var selected_val=$(this).val();
                    var marca=$(this);

                    marca.empty();

                    // Add options
                    $.each(data, function (index, data) {
                        if (selected_val==null && index==0){
                            selected_val=data['id_marca'];
                        }
                        marca.append('<option value="' + data['id_marca'] + '">' + data['nombre'] + '</option>');
                    });

                    if (selected_val!=null){
                        marca.val(selected_val);
                        cargar_productos(selected_val,marca.closest('.dynamic-element').find('.producto'));

                    }
                });

            }
        });





    }





    function cargar_productos(id,product_selector){
        var selected_val=product_selector.val();

        $.ajax({

            url: "<?php echo base_url(); ?>Marca_producto/fetch_producto",
            method: "POST",
            dataType: 'json',
            data:{id:id},
            success: function (data) {
                product_selector.empty();

                // Add options
                $.each(data, function (index, data) {
                    if (selected_val==null && index==0){
                        selected_val=data['id_producto'];
                    }


                    product_selector.append('<option value="' + data['id_producto'] + '">' + data['nombre'] + '</option>');
                });

                if (selected_val!=null){
                    product_selector.val(selected_val);
                    cargar_formato(selected_val,product_selector.closest('.dynamic-element').find('.formato'))

                }
            }
        });


    }


    function cargar_formato(id,format_selector){
        var selected_val=format_selector.val();

        $.ajax({

            url: "<?php echo base_url(); ?>Producto_formato/fetch_producto_formato",
            method: "POST",
            dataType: 'json',
            data:{id:id},
            success: function (data) {
                format_selector.empty();

                // Add options
                $.each(data, function (index, data) {
                    format_selector.append('<option value="' + data['id_producto_formato'] + '">' + data['cantidad_contenida'] + data['medida'] + '</option>');
                });

                if (selected_val!=null){
                    format_selector.val(selected_val);
                }
            }
        });


    }



    function cargar_sucursales(){

        var selected_val=$('#sucursal').val();
        var tipo=$('#sucursal');
        $.ajax({

            url: "<?php echo base_url(); ?>sucursal/fetch_sucursales",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                tipo.empty();

                // Add options
                $.each(data, function (index, data) {
                    if (selected_val==null && index==0){
                        selected_val=data['id_sucursal'];
                    }
                    tipo.append('<option value="' + data['id_sucursal'] + '">' + data['nombre'] + '</option>');
                });



            }
        });

    }

    function cargar_areas(){


        var tipo=$('#area');
        $.ajax({

            url: "<?php echo base_url(); ?>area_trabajo/fetch_areas",
            method: "POST",
            dataType: 'json',
            success: function (data) {
                tipo.empty();

                // Add options
                $.each(data, function (index, data) {
                    tipo.append('<option value="' + data['id_area_trabajo'] + '">' + data['area'] + '</option>');
                });



            }
        });


    }



</script>