
<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>

    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Pagos y Cobros</a></li>
                            <li class="breadcrumb-item active">Pagos Pendientes</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Pagos Pendientes</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">

                <div class="card-box">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="calendar"></div>
                        </div> <!-- end col -->



                        <div class="col-md-6">

                            <div class="alert alert-info" role="alert">
                                <h4>Importante</h4>
                                <ul>
                                    <li>El monto bruto calculado es un valor aproximado y puede no reflejar el valor total real a pagar.</li>
                                </ul>
                            </div>


                            <form id="pago_form" role="form" method="post" action="<?php echo base_url();?>notificacion_pago/crear">
                            <h3>Notificacion de Pago</h3>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nombre">Responsable *</label>
                                        <select class="form-control" id="responsable" name="responsable" required>
                                            <option value="<?php echo $this->session->userdata('id');?>"><?php echo $this->session->userdata('nombre');?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tipo_dte">Tipo de Pago *</label>
                                        <select class="form-control" id="tipo" name="tipo" required>
                                            <option value="1">Efectivo</option>
                                            <option value="2">Cheque</option>
                                            <option value="3">Transferencia / Deposito</option>
                                            <option value="4">Tarjeta de Credito</option>
                                        </select>



                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tipo_dte">N° Documento/ Transferencia *</label>
                                        <input type="number" class="form-control" id="numero_doc" name="numero_doc" disabled>


                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total">Monto total Bruto *</label>
                                        <input type="number" class="form-control" id="total" name="total" required>


                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="datepicker">Fecha *</label>
                                        <input type="text" class="form-control hasDatepicker" placeholder="aaaa-mm-dd"  id="datep" name="fecha" required>

                                    </div>

                                </div>


                            </div>



                            <h4>Facturas a pagar</h4>
                            <hr>

                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>

                                    <th>Numero DTE</th>
                                    <th>Proveedor</th>
                                    <th>Monto</th>
                                    <th>Eliminar</th>
                                </tr>
                                </thead>


                                <tbody class="dynamic-stuff">

                                </tbody>
                            </table>

                            <hr>
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-custom">Ingresar</button>

                                </div>
                            </form>
                        </div>


                    </div>  <!-- end row -->
                </div>

                <!-- BEGIN MODAL -->
                <div id="por_pagar_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h2 class="text-center m-b-30" id="titulo">
                                    Facturas a Pagar
                                </h2>
                                <div class="dinamico">


                                </div>



                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- END MODAL -->

        </div> <!-- end row -->


    </div> <!-- end container -->
</div>
<!-- end wrapper -->




<!-- Jquery-Ui -->

<!-- SCRIPTS -->
<script src="<?php echo base_url();?>plugins/moment/moment.js"></script>
<script src='<?php echo base_url();?>plugins/fullcalendar/js/fullcalendar.min.js'></script>


<script>

    $( document ).ready(function() {
        $(document).on('click','.add-dynamic',function(){ $('#total').val(calcular_monto());});
        $(document).on('click','.delete-row',function(){ $('#total').val(calcular_monto());});


        $('#datep').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: "es",
            todayHighlight: true,
            showNonCurrentDates: false,
            fixedWeekCount: false

        });

    });


</script>

<script>


    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month'
        },
        eventClick:function(a,b,c){
            var texto='Facturas a Pagar '+a.start.format('DD/MMMM/YYYY');
            $('#por_pagar_modal h2').text(texto);
            $('#por_pagar_modal').modal('toggle');
            fetch_pagar_dia(a.start.format('YYYY-MM-DD'))




        },
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],

        events: '<?php echo base_url();?>notificacion_pago/fetch_eventos_pagar',
        editable:false


    });
    $('#calendar').fullCalendar('refetchEvents');

    $('.dinamico').on('click','.add-dynamic',function(event){
        console.log($(this).attr('data-dte'));
        $('.dynamic-stuff').append('<tr>' +
            '<td>'+    $(this).attr('data-numero')+'</td>' +
            '<td>'+    $(this).attr('data-proveedor')+'</td>' +
            '<td class="subtotal" value="'+$(this).attr('data-monto')+'">$'+    Number($(this).attr('data-monto')).toLocaleString('es-ES')+'</td>' +
            '<td><input type="number" class="form-control" id="id_dte[]" name="id_dte[]" value="'+$(this).attr('data-dte')+'" hidden required><button type="button" class="btn btn-icon waves-effect waves-light btn-danger delete-row"</button>Eliminar</td>' +

            '</tr>');
        attach_delete();
    });

    function attach_delete() {
        $('.delete-row').off();
        $('.delete-row').click(function () {

            $(this).closest(' tr ').remove();
        });
    }

    function fetch_pagar_dia(dia) {
        $('.dinamico').empty();
        var url = "<?php echo base_url(); ?>Notificacion_pago/fetch_pagar_dia";

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {dia: dia}, // serializes the form's elements.
            success: function (data) {
                $.each(data, function (index, data) {
                    console.log(Number(data.monto).toLocaleString('es-ES'));
                    $('.dinamico').append('<div class="row "><div class="col-sm-3"><label for="numero_dte">Numero dte</label><input type="number" class="form-control" id="numero_dte" value="'+data.numero_dte+'" readonly>'
                        + '</div><div class="col-sm-4"><label for="proveedor">Proveedor</label><input type="text" class="form-control" id="proveedor" value="' + data.nombre + '" readonly></div>' +
                        '<div class="col-sm-3"><label for="marca">Monto</label><input type="text" class="form-control" id="monto_dte" value="$'+Number(data.monto).toLocaleString('es-ES')+'" readonly></div>' +
                        '<div class="col-sm-1"><label >Agregar</label><button type="button" class="btn btn-icon waves-effect waves-light btn-success add-dynamic" data-numero="'+data.numero_dte+'" data-proveedor="'+data.nombre+'" data-monto="'+data.monto+'" data-dte="'+data.id_dte+'"><i class="fa fa-plus"></i></button></div> </div>');
                });
            }
        });
    }


    $('#tipo').on('change', function(){

        var valor = this.value;
        if (valor == '1' | valor=='4') {



            $('#numero_doc').removeAttr('required');
            $('#numero_doc').val('');
            $("#numero_doc").attr('disabled', 'disabled');

        } else {
            $('#numero_doc').removeAttr('disabled');
            $('#numero_dte').attr('required',true);




        }
        return false;
    });


    $("#pago_form").submit(function(e) {
        if($('.dynamic-stuff > tr ').length===0){
            e.preventDefault();
        }
    });

    function calcular_monto(){
        var total=0;
        $('.subtotal').each(function(){
            var cantidad =Number($(this).attr('value'));
            console.log(cantidad);
            total+=cantidad*1.19;

        });
        return total;
    }

</script>
