
<div class="wrapper">

    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item"><a href="#">Solicitud Compra</a></li>
                            <li class="breadcrumb-item active">Ver detalle</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Detalle Notificacion de Pago N°<?php echo $notif->id_notificacion_pago?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">

                        <div class="col-sm-4">
                            <h5 class="text-custom">Responsable</h5>
                            <?php echo $notif->nombre.' '.$notif->ape_pat?>

                        </div>

                        <div class="col-sm-4">
                            <h5 class="text-custom" >Tipo</h5>
                            <?php echo $notif->tipo
                            ?>


                        </div>
                        <div class="col-sm-4">
                            <h5 class="text-custom" >N° Documento</h5>
                            <?php echo $notif->numero_documento?>

                        </div>
                        <div class="col-sm-12">
                            <h5 class="text-custom" >Fecha de Pago</h5>
                            <?php echo $notif->fecha_pago?>

                        </div>


                    </div>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card-box">



                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>

                            <th>Proveedor</th>
                            <th>Id DTE</th>
                            <th>Tipo DTE</th>
                            <th>N° DTE</th>
                            <th>Monto</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($detalles):
                            foreach ($detalles as $detalle):
                                echo "<tr>";
                                echo "<td>".$detalle->proveedor."</td>";
                                echo "<td>".$detalle->id_dte."</td>";
                                echo "<td>".$detalle->tipo_dte."</td>";
                                echo "<td>".$detalle->numero_dte."</td>";
                                echo "<td>".number_format($detalle->monto,0,",",".")."</td>";

                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>












        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>


<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>





<script src="<?php echo base_url();?>plugins/sweet-alert/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        $('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });

    } );







</script>