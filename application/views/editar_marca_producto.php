<style>


     input.error {
        background: rgb(251, 227, 228);
        border: 1px solid #fbc2c4;
        color: #8a1f11;
    }

     label.error
    {
        color: #f1556c;
        margin-left: 0;
    }

</style>

<div class="wrapper">

    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>






    <div class="container">


        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administrar</a></li>
                            <li class="breadcrumb-item"><a href="#">Marcas y Productos</a></li>
                            <li class="breadcrumb-item active">Editar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Producto</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->




        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                <h4><?php echo $marca_producto->marca.' '.$marca_producto->producto.' '.$marca_producto->cantidad_contenida.' '.$marca_producto->medida;?></h4>
                <form id="marca_producto_form" role="form" method="post" action="<?php echo base_url();?>marca_producto/editar_marca_producto/<?php echo $marca_producto->id_marca_producto_formato?>">

                    <div class="form-group">
                        <label for="sku">Codigo*</label>
                        <input class="form-control" type="number" id="sku" name="sku" value="<?php echo $marca_producto->sku;?>" >

                    </div>



                    <button type="submit" class="btn btn-primary">Actualizar </button>
                </form>


            </div>

            </div>


        </div>




    </div>



</div>

<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/localization/messages_es.min.js"></script>
