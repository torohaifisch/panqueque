
<div class="wrapper">
    <?php if($this->session->flashdata('success_msg')){ ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>

    <?php } ?>

    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bodega</a></li>
                            <li class="breadcrumb-item active">Sub Bodega</li>

                        </ol>
                    </div>
                    <h4 class="page-title">Inventario Sub bodega</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="card-box">
        <div class="row">

            <div class="col-sm-3">
                <h5 class="text-custom">Sucursal</h5>
                <?php echo $sucursal->nombre?>

            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="sucursal" class="text-custom">Area</label>
                    <select class="custom-select area" id="area" name="area">
                        <option value="0">Todas</option>
                        <?php if($areas):
                            foreach($areas as $area):
                                echo '<option value="'.$area->id_area_trabajo.'">'. $area->area.'</option>';

                            endforeach;
                        endif


                        ?>
                    </select>
                </div>
            </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-3">
                <button class="btn btn-custom dropdown-toggle btn-block" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Modificar Detalles Stock
                </button>


                <div class="dropdown-menu" aria-labelledby="dropdownMenu2" style="font-size:14px">
                    <?php
                    if ($areas):
                        foreach ($areas as $area):
                            echo  "<a class=\"dropdown-item\" href=\"".base_url().'bodega/stock_critico_sub_bodega/'.$sucursal->id_sucursal.'/'.$area->id_area_trabajo."\" >".$area->area."</a>";

                        endforeach;

                    endif

                    ?>



                </div>
            </div>

            <div class="col-sm-3">
                <button class="btn btn-custom dropdown-toggle btn-block" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Asignar Productos - Automatico
                </button>


                <div class="dropdown-menu" aria-labelledby="dropdownMenu2" style="font-size:14px">
                    <?php
                    if ($areas):
                        foreach ($areas as $area):
                            echo  "<a class=\"dropdown-item\" href=\"".base_url().'bodega/asignar_producto_area/'.$sucursal->id_sucursal.'/'.$area->id_area_trabajo."\" >".$area->area."</a>";

                        endforeach;

                    endif

                    ?>



                </div>
            </div>

            <div class="col-sm-3">
                <button class="btn btn-custom dropdown-toggle btn-block" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Copiar Productos desde General
                </button>


                <div class="dropdown-menu" aria-labelledby="dropdownMenu2" style="font-size:14px">
                    <?php
                    if ($areas):
                        foreach ($areas as $area):
                            echo  "<a class=\"dropdown-item\" href=\"".base_url().'bodega/nueva_stock_cero/'.$sucursal->id_sucursal.'/'.$area->id_area_trabajo."\" >".$area->area."</a>";

                        endforeach;

                    endif

                    ?>



                </div>
            </div>



        </div>


        <br>

        <div class="row">
            <div class="col-12">
                <div class="card-box">

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Area</th>
                            <th>Producto</th>
                            <th>Stock</th>


                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php if ($productos):
                            foreach ($productos as $producto):
                                echo "<tr>";
                                echo "<td>".$producto->sucursal."</td>";
                                echo "<td>".$producto->area."</td>";
                                echo "<td>".$producto->producto."</td>";
                                echo "<td>".(float)$producto->sub_stock.' '.$producto->medida."</td>";

                                if($producto->sub_stock<=$producto->cantidad_critica):
                                    echo '<td><span class="badge badge-warning">Crítico</span></td>';

                                else:
                                    echo '<td><span class="badge badge-success">Normal</span></td>';
                                endif;
                                echo "<td>
                                            <div class=\"btn-group dropdown\">
                                            <a href=\"javascript: void(0);\" class=\"table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"mdi mdi-dots-horizontal\"></i></a>
                                            <div class=\"dropdown-menu dropdown-menu-right\">
                                                <a class=\"dropdown-item\" href=\"\" id=\"modificar_stock\" data-id='".$producto->id_mini_bodega."' data-stock='".(float)$producto->sub_stock."' data-suc='".$producto->sucursal."' data-info='".$producto->producto."' data-medida='".$producto->medida."'>Modificar Stock</a>

                                            </div>
                                        </div>
                                        </td>";
                                echo "</tr>";
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>




            <div id="modificar_stock_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2 class="text-center m-b-30 text-custom">
                                Modificar Stock
                            </h2>
                            <h4 class="text-center m-b-30" id="m_info">
                            </h4>

                            <form  id="modificar_stock_form" role="form" method="post" action="<?php echo base_url();?>Bodega/modificar_stock_mini">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <label for="sucursal">Sucursal :</label>
                                        <p id="m_suc"></p>


                                    </div>

                                    <div class="col-sm-6">
                                        <label for="sucursal">Cantidad Actual:</label>
                                        <p id="m_stock"></p>


                                    </div>


                                    <input class="form-control" type="number" id="id_inventario" name="id_inventario"  hidden required>



                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="cantidad">Nuevo Cantidad*</label>
                                            <input class="form-control" type="number" id="cantidad" name="cantidad" step="0.001" min="0.001" required>

                                        </div>
                                    </div>




                                </div>





                                <button type="submit" class="btn btn-primary">Modificar</button>
                            </form>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->





<!-- Required datatable js -->
<script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap4.min.js"></script>



<!-- Responsive examples -->
<script src="<?php echo base_url();?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>plugins/datatables/responsive.bootstrap4.min.js"></script>





<script type="text/javascript">
    $(document).ready(function() {


        // Default Datatable
        var table=$('#datatable').DataTable({

            "language": {

                "emptyTable":     "No data available in table",
                "info":           "Mostrando desde _START_ a _END_ de _TOTAL_ entradas en total",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "infoFiltered":   "(filtrado de _MAX_ entradas totales)",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "search":         "Buscar:",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }


        });

        $('#area').on('change', function(){
            var val=$('#area option:selected').text();
            if (val=='Todas'){
                val='';
            }
            table.search(val).draw();
        });

    } );



    $('a#modificar_stock').click(function (event) {
        event.preventDefault();
        $('#modificar_stock_modal').modal('toggle');
        $('#m_info').text($(this).attr("data-info"));
        $('#m_suc').text($(this).attr("data-suc"));
        $('#m_stock').text($(this).attr("data-stock")+' '+$(this).attr("data-medida"));
        $('#id_inventario').val($(this).attr("data-id"))

    });




</script>